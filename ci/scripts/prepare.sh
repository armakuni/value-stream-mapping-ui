#!/usr/bin/env bash

set -euo pipefail

set_manifest_val () {
  TEMP_FILE="$(mktemp)"
  yaml set "${1}" "${2}" "${3}" >> "${TEMP_FILE}"
  rm "${1}"
  mv "${TEMP_FILE}" "${1}"
}

(
  cd value-stream-mapping-ui-git &&
  npm install
)

npm install -g yaml-cli

BUILD_VERSION="$(cat version/number)"

cp -r value-stream-mapping-ui-git/. value-stream-mapping-ui
cd value-stream-mapping-ui

npm install -g browserify
browserify js/umlsc.js -o assets/js/umlsc.js
browserify js/index.js -o assets/js/index.js

set_manifest_val manifest.yml applications.0.env.USER_CREDENTIALS "${USER_CREDENTIALS}"
set_manifest_val manifest.yml applications.0.env.DB_ENCRYPTION_KEY "${DB_ENCRYPTION_KEY}"
set_manifest_val manifest.yml applications.0.env.BUILD_VERSION "${BUILD_VERSION}"

