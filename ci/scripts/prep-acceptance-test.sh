#!/bin/sh

set -eu

jq -n --arg baseUrl "${URL}" \
--arg username "${SMOKE_TEST_USERNAME}" \
--arg password "${SMOKE_TEST_PASSWORD}" \
'{
  "baseUrl": "\($baseUrl)",
  "username": "\($username)",
  "password": "\($password)"
}' > config/config.json
