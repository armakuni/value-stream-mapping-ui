#!/usr/bin/env bash

set -euo pipefail

nonVendorGoFiles=$(find . -type f -name '*.go' -not -path "./vendor/*")

exitCode=0

reportOnOutput() {
  if [ -z "${1}" ]; then
    echo "Everything is awesome!"
  else
    echo "${1}"
    exitCode=1
  fi
}

gofmtOutput=$(gofmt -s -e -d $nonVendorGoFiles)

echo "##### Start GoFMT #####"

reportOnOutput "${gofmtOutput}"

printf "##### End GoFMT #####\n\n"

echo "##### Start Go Vet #####"

goVetOutput=$(go vet .)

reportOnOutput "${goVetOutput}"

printf "##### End Go Vet #####\n\n"

goCycloOutput=$(gocyclo -over 10 $nonVendorGoFiles)

echo "##### Start GoCyclo #####"

reportOnOutput "${goCycloOutput}"

printf "##### End GoCyclo #####\n\n"

goLintOutput=$(golint -set_exit_status $nonVendorGoFiles)

echo "##### Start GoLint #####"

reportOnOutput "${goLintOutput}"

printf "##### End GoLint #####\n\n"

misspellOutput=$(misspell . | sed '/node_modules/d' | sed '/vendor/d' | sed '/fontawesome/d' | sed '/font-awesome/d')

echo "##### Start Misspell #####"

reportOnOutput "${misspellOutput}"

printf "##### End Misspell #####\n"

if [ -f ./package.json ]; then
  npm install
fi


if [ -f ./node_modules/.bin/standard ]; then
  standardOutput=$(./node_modules/.bin/standard "assets/**/*.js")

  echo "##### Start Standard #####"

  reportOnOutput "${standardOutput}"

  printf "##### End Standard #####\n"
fi

if [ -f ./node_modules/.bin/eslint ]; then
  eslintOutput=$(./node_modules/.bin/eslint assets)

  echo "##### Start ESLint #####"

  reportOnOutput "${eslintOutput}"

  printf "##### End ESLint #####\n"
fi

exit $exitCode
