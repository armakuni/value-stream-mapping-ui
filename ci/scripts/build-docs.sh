#!/usr/bin/env bash

set -euo pipefail


cd value-stream-mapping-ui-git

pip install --upgrade pip
pip install mkdocs
pip install mkdocs-material
pip install pygments

mkdir -p ../value-stream-mapping-docs/public

cp -r ci/doc-config/. ../value-stream-mapping-docs/
mkdocs build -d ../value-stream-mapping-docs/public
