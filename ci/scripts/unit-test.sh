#!/usr/bin/env bash

set -euo pipefail

go get ./...
npm install

ginkgo -p -r -cover -race -skipPackage integration
