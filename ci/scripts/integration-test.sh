#!/usr/bin/env bash

set -euo pipefail

trap "su - postgres -c '/usr/lib/postgresql/*/bin/pg_ctl -D /var/lib/postgresql/data stop'" EXIT INT

go get ./...
npm install
npm install -g browserify
browserify js/umlsc.js -o assets/js/umlsc.js
browserify js/index.js -o assets/js/index.js

su - postgres -c '/usr/lib/postgresql/*/bin/pg_ctl init -D /var/lib/postgresql/data'
su - postgres -c '/usr/lib/postgresql/*/bin/pg_ctl -D /var/lib/postgresql/data start'

TEST_URI="postgres://postgres:postgres@127.0.0.1:5432/postgres?sslmode=disable"

ginkgo -r integration
