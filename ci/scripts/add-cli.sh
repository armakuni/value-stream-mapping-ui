#!/usr/bin/env bash

set -euo pipefail


GOPATH=$PWD
export GOPATH

BUILD_VERSION="$(cat version/number)"

cp -r value-stream-mapping-ui-partial/. value-stream-mapping-ui

cd "${GOPATH}/src/bitbucket.org/armakuni/value-stream-mapping-ui"
go get ./...

cd "${GOPATH}/src/bitbucket.org/armakuni/value-stream-mapping-cli"
go mod edit -replace bitbucket.org/armakuni/value-stream-mapping-ui=../value-stream-mapping-ui
go get ./...

cd vsm

GOOS=linux go build -ldflags "-X main.BuildVersion=${BUILD_VERSION}"
mkdir -p "${GOPATH}/value-stream-mapping-ui/cli/linux"
mv vsm "${GOPATH}/value-stream-mapping-ui/cli/linux/"

GOOS=darwin go build -ldflags "-X main.BuildVersion=${BUILD_VERSION}"
mkdir -p "${GOPATH}/value-stream-mapping-ui/cli/darwin"
mv vsm "${GOPATH}/value-stream-mapping-ui/cli/darwin/"

GOOS=windows go build -ldflags "-X main.BuildVersion=${BUILD_VERSION}"
mkdir -p "${GOPATH}/value-stream-mapping-ui/cli/windows"
mv vsm.exe "${GOPATH}/value-stream-mapping-ui/cli/windows/"
