#!/bin/sh

set -eu

cp config/config.json value-stream-mapping-ui-git/acceptance_tests/config.json

cd value-stream-mapping-ui-git

/opt/testcafe/docker/testcafe-docker.sh 'chromium --no-sandbox,firefox' acceptance_tests/testcafe
