#!/usr/bin/env bash

set -euo pipefail


cf api "${API_URL}"
cf auth "${CF_USERNAME}" "${CF_PASSWORD}"

cf target -o armakuni

if ! cf space value-stream-mapping; then
  cf create-space value-stream-mapping
fi

cf target -s value-stream-mapping

if ! cf service vsm-postgres; then
  cf create-service elephantsql turtle vsm-postgres
fi

cf cups applogger -l "${SYSLOG_URL}" || cf uups applogger -l "${SYSLOG_URL}"
