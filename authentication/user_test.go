package authentication_test

import (
	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("NewUser", func() {
	var (
		user authentication.User
	)

	Context("It creates a user", func() {

		BeforeEach(func() {
			user = authentication.NewUser("username", "password", "server password")
		})

		It("has encryption details", func() {
			Ω(user.Encryption.Pad).Should(Not(BeEmpty()))
		})

		It("has the namespace set", func() {
			Ω(user.Namespace).Should(Equal("username"))
		})
	})
})
