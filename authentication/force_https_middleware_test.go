package authentication_test

import (
	"net/http"
	"net/http/httptest"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("ForceHTTPSMiddleware", func() {

	var (
		middleware        authentication.ForceHTTPSMiddleware
		r                 *http.Request
		w                 *httptest.ResponseRecorder
		handlerFuncCalled bool
		url               string
		protocol          string
	)

	It("is middleware", func() {
		Ω(middleware.Middleware).Should(BeAssignableToTypeOf(func(next http.Handler) http.Handler { return next }))
	})

	Context("incoming request", func() {
		JustBeforeEach(func() {
			r = httptest.NewRequest("GET", url, nil)
			w = httptest.NewRecorder()

			r.Header.Set("X-FORWARDED-PROTO", protocol)

			middleware = authentication.ForceHTTPSMiddleware{}

			handlerFuncCalled = false

			middleware.Middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				handlerFuncCalled = true
			})).ServeHTTP(w, r)
		})

		Context("http request", func() {

			BeforeEach(func() {
				url = "http://example.com/foo"
				protocol = "http"
			})

			It("sets the response code moved permanently (301)", func() {
				Ω(w.Code).Should(Equal(http.StatusMovedPermanently))
			})
			It("goes to the https version of the url", func() {
				Ω(w.HeaderMap["Location"]).Should(HaveLen(1))
				Ω(w.HeaderMap["Location"][0]).Should(Equal("https://example.com/foo"))
			})

			It("Should not call the next handler function", func() {
				Ω(handlerFuncCalled).Should(BeFalse())
			})

		})
		Context("https request", func() {

			BeforeEach(func() {
				url = "https://example.com/foo"
				protocol = "https"
			})

			It("leaves the status code", func() {
				Ω(w.Code).Should(Equal(http.StatusOK))
			})

			It("Should call the next handler function", func() {
				Ω(handlerFuncCalled).Should(BeTrue())
			})

		})
	})

})
