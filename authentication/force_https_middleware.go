package authentication

import (
	"fmt"
	"net/http"
)

// ForceHTTPSMiddleware forces connections to be https, and sets HSTS to auto upgrade them in future
type ForceHTTPSMiddleware struct {
}

// Middleware creates mux compatible middleware to force HTTPS
func (fh ForceHTTPSMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("X-FORWARDED-PROTO") == "http" {
			http.Redirect(w, r, fmt.Sprintf("https://%s%s", r.Host, r.URL.Path), http.StatusMovedPermanently)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
