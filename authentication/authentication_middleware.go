package authentication

import (
	"fmt"
	"net/http"
)

// AuthenticatorMiddleware to handle the authentication of users against a provided list
type AuthenticatorMiddleware struct {
	UserCredentials UserCredentials
	Users           Users
}

// NewAuthenticationMiddleware from an encryption key and a list of users credentials
func NewAuthenticationMiddleware(dbEncryptionKey string, credentials UserCredentials) AuthenticatorMiddleware {
	a := AuthenticatorMiddleware{
		UserCredentials: credentials,
	}

	a.Users = make(Users)

	for username, password := range credentials {
		a.Users[username] = NewUser(username, password, dbEncryptionKey)
	}

	return a
}

// Middleware creates mux compatible middleware that authenticates a user against the user credentials list
func (a AuthenticatorMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestUsername, requestPassword, _ := r.BasicAuth()
		storedPassword, userExists := a.UserCredentials[requestUsername]
		if !userExists || storedPassword != requestPassword {
			w.Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm=%q`, "Restricted"))

			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

// UserCredentials a username keyed map of the users passwords
type UserCredentials map[string]string

// Users a username keyed map of the users namespace and encryption credentials
type Users map[string]User
