package authentication

import "bitbucket.org/armakuni/value-stream-mapping-ui/encryption"

// encryptionPad - Characters we use to pad out a users key if it's too short
const encryptionPad = "E\\I>aJ%vtY7:W&tLG@w7bejGdq~UKtLZ"

// User is an instance of a user containing their encryption credentials, and DB namespace
type User struct {
	Encryption encryption.Credentials
	Namespace  string
}

// NewUser creates a instance of a user containing their encryption credentials, and DB namespace
func NewUser(username string, password string, dbEncryptionKey string) User {
	credentials, _ := encryption.NewCredentials(username, password, dbEncryptionKey, encryptionPad)

	return User{
		Namespace:  username,
		Encryption: *credentials,
	}
}
