package authentication_test

import (
	"net/http"
	"net/http/httptest"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("AuthenticatorMiddleware", func() {

	var (
		middleware      authentication.AuthenticatorMiddleware
		userCredentials authentication.UserCredentials
	)

	It("is middleware", func() {
		Ω(middleware.Middleware).Should(BeAssignableToTypeOf(func(next http.Handler) http.Handler { return next }))
	})

	Context("receiving http request", func() {
		var (
			r                 *http.Request
			w                 *httptest.ResponseRecorder
			requestUsername   string
			requestPassword   string
			handlerFuncCalled bool
		)

		BeforeEach(func() {
			r = httptest.NewRequest("GET", "http://example.com/foo", nil)
			w = httptest.NewRecorder()
		})

		JustBeforeEach(func() {

			if requestUsername != "" && requestPassword != "" {
				r.SetBasicAuth(requestUsername, requestPassword)
			}

			middleware = authentication.NewAuthenticationMiddleware(
				"igh7yohhei0oKeeg9aemiiSaixei6aid",
				userCredentials,
			)

			handlerFuncCalled = false

			middleware.Middleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				handlerFuncCalled = true
			})).ServeHTTP(w, r)
		})

		Context("no authentication", func() {
			It("sets the response code unauthorized (401)", func() {
				Ω(w.Code).Should(Equal(401))
			})

			It("Should not call the next handler function", func() {
				Ω(handlerFuncCalled).Should(BeFalse())
			})
		})

		Context("logged in", func() {
			Context("with invalid credentials", func() {

				BeforeEach(func() {
					userCredentials = make(authentication.UserCredentials)
					userCredentials["username"] = "secr3t"

					requestUsername = "i do not exist"
					requestPassword = "i also do not exist"
				})

				It("sets the response code unauthorized (401)", func() {
					Ω(w.Code).Should(Equal(401))
				})

				It("Should not call the next handler function", func() {
					Ω(handlerFuncCalled).Should(BeFalse())
				})
			})
			Context("with valid credentials", func() {

				BeforeEach(func() {
					userCredentials = make(authentication.UserCredentials)
					userCredentials["test user"] = "secr3t"

					requestUsername = "test user"
					requestPassword = "secr3t"
				})

				It("sets the response code OK (200)", func() {
					Ω(w.Code).Should(Equal(200))
				})

				It("Should call the next handler function", func() {
					Ω(handlerFuncCalled).Should(BeTrue())
				})

				It("Should have a user matching that username", func() {
					_, userExists := middleware.Users["test user"]
					Ω(userExists).Should(BeTrue())
				})
			})
		})
	})
})
