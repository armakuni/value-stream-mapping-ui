resource "aws_iam_access_key" "vsm-ci" {
  user    = "${aws_iam_user.vsm-ci.name}"
}

resource "aws_iam_user" "vsm-ci" {
  name = "vsm-ci"
  path = "/"
}

output "id" {
  value = "${aws_iam_access_key.vsm-ci.id}"
}

output "secret" {
  value = "${aws_iam_access_key.vsm-ci.secret}"
}

resource "aws_iam_group" "vsm-ci" {
  name = "vsm-ci"
  path = "/"
}

resource "aws_iam_group_membership" "vsm-ci-members" {
  name = "vsm-ci-members"

  users = [
    "${aws_iam_user.vsm-ci.name}",
  ]

  group = "${aws_iam_group.vsm-ci.name}"
}

resource "aws_iam_group_policy" "vsm-policy" {
  name = "vsm"
  group = "${aws_iam_group.vsm-ci.id}"

  policy = <<EOF
{
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "route53:GetHostedZone",
                "route53:ChangeResourceRecordSets",
                "route53:ListResourceRecordSets"
            ],
            "Resource": [
                "arn:aws:route53:::hostedzone/${var.existing_zone_id}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:GetChange",
                "route53:ListHostedZones",
                "route53:ListHostedZonesByName"
            ],
            "Resource": "*"
        },
        {
          "Effect": "Allow",
          "Action": "s3:ListAllMyBuckets",
          "Resource": "arn:aws:s3:::*"
        },
        {
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": [
              "${aws_s3_bucket.vsm-states.arn}",
              "${aws_s3_bucket.vsm-states.arn}/*"
            ]
        },
        {
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": [
              "${aws_s3_bucket.vsm-db-backups.arn}",
              "${aws_s3_bucket.vsm-db-backups.arn}/*"
            ]
        },
        {
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": [
              "${aws_s3_bucket.vsm-versions.arn}",
              "${aws_s3_bucket.vsm-versions.arn}/*"
            ]
        }
    ]
}
EOF
}

