## Generic

resource "aws_s3_bucket" "vsm-states" {
  bucket = "vsm-tf-states"
  acl = "private"

  versioning {
    enabled = false
  }
}

resource "aws_s3_bucket" "vsm-versions" {
  bucket = "vsm-versions"
  acl = "private"

  versioning {
    enabled = false
  }
}

resource "aws_s3_bucket" "vsm-db-backups" {
  bucket = "vsm-db-backups"
  acl = "private"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "vsm_backups_lifecycle"
    enabled = true

    prefix  = ""
    tags {
      "rule"      = "log"
      "autoclean" = "true"
    }

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }
}
