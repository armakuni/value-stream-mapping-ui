variable "existing_zone_id" {
  type = "string"
  description = "The zone id that the vsm cname's will reside in."
}
