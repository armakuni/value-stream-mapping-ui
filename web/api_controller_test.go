package web_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	"github.com/stretchr/testify/mock"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/armakuni/value-stream-mapping-ui/db"
	"bitbucket.org/armakuni/value-stream-mapping-ui/web"
)

func valueStreamJSON(valueStream mapper.ValueStream) string {
	valueStreamJSON, _ := json.Marshal(valueStream)
	return string(valueStreamJSON)
}

var _ = Describe("#NewAPIController", func() {
	var (
		controller *web.APIController
		err        error
		username   string
		password   string
	)

	JustBeforeEach(func() {
		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		controller = web.NewAPIController(&db.VsmDatabase{}, authenticator)
	})

	BeforeEach(func() {
		username = "test"
		password = "test"
	})

	It("creates a controller", func() {
		Ω(controller).Should(BeAssignableToTypeOf(&web.APIController{}))
		Ω(controller).ShouldNot(Equal(&web.APIController{}))
		Ω(err).Should(BeNil())
	})
})

var _ = Describe("APIController#GetMap", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		versionQuery string
		fakeDB       fakeVsmDB
		customer     = "an example customer"
		title        = "test1"
		name         = "test-name"
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", fmt.Sprintf("http://example.com/v1/maps/%s%s", name, versionQuery), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		apiController := web.NewAPIController(&fakeDB, authenticator)
		server := web.NewServer(authenticator, apiController, nil, "")
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("when getting the map from db errors", func() {
			BeforeEach(func() {
				versionQuery = ""
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, "").Return(
					mapper.ValueStream{},
					fmt.Errorf("error getting map from db"),
				)
			})

			It("returns an error", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusInternalServerError))
				Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"error getting map from db"}`))
			})
		})

		Context("and getting the map from the db is successful", func() {
			Context("and the map does not exist", func() {
				BeforeEach(func() {
					versionQuery = ""
					fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, "").Return(
						mapper.ValueStream{},
						nil,
					)
				})

				It("returns nothing", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
					Ω(mockRecorder.Body.String()).Should(Equal("{}"))
				})
			})

			Context("and the map exists", func() {
				Context("when a version is provided", func() {
					var version = "1.0.0"
					BeforeEach(func() {
						versionQuery = fmt.Sprintf("?version=%s", version)
						fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
							mapper.ValueStream{
								Customer: customer,
								Title:    title,
								Version:  version,
							},
							nil,
						)
					})

					It("returns a value stream map", func() {
						Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
						Ω(mockRecorder.Body.String()).Should(Equal(valueStreamJSON(mapper.ValueStream{
							Title:    title,
							Customer: customer,
							Version:  version,
						})))
					})
				})

				Context("when a version is not provided", func() {
					BeforeEach(func() {
						versionQuery = ""
						fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, "").Return(
							mapper.ValueStream{
								Customer: customer,
								Title:    title,
							},
							nil,
						)
					})

					It("returns a value stream map", func() {
						Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
						Ω(mockRecorder.Body.String()).Should(Equal(valueStreamJSON(mapper.ValueStream{
							Title:    title,
							Customer: customer,
						})))
					})
				})
			})
		})
	})
})

var _ = Describe("APIController#GetTitles", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", "http://example.com/v1/titles", nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		apiController := web.NewAPIController(&fakeDB, authenticator)
		server := web.NewServer(authenticator, apiController, nil, "")
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("when reading titles from the databases fails", func() {
			BeforeEach(func() {
				fakeDB.On("GetMapTitles", authentication.NewUser(authUsername, password, "qwertyu")).Return(make(map[string]string), fmt.Errorf("could not read titles from database"))
			})

			It("returns an error", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusInternalServerError))
				Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"could not read titles from database"}`))
			})
		})

		Context("when reading titles from the database is successful", func() {
			Context("when there are no maps", func() {
				BeforeEach(func() {
					fakeDB.On("GetMapTitles", authentication.NewUser(authUsername, password, "qwertyu")).Return(make(map[string]string), nil)
				})

				It("returns an empty json array", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
					Ω(mockRecorder.Body.String()).Should(Equal(`{}`))
				})
			})

			Context("when there are maps", func() {
				BeforeEach(func() {
					namedTitles := make(map[string]string)
					namedTitles["test1"] = "test1"
					namedTitles["test2"] = "test2"
					fakeDB.On("GetMapTitles", authentication.NewUser(authUsername, password, "qwertyu")).Return(namedTitles, nil)
				})

				It("returns a json array of maps", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
					Ω(mockRecorder.Body.String()).Should(Equal(`{"test1":"test1","test2":"test2"}`))
				})
			})
		})
	})
})

var _ = Describe("APIController#GetVersions", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		name         = "test-name"
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", fmt.Sprintf("http://example.com/v1/versions/%s", name), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		apiController := web.NewAPIController(&fakeDB, authenticator)
		server := web.NewServer(authenticator, apiController, nil, "")
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder).ShouldNot(BeNil())
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("when reading from that database fails", func() {
			BeforeEach(func() {
				fakeDB.On("GetVersions", authentication.NewUser(username, password, "qwertyu"), name).Return([]string{},
					fmt.Errorf("could not read from database"))
			})

			It("returns the versions that match a name", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusInternalServerError))
				Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"could not read from database"}`))
			})
		})
		Context("when there are no versions in the db", func() {
			BeforeEach(func() {
				fakeDB.On("GetVersions", authentication.NewUser(authUsername, password, "qwertyu"), name).Return(
					make([]string, 0),
					fmt.Errorf("map with name: '%s' does not exist, cannot list versions", name),
				)
			})

			It("returns the versions that match a name", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
				Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"map with name: 'test-name' does not exist, cannot list versions"}`))
			})
		})

		Context("when reading from the database is successful", func() {
			BeforeEach(func() {
				fakeDB.On("GetVersions", authentication.NewUser(authUsername, password, "qwertyu"), name).Return([]string{"2.0.0", "1.0.0", "1.0.1", "notSemver"}, nil)
			})

			It("returns the semver versions that match a name", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
				Ω(mockRecorder.Body.String()).Should(Equal(`["1.0.0","1.0.1","2.0.0"]`))
			})
		})
	})
})

var _ = Describe("APIController#SetMap", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		title        = "test1"
		requestBody  io.Reader
		customer     = "an example customer"
		name         = "test-name"
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("PUT", fmt.Sprintf("http://example.com/v1/maps/%s", name), requestBody)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		apiController := web.NewAPIController(&fakeDB, authenticator)
		server := web.NewServer(authenticator, apiController, nil, "")
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("when the request body is invalid json", func() {
			BeforeEach(func() {
				requestBody = bytes.NewReader([]byte("{]invalid"))
			})

			It("returns an error", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusInternalServerError))
				Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"invalid character ']' looking for beginning of object key string"}`))
			})
		})

		Context("when the request body is valid json", func() {
			Context("when the requested value stream map is invalid", func() {
				BeforeEach(func() {
					requestBody = bytes.NewReader([]byte(valueStreamJSON(mapper.ValueStream{
						Title:    title,
						Customer: customer,
					})))
				})

				Context("and the writing to the database fails", func() {
					It("returns an error", func() {
						Ω(mockRecorder.Code).Should(Equal(http.StatusBadRequest))
						Ω(mockRecorder.Body.String()).Should(Equal(`{"resource-validation-errors":{"empty-resources-validation-error":"maps must have at least one resource"}}`))
					})
				})
			})

			Context("when the requested value stream map is valid", func() {
				BeforeEach(func() {
					requestBody = bytes.NewReader([]byte(valueStreamJSON(mapper.ValueStream{
						Title:    title,
						Customer: customer,
						Resources: mapper.Resources{
							{
								Process: "test",
								Type:    "process",
							},
						},
					})))
				})

				Context("and the writing to the database fails", func() {
					BeforeEach(func() {
						fakeDB.On("SetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, mock.AnythingOfType("mapper.ValueStream")).Return(fmt.Errorf("could not write to database"))
					})

					It("returns an error", func() {
						Ω(mockRecorder.Code).Should(Equal(http.StatusInternalServerError))
						Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"could not write to database"}`))
					})
				})

				Context("and the writing to the database is successful", func() {
					BeforeEach(func() {
						fakeDB.On("SetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, mock.AnythingOfType("mapper.ValueStream")).Return(nil)
					})

					Context("and the value stream map has a title", func() {
						It("sets a map", func() {
							Ω(mockRecorder.Code).Should(Equal(http.StatusNoContent))
							Ω(mockRecorder.Body.String()).Should(Equal(""))
						})
					})

					Context("and the value stream map does not have a title", func() {
						BeforeEach(func() {
							requestBody = bytes.NewReader([]byte(valueStreamJSON(mapper.ValueStream{
								Customer: customer,
								Resources: mapper.Resources{
									{
										Process: "test",
										Type:    "process",
									},
								},
							})))
						})

						It("sets a map using the name as the title", func() {
							Ω(mockRecorder.Code).Should(Equal(http.StatusNoContent))
							Ω(mockRecorder.Body.String()).Should(Equal(""))
						})
					})
				})
			})
		})
	})
})

var _ = Describe("APIController#DeleteMap", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		name         = "test-name"
		versionQuery string
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("DELETE", fmt.Sprintf("http://example.com/v1/maps/%s%s", name, versionQuery), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		apiController := web.NewAPIController(&fakeDB, authenticator)
		server := web.NewServer(authenticator, apiController, nil, "")
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("when you specify a version", func() {
			BeforeEach(func() {
				version := "1.0.0"
				versionQuery = "?version=" + "1.0.0"
				fakeDB.On("DeleteMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(nil)
			})

			It("deletes a map", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
				Ω(mockRecorder.Body.String()).Should(Equal(""))
			})
		})

		Context("when you do not specify a version", func() {
			BeforeEach(func() {
				versionQuery = ""
			})

			Context("when deleting from database fails", func() {
				BeforeEach(func() {
					fakeDB.On("DeleteMap", authentication.NewUser(authUsername, password, "qwertyu"), name, "").Return(fmt.Errorf("could not delete from database"))
				})

				It("returns an error", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusInternalServerError))
					Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"could not delete from database"}`))
				})
			})

			Context("when deleting from database fails because the map doesn't exist", func() {
				BeforeEach(func() {
					fakeDB.On("DeleteMap", authentication.NewUser(authUsername, password, "qwertyu"), name, "").Return(fmt.Errorf("it did not exist"))
				})

				It("returns an error", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
					Ω(mockRecorder.Body.String()).Should(Equal(`{"error":"it did not exist"}`))
				})
			})

			Context("when deleting from database is successful", func() {
				BeforeEach(func() {
					fakeDB.On("DeleteMap", authentication.NewUser(authUsername, password, "qwertyu"), name, "").Return(nil)
				})

				It("deletes a map", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
					Ω(mockRecorder.Body.String()).Should(Equal(""))
				})
			})
		})
	})
})
