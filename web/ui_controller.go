package web

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/db"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	"github.com/gorilla/mux"
)

// UIController contains the actions relating to UI elements
type UIController struct {
	DB                db.VsmDB
	Authenticator     authentication.AuthenticatorMiddleware
	Templates         *template.Template
	TemplateVariables struct{ Year int }
	BaseDir           string
}

// NewUIController creates a controller with a VsmDB interface
func NewUIController(db db.VsmDB, authenticator authentication.AuthenticatorMiddleware, baseDir string) *UIController {
	controller := &UIController{
		DB:            db,
		Authenticator: authenticator,
	}

	var templatePath string

	if baseDir != "" {
		templatePath = baseDir + "templates/*"
	} else {
		templatePath = "./templates/*"
	}

	glob, err := template.ParseGlob(templatePath)
	controller.Templates = template.Must(glob, err)
	controller.TemplateVariables = struct{ Year int }{Year: time.Now().Year()}

	return controller
}

func (c *UIController) mapPage(w http.ResponseWriter, r *http.Request) {
	username, _, _ := r.BasicAuth()
	name := mux.Vars(r)["name"]
	if _, err := c.DB.GetVersions(c.Authenticator.Users[username], name); err != nil && err.Error() == fmt.Sprintf("map with name: '%s' does not exist, cannot list versions", name) {
		http.NotFound(w, r)

		return
	} else if err != nil {
		internalServerError(w, err)

		return
	}

	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	if err := c.Templates.ExecuteTemplate(w, "maps", c.TemplateVariables); err != nil {
		log.Fatal(err.Error())
	}
}

func (c *UIController) mapGraphJSON(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	name := mux.Vars(r)["name"]
	queryValues := r.URL.Query()
	version := queryValues.Get("version")
	username, _, _ := r.BasicAuth()
	valueStreamMapper, err := c.DB.GetMap(c.Authenticator.Users[username], name, version)
	if err != nil {
		internalServerError(w, err)
		return
	}

	if len(valueStreamMapper.Resources) == 0 {
		http.NotFound(w, r)
		return
	}

	if err := valueStreamMapper.CalculateValueStream(); err != nil {
		internalServerError(w, err)
		return
	}

	jointJsGraph, err := mapper.NewGraph(valueStreamMapper)
	if err != nil {
		internalServerError(w, err)
		return
	}

	jointJSJSON, err := json.Marshal(jointJsGraph)
	if err != nil {
		internalServerError(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	if _, printErr := fmt.Fprint(w, string(jointJSJSON)); printErr != nil {
		log.Fatal(printErr.Error())
	}
}

func (c *UIController) compareGraphJSON(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	name := mux.Vars(r)["name"]
	versions := []string{mux.Vars(r)["version"], mux.Vars(r)["comparison"]}
	username, _, _ := r.BasicAuth()
	maps := make([]mapper.ValueStream, 0)

	for _, version := range versions {
		valueStreamMapper, err := c.DB.GetMap(c.Authenticator.Users[username], name, version)
		if err != nil {
			internalServerError(w, err)
			return
		}

		if len(valueStreamMapper.Resources) == 0 {
			http.NotFound(w, r)
			return
		}
		if calcErr := valueStreamMapper.CalculateValueStream(); calcErr != nil {
			internalServerError(w, calcErr)
			return
		}

		maps = append(maps, valueStreamMapper)
	}

	jointJsGraph, err := mapper.NewCompareGraph(maps[0], maps[1])
	if err != nil {
		internalServerError(w, err)
		return
	}

	jointJSJSON, err := json.Marshal(jointJsGraph)
	if err != nil {
		internalServerError(w, err)
		return
	}
	w.WriteHeader(http.StatusOK)
	if _, printErr := fmt.Fprint(w, string(jointJSJSON)); printErr != nil {
		log.Fatal(printErr.Error())
	}
}

func internalServerError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	if _, printErr := fmt.Fprintln(w, err.Error()); printErr != nil {
		log.Fatal(err.Error())
	}
}

func (c *UIController) indexPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)

	if err := c.Templates.ExecuteTemplate(w, "index", c.TemplateVariables); err != nil {
		log.Fatal(err.Error())
	}
}

func (c *UIController) versionPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	version := "dev"

	if os.Getenv("BUILD_VERSION") != "" {
		version = os.Getenv("BUILD_VERSION")
	}

	json, err := json.Marshal(version)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Fatal(err.Error())

	} else {
		w.WriteHeader(http.StatusOK)
	}

	w.Write(json)
}
