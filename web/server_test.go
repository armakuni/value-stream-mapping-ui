package web_test

import (
	"net/http"
	"net/http/httptest"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/armakuni/value-stream-mapping-ui/web"
)

var _ bool = Describe("#NewServer", func() {
	var (
		server          *web.Server
		err             error
		username        = "test"
		password        = "test"
		requestUsername = "test"
		requestPassword = "test"
		r               *http.Request
		w               *httptest.ResponseRecorder
		url             string
		protocol        string
	)

	JustBeforeEach(func() {

		usersCredentials := make(authentication.UserCredentials)
		usersCredentials[username] = password

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		server = web.NewServer(
			authenticator,
			nil,
			web.NewUIController(nil, authenticator, "../"),
			"",
		)
	})

	It("creates a server", func() {
		Ω(server).Should(BeAssignableToTypeOf(&web.Server{}))
		Ω(server).ShouldNot(Equal(&web.Server{}))
		Ω(err).Should(BeNil())
	})

	Context("incoming request", func() {
		JustBeforeEach(func() {
			r = httptest.NewRequest("GET", url, nil)
			w = httptest.NewRecorder()

			r.SetBasicAuth(requestUsername, requestPassword)

			r.Header.Set("X-FORWARDED-PROTO", protocol)

			router := server.Start()
			router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte("Ran"))
			})

			router.ServeHTTP(w, r)
		})

		Context("http request", func() {

			BeforeEach(func() {
				protocol = "http"
				url = "http://example.com/"
			})

			It("sets the response code moved permanently (301)", func() {
				Ω(w.Code).Should(Equal(http.StatusMovedPermanently))
			})
			It("goes to the https version of the url", func() {
				Ω(w.HeaderMap["Location"]).Should(HaveLen(1))
				Ω(w.HeaderMap["Location"][0]).Should(Equal("https://example.com/"))
			})
		})
		Context("https request", func() {

			BeforeEach(func() {
				url = "http://example.com/"
				protocol = "https"

			})

			Context("authenticated request", func() {
				It("returns a 200", func() {
					Ω(w.Code).Should(Equal(http.StatusOK))
				})

				It("sets the hsts header", func() {
					Ω(w.HeaderMap["Strict-Transport-Security"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Strict-Transport-Security"][0]).Should(Equal("max-age=15984000; preload"))
				})
				It("sets the X-Frame-Options header", func() {
					Ω(w.HeaderMap["X-Frame-Options"]).Should(HaveLen(1))
					Ω(w.HeaderMap["X-Frame-Options"][0]).Should(Equal("SAMEORIGIN"))
				})
				It("sets the Referrer policy header", func() {
					Ω(w.HeaderMap["Referrer-Policy"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Referrer-Policy"][0]).Should(Equal("origin-when-cross-origin"))
				})
				It("sets the XSS protection header", func() {
					Ω(w.HeaderMap["X-Xss-Protection"]).Should(HaveLen(1))
					Ω(w.HeaderMap["X-Xss-Protection"][0]).Should(Equal("1; mode=block"))
				})
				It("sets the CSP header", func() {
					Ω(w.HeaderMap["Content-Security-Policy"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Content-Security-Policy"][0]).Should(Equal("default-src 'none'; script-src 'self' www.googletagmanager.com www.google-analytics.com 'sha256-hIFDGeM5FRsqb+Cjb2h0wsvyDTB4ZGAoRMVQvn28zpk='; style-src 'self' 'unsafe-inline'; img-src 'self' www.google-analytics.com; font-src 'self'; connect-src 'self'"))
				})
				It("sets the Content type options header", func() {
					Ω(w.HeaderMap["X-Content-Type-Options"]).Should(HaveLen(1))
					Ω(w.HeaderMap["X-Content-Type-Options"][0]).Should(Equal("nosniff"))
				})
				It("sets the Feature policy header", func() {
					Ω(w.HeaderMap["Feature-Policy"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Feature-Policy"][0]).Should(Equal("microphone 'none'; geolocation 'none'"))
				})
			})
			Context("unauthenticated request", func() {
				BeforeEach(func() {
					requestUsername = "wrong"
					requestPassword = "wrong"
				})

				It("returns a 401", func() {
					Ω(w.Code).Should(Equal(http.StatusUnauthorized))
				})

				It("sets the hsts header", func() {
					Ω(w.HeaderMap["Strict-Transport-Security"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Strict-Transport-Security"][0]).Should(Equal("max-age=15984000; preload"))
				})
				It("sets the X-Frame-Options header", func() {
					Ω(w.HeaderMap["X-Frame-Options"]).Should(HaveLen(1))
					Ω(w.HeaderMap["X-Frame-Options"][0]).Should(Equal("SAMEORIGIN"))
				})
				It("sets the Referrer policy header", func() {
					Ω(w.HeaderMap["Referrer-Policy"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Referrer-Policy"][0]).Should(Equal("origin-when-cross-origin"))
				})
				It("sets the XSS protection header", func() {
					Ω(w.HeaderMap["X-Xss-Protection"]).Should(HaveLen(1))
					Ω(w.HeaderMap["X-Xss-Protection"][0]).Should(Equal("1; mode=block"))
				})
				It("sets the CSP header", func() {
					Ω(w.HeaderMap["Content-Security-Policy"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Content-Security-Policy"][0]).Should(Equal("default-src 'none'; script-src 'self' www.googletagmanager.com www.google-analytics.com 'sha256-hIFDGeM5FRsqb+Cjb2h0wsvyDTB4ZGAoRMVQvn28zpk='; style-src 'self' 'unsafe-inline'; img-src 'self' www.google-analytics.com; font-src 'self'; connect-src 'self'"))
				})
				It("sets the Content type options header", func() {
					Ω(w.HeaderMap["X-Content-Type-Options"]).Should(HaveLen(1))
					Ω(w.HeaderMap["X-Content-Type-Options"][0]).Should(Equal("nosniff"))
				})
				It("sets the Feature policy header", func() {
					Ω(w.HeaderMap["Feature-Policy"]).Should(HaveLen(1))
					Ω(w.HeaderMap["Feature-Policy"][0]).Should(Equal("microphone 'none'; geolocation 'none'"))
				})
			})
		})
	})

})
