package web

import (
	"database/sql"
	"net/http"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"github.com/gorilla/mux"
	secure "github.com/srikrsna/security-headers"
)

// Server - A server object containing a controller
type Server struct {
	APIController *APIController
	UIController  *UIController
	Authenticator authentication.AuthenticatorMiddleware
	BaseDir       string
}

// DBConn - database opening interface
type DBConn func(driverName string, connectionString string) (*sql.DB, error)

// NewServer - creates a server
func NewServer(authenticator authentication.AuthenticatorMiddleware, apiController *APIController, uiController *UIController, baseDir string) *Server {
	return &Server{APIController: apiController, UIController: uiController, Authenticator: authenticator, BaseDir: baseDir}
}

// Start - starts the web server
func (s *Server) Start() *mux.Router {
	router := mux.NewRouter()

	featurePolicyHeader := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Feature-Policy", "microphone 'none'; geolocation 'none'")
			next.ServeHTTP(w, r)
		})
	}

	router.Use(featurePolicyHeader)

	securityHeaders := &secure.Secure{
		STSIncludeSubdomains: false,
		STSPreload:           true,
		STSMaxAgeSeconds:     15984000,
		FrameOption:          secure.FrameSameOrigin,
		XSSFilterBlock:       true,
		ReferrerPolicy:       secure.ReferrerOriginWhenCrossOrigin,
		ContentTypeNoSniff:   true,
	}
	router.Use(securityHeaders.Middleware())
	csp := &secure.CSP{
		Value: `default-src 'none'; script-src 'self' www.googletagmanager.com www.google-analytics.com 'sha256-hIFDGeM5FRsqb+Cjb2h0wsvyDTB4ZGAoRMVQvn28zpk='; style-src 'self' 'unsafe-inline'; img-src 'self' www.google-analytics.com; font-src 'self'; connect-src 'self'`,
	}
	router.Use(csp.Middleware())

	forceHTTPS := authentication.ForceHTTPSMiddleware{}
	router.Use(forceHTTPS.Middleware)
	authenticator := s.Authenticator.Middleware
	router.Use(authenticator)

	if s.APIController != nil {
		router.HandleFunc("/v1/maps/{name}", s.APIController.GetMap).Methods("GET")
		router.HandleFunc("/v1/titles", s.APIController.GetTitles).Methods("GET")
		router.HandleFunc("/v1/versions/{name}", s.APIController.GetVersions).Methods("GET")
		router.HandleFunc("/v1/maps/{name}", s.APIController.SetMap).Methods("PUT")
		router.HandleFunc("/v1/maps/{name}", s.APIController.DeleteMap).Methods("DELETE")
	}
	// UI
	if s.UIController != nil {
		router.HandleFunc("/maps/{name}", s.UIController.mapPage)
		router.HandleFunc("/maps/{name}/json", s.UIController.mapGraphJSON)
		router.HandleFunc("/maps/{name}/{version}/comparison/{comparison}/json", s.UIController.compareGraphJSON)
		router.HandleFunc("/version", s.UIController.versionPage)
		router.HandleFunc("/", s.UIController.indexPage)
	}

	// Assets
	router.PathPrefix("/js/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/js/", http.FileServer(http.Dir(s.normaliseAssetsPath("assets/js/")))).ServeHTTP(w, r)
	})

	router.PathPrefix("/css/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/css/", http.FileServer(http.Dir(s.normaliseAssetsPath("assets/css/")))).ServeHTTP(w, r)
	})

	router.PathPrefix("/images/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/images/", http.FileServer(http.Dir(s.normaliseAssetsPath("assets/images/")))).ServeHTTP(w, r)
	})

	router.PathPrefix("/cli/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/cli/", http.FileServer(http.Dir(s.normaliseAssetsPath("/cli/")))).ServeHTTP(w, r)
	})

	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.FileServer(http.Dir(s.normaliseAssetsPath("node_modules"))).ServeHTTP(w, r)
	})

	return router
}

func (s *Server) normaliseAssetsPath(assetPath string) string {
	if s.BaseDir != "" {
		return s.BaseDir + assetPath
	}
	return "./" + assetPath
}
