package web_test

import (
	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	"github.com/stretchr/testify/mock"
)

type fakeVsmDB struct {
	mock.Mock
}

func (db *fakeVsmDB) GetMapTitles(user authentication.User) (map[string]string, error) {
	args := db.Called(user)
	return args.Get(0).(map[string]string), args.Error(1)
}

func (db *fakeVsmDB) GetMap(user authentication.User, title string, version string) (mapper.ValueStream, error) {
	args := db.Called(user, title, version)
	return args.Get(0).(mapper.ValueStream), args.Error(1)
}

func (db *fakeVsmDB) GetVersions(user authentication.User, title string) ([]string, error) {
	args := db.Called(user, title)
	return args.Get(0).([]string), args.Error(1)
}

func (db *fakeVsmDB) SetMap(user authentication.User, name string, valueStream mapper.ValueStream) error {
	args := db.Called(user, name, valueStream)
	return args.Error(0)
}

func (db *fakeVsmDB) DeleteMap(user authentication.User, title string, version string) error {
	args := db.Called(user, title, version)
	return args.Error(0)
}
