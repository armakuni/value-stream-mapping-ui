package web

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	"github.com/blang/semver"
	"github.com/gorilla/mux"

	"bitbucket.org/armakuni/value-stream-mapping-ui/db"
)

// APIController an http controller struct containing the VsmDB
type APIController struct {
	DB            db.VsmDB
	Authenticator authentication.AuthenticatorMiddleware
}

// NewAPIController creates a controller with a VsmDB interface
func NewAPIController(db db.VsmDB, authenticator authentication.AuthenticatorMiddleware) *APIController {
	return &APIController{
		DB:            db,
		Authenticator: authenticator,
	}
}

type controllerErr struct {
	Err string `json:"error"`
}

func (err controllerErr) Error() string {
	errBytes, _ := json.Marshal(err)
	return string(errBytes)
}

// GetMap gets a value stream map from the database
func (controller *APIController) GetMap(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	name := mux.Vars(r)["name"]
	queryValues := r.URL.Query()
	version := queryValues.Get("version")
	username, _, _ := r.BasicAuth()
	valueStream, err := controller.DB.GetMap(controller.Authenticator.Users[username], name, version)

	if err != nil {
		writerInternalErr(w, err.Error())
		return
	}

	if valueStream.Title == "" {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "{}")
		return
	}

	valueStream.Version = version
	valueStreamJSON, _ := json.Marshal(valueStream)

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s", string(valueStreamJSON))
}

// GetTitles returns all unique titles in a json array
func (controller *APIController) GetTitles(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	username, _, _ := r.BasicAuth()
	namedTitles, err := controller.DB.GetMapTitles(controller.Authenticator.Users[username])
	if err != nil {
		writerInternalErr(w, err.Error())
		return
	}

	namedTitlesJSON, _ := json.Marshal(namedTitles)
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, string(namedTitlesJSON))
}

// GetVersions returns an array of versions for maps with a matching title
func (controller *APIController) GetVersions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	name := mux.Vars(r)["name"]
	username, _, _ := r.BasicAuth()
	versions, err := controller.DB.GetVersions(controller.Authenticator.Users[username], name)
	if err != nil {
		if err.Error() == fmt.Sprintf("map with name: '%s' does not exist, cannot list versions", name) {
			writerNotFoundErr(w, err.Error())
		} else {
			writerInternalErr(w, err.Error())
		}

		return
	}

	var semverVersions []semver.Version
	for _, version := range versions {
		semverVersion, err := semver.Parse(version)
		if err != nil {
			continue
		}
		semverVersions = append(semverVersions, semverVersion)
	}

	semver.Sort(semverVersions)
	var sortedVersions []string
	for _, semverVersion := range semverVersions {
		sortedVersions = append(sortedVersions, semverVersion.String())
	}

	versionsJSON, _ := json.Marshal(sortedVersions)
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, string(versionsJSON))
}

// SetMap - creates/ updates maps
func (controller *APIController) SetMap(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	name := mux.Vars(r)["name"]
	defer r.Body.Close()
	data, _ := ioutil.ReadAll(r.Body)
	var valueStream mapper.ValueStream
	if err := json.Unmarshal(data, &valueStream); err != nil {
		writerInternalErr(w, err.Error())
		return
	}

	err := valueStream.Validate()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if valueStream.Title == "" {
		valueStream.Title = name
	}

	username, _, _ := r.BasicAuth()

	if err := controller.DB.SetMap(controller.Authenticator.Users[username], name, valueStream); err != nil {
		writerInternalErr(w, err.Error())
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// DeleteMap - deletes a map from the database
func (controller *APIController) DeleteMap(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	name := mux.Vars(r)["name"]
	queryValues := r.URL.Query()
	version := queryValues.Get("version")
	username, _, _ := r.BasicAuth()
	if err := controller.DB.DeleteMap(controller.Authenticator.Users[username], name, version); err != nil {
		if strings.Contains(err.Error(), "it did not exist") {
			writerNotFoundErr(w, err.Error())
		} else {
			writerInternalErr(w, err.Error())
		}

		return
	}

	w.WriteHeader(http.StatusOK)
}

func writerInternalErr(w http.ResponseWriter, err string) {
	writerErr(w, err, http.StatusInternalServerError)
}

func writerErr(w http.ResponseWriter, err string, statusCode int) {
	w.WriteHeader(statusCode)
	fmt.Fprint(w, controllerErr{Err: err}.Error())
}

func writerNotFoundErr(w http.ResponseWriter, err string) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprint(w, controllerErr{Err: err}.Error())
}
