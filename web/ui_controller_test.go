package web_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/armakuni/value-stream-mapping-ui/db"
	"bitbucket.org/armakuni/value-stream-mapping-ui/web"
)

const baseDir = "../"

var _ = Describe("#NewUIController", func() {
	var (
		controller *web.UIController
		err        error
		username   string
		password   string
	)

	JustBeforeEach(func() {
		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		controller = web.NewUIController(&db.VsmDatabase{}, authenticator, baseDir)
	})

	BeforeEach(func() {
		username = "test"
		password = "test"
	})

	It("creates a controller", func() {
		Ω(controller).Should(BeAssignableToTypeOf(&web.UIController{}))
		Ω(controller).ShouldNot(Equal(&web.UIController{}))
		Ω(err).Should(BeNil())
	})
})

var _ = Describe("UIController#mapPage", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		name         = "test-name"
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", fmt.Sprintf("http://example.com/maps/%s", name), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		uiController := web.NewUIController(&fakeDB, authenticator, baseDir)
		server := web.NewServer(authenticator, nil, uiController, baseDir)
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
		})

		It("returns something descriptive in the body", func() {
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("the map exists", func() {

			BeforeEach(func() {
				fakeDB.On("GetVersions", authentication.NewUser(authUsername, password, "qwertyu"), name).Return(
					[]string{"0.1.0"},
					nil,
				)
			})

			It("returns ok", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
			})

			It("returns an a web page", func() {
				Ω(mockRecorder.Body.String()).Should(ContainSubstring("<body"))
			})
		})
		Context("the map does not exist", func() {
			BeforeEach(func() {
				fakeDB.On("GetVersions", authentication.NewUser(authUsername, password, "qwertyu"), name).Return(
					make([]string, 0),
					fmt.Errorf("map with name: '%s' does not exist, cannot list versions", name),
				)
			})

			It("return an not found error", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
			})

			It("returns something descriptive in the body", func() {
				Ω(mockRecorder.Body.String()).Should(Equal("404 page not found\n"))
			})
		})
	})
})

var _ = Describe("UIController#version", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", "http://example.com/version", nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		uiController := web.NewUIController(&fakeDB, authenticator, baseDir)
		server := web.NewServer(authenticator, nil, uiController, baseDir)
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
		})

		It("returns something descriptive in the body", func() {
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("build ver is set", func() {
			var buildVersion string
			BeforeEach(func() {
				buildVersion = os.Getenv("BUILD_VERSION")
				os.Setenv("BUILD_VERSION", "0.1.0")
			})

			It("returns ok", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
			})

			It("returns something descriptive in the body", func() {
				Ω(mockRecorder.Body.String()).Should(Equal("\"0.1.0\""))
			})

			AfterEach(func() {
				os.Setenv("BUILD_VERSION", buildVersion)
			})
		})
		Context("build ver is not set", func() {
			var buildVersion string
			BeforeEach(func() {
				buildVersion = os.Getenv("BUILD_VERSION")
				os.Setenv("BUILD_VERSION", "")
			})

			It("returns ok", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
			})
			It("returns something descriptive in the body", func() {
				Ω(mockRecorder.Body.String()).Should(Equal("\"dev\""))
			})

			AfterEach(func() {
				os.Setenv("BUILD_VERSION", buildVersion)
			})
		})

	})
})

var _ = Describe("UIController#index", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", fmt.Sprintf("http://example.com/"), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		uiController := web.NewUIController(&fakeDB, authenticator, baseDir)
		server := web.NewServer(authenticator, nil, uiController, baseDir)
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
		})

		It("returns something descriptive in the body", func() {
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("the base html should be loaded", func() {

			It("returns a ok status", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
			})

			It("returns an a web page", func() {
				Ω(mockRecorder.Body.String()).Should(ContainSubstring("<body"))
			})
		})
	})
})

var _ = Describe("UIController#mapGraphJSON", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		name         = "test-name"
		version      = "v0.1.0"
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", fmt.Sprintf("http://example.com/maps/%s/json?version=%s", name, version), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		uiController := web.NewUIController(&fakeDB, authenticator, baseDir)
		server := web.NewServer(authenticator, nil, uiController, baseDir)
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
		})

		It("returns something descriptive in the body", func() {
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("there is a map to load with versions", func() {
			Context("no version in request", func() {

				BeforeEach(func() {
					version = ""
					fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
						mapper.ValueStream{},
						nil,
					)
				})

				It("return an not found error", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
				})

				It("returns something descriptive in the body", func() {
					Ω(mockRecorder.Body.String()).Should(Equal("404 page not found\n"))
				})
			})
			Context("version in request", func() {

				BeforeEach(func() {
					version = "v0.1.0"

					fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
						mapper.ValueStream{
							Customer: "customer",
							Title:    "title",
							Version:  "v0.1.0",
							Resources: mapper.Resources{
								{
									Type:        "process",
									Title:       "test1",
									Description: "test",
								},
								{
									Type:        "process",
									Title:       "test2",
									Description: "test",
								},
							},
						},
						nil,
					)
				})

				It("returns a ok status", func() {
					Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
				})

				It("returns jointjs json", func() {
					Ω(mockRecorder.HeaderMap["Content-Type"]).Should(HaveLen(1))
					Ω(mockRecorder.Header().Get("Content-Type")).Should(Equal("application/json"))
				})
				It("json should unmarshal into jointjs graph", func() {
					actual := mapper.Graph{}
					err := json.Unmarshal(mockRecorder.Body.Bytes(), &actual)

					Ω(err).ShouldNot(HaveOccurred())
				})
			})
		})
		Context("there is a map to load without version", func() {
			BeforeEach(func() {
				version = ""
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
					mapper.ValueStream{
						Customer: "customer",
						Title:    "title",
						Resources: mapper.Resources{
							{
								Type:        "process",
								Title:       "test1",
								Description: "test",
							},
							{
								Type:        "process",
								Title:       "test2",
								Description: "test",
							},
						},
					},
					nil,
				)
			})

			It("returns a ok status", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
			})

			It("returns jointjs json", func() {
				Ω(mockRecorder.HeaderMap["Content-Type"]).Should(HaveLen(1))
				Ω(mockRecorder.Header().Get("Content-Type")).Should(Equal("application/json"))
			})
			It("json should unmarshal into jointjs graph", func() {
				actual := mapper.Graph{}
				err := json.Unmarshal(mockRecorder.Body.Bytes(), &actual)

				Ω(err).ShouldNot(HaveOccurred())
			})
		})
	})
})

var _ = Describe("UIController#compareGraphJSON", func() {
	var (
		req          *http.Request
		mockRecorder *httptest.ResponseRecorder
		err          error
		fakeDB       fakeVsmDB
		name         = "test-name"
		version      = "0.1.0"
		comparison   = "0.2.0"
		username     = "test"
		password     = "test"
		authUsername string
		authPassword string
	)

	JustBeforeEach(func() {
		req, err = http.NewRequest("GET", fmt.Sprintf("http://example.com/maps/%s/%s/comparison/%s/json", name, version, comparison), nil)
		if err != nil {
			defer GinkgoRecover()
			Fail(err.Error())
		}
		req.Header.Set("X-FORWARDED-PROTO", "https")
		req.SetBasicAuth(authUsername, authPassword)
		mockRecorder = httptest.NewRecorder()

		usersCredentials := make(authentication.UserCredentials)

		if username != "" && password != "" {
			usersCredentials[username] = password
			authentication.NewUser(username, password, "qwertyu")
		}

		authenticator := authentication.NewAuthenticationMiddleware("qwertyu", usersCredentials)

		Ω(err).ShouldNot(HaveOccurred())

		uiController := web.NewUIController(&fakeDB, authenticator, baseDir)
		server := web.NewServer(authenticator, nil, uiController, baseDir)
		server.Start().ServeHTTP(mockRecorder, req)
	})

	AfterEach(func() {
		fakeDB = fakeVsmDB{}
	})

	Context("when auth is invalid", func() {
		BeforeEach(func() {
			authUsername = "invalid"
			authPassword = "invalid"
		})

		It("return an unauthorized error", func() {
			Ω(mockRecorder.Code).Should(Equal(http.StatusUnauthorized))
			Ω(mockRecorder.Body.String()).Should(Equal("Unauthorized\n"))
		})
	})

	Context("when auth is valid", func() {
		BeforeEach(func() {
			authUsername = username
			authPassword = password
		})

		Context("there is a source & destination map to load", func() {
			BeforeEach(func() {
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
					mapper.ValueStream{
						Customer: "customer",
						Title:    "title",
						Version:  "0.1.0",
						Resources: mapper.Resources{
							{
								Type:        "process",
								Title:       "test1",
								Description: "test",
							},
							{
								Type:        "process",
								Title:       "test2",
								Description: "test",
							},
						},
					},
					nil,
				)
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, comparison).Return(
					mapper.ValueStream{
						Customer: "customer",
						Title:    "title",
						Version:  "0.2.0",
						Resources: mapper.Resources{
							{
								Type:        "process",
								Title:       "test1",
								Description: "test",
							},
							{
								Type:        "process",
								Title:       "test2",
								Description: "test",
							},
						},
					},
					nil,
				)
			})

			It("returns a ok status", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusOK))
			})

			It("returns jointjs json", func() {
				Ω(mockRecorder.HeaderMap["Content-Type"]).Should(HaveLen(1))
				Ω(mockRecorder.Header().Get("Content-Type")).Should(Equal("application/json"))
			})
			It("json should unmarshal into jointjs graph", func() {
				actual := mapper.Graph{}
				jsonErr := json.Unmarshal(mockRecorder.Body.Bytes(), &actual)

				Ω(jsonErr).ShouldNot(HaveOccurred())
			})
		})
		Context("there is no destination map", func() {
			BeforeEach(func() {
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
					mapper.ValueStream{
						Customer: "customer",
						Title:    "title",
						Version:  "0.1.0",
						Resources: mapper.Resources{
							{
								Type:        "process",
								Title:       "test1",
								Description: "test",
							},
							{
								Type:        "process",
								Title:       "test2",
								Description: "test",
							},
						},
					},
					nil,
				)
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, comparison).Return(
					mapper.ValueStream{},
					nil,
				)
			})

			It("return an not found error", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
			})

			It("returns something descriptive in the body", func() {
				Ω(mockRecorder.Body.String()).Should(Equal("404 page not found\n"))
			})
		})
		Context("there is no source map", func() {
			BeforeEach(func() {
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, version).Return(
					mapper.ValueStream{},
					nil,
				)
				fakeDB.On("GetMap", authentication.NewUser(authUsername, password, "qwertyu"), name, comparison).Return(
					mapper.ValueStream{
						Customer: "customer",
						Title:    "title",
						Version:  "0.2.0",
						Resources: mapper.Resources{
							{
								Type:        "process",
								Title:       "test1",
								Description: "test",
							},
							{
								Type:        "process",
								Title:       "test2",
								Description: "test",
							},
						},
					},
					nil,
				)
			})

			It("return an not found error", func() {
				Ω(mockRecorder.Code).Should(Equal(http.StatusNotFound))
			})

			It("returns something descriptive in the body", func() {
				Ω(mockRecorder.Body.String()).Should(Equal("404 page not found\n"))
			})
		})
	})
})
