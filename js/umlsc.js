/* global joint svgPanZoom Hammer SVG _ $ */

var joint = require('jointjs');
var $ = require("jquery");
var _ = require("lodash");
var svgPanZoom = require('svg-pan-zoom')
require('tooltipster');
require('backbone');
require('svg.js')
require('svg.screenbbox.js')
require('hammerjs')

var graph = new joint.dia.Graph()

var paper = new joint.dia.Paper({
  el: $('#paper'),
  gridSize: 1,
  model: graph,
  interactive: false
})

var baseWidth = 180
var lightestColor = '#FFFFFF'
var secondaryDarkColor = '#5D6D7E'
var darkestColor = '#000000'
var tertiaryDarkColor = '#CCCCCC'
var trafficLightRed = '#E55043'
var trafficLightAmber = '#E58031'
var trafficLightYellow = '#F0C52F'
var trafficLightGreen = '#35CB73'

joint.shapes.uml.SimpleTransition = joint.dia.Link.extend({
  defaults: {
    type: 'uml.SimpleTransition',
    attrs: {
      '.marker-target': {
        d: 'M 12 0 L 0 6 L 12 12 z',
        fill: secondaryDarkColor,
        stroke: secondaryDarkColor
      },
      '.connection': {
        'stroke-linejoin': 'round',
        'stroke-width': '2',
        stroke: secondaryDarkColor
      }
    },
    connector: {
      name: 'jumpover'
    },
    z: -1
  }
})

joint.shapes.uml.SimpleAutomatedTransition = joint.dia.Link.extend({
  defaults: {
    type: 'uml.SimpleAutomatedTransition',
    attrs: {
      '.marker-target': {
        d: 'M 12 0 L 0 6 L 12 12 z',
        fill: secondaryDarkColor,
        stroke: secondaryDarkColor
      },
      '.connection': {
        'stroke-linejoin': 'round',
        'stroke-linecap': 'round',
        'stroke-dasharray': '0.2,4',
        'stroke-width': '2',
        stroke: secondaryDarkColor
      }
    },
    connector: {
      name: 'jumpover'
    },
    labels: [
      {
        position: 0.5,
        attrs: {
          text: {
            text: '\uf0e7',
            'font-family': 'FontAwesome',
            fill: secondaryDarkColor,
            'font-size': 20
          },
          rect: {
            fill: '#263748'
          }
        }
      }
    ],
    z: -1
  }
})

joint.shapes.uml.DashedTransition = joint.dia.Link.extend({
  defaults: {
    type: 'uml.DashedTransition',
    attrs: {
      '.marker-target': {
        d: 'M 12 0 L 0 6 L 12 12 z',
        fill: secondaryDarkColor,
        stroke: secondaryDarkColor
      },
      '.connection': {
        'stroke-linejoin': 'round',
        'stroke-dasharray': '8,8',
        'stroke-width': '4',
        stroke: secondaryDarkColor
      }
    },
    z: -1
  }
})

joint.shapes.uml.InformationFlow = joint.shapes.basic.Generic.extend({
  markup: `<svg width="${baseWidth - 30}" height="30" x="0" y="0">
  <rect class="uml-info-flow-rect" x="0" y="0"/>
  <text class="uml-info-flow" x="50%" y="60%"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.InformationFlow',
      attrs: {
        '.uml-info-flow-rect': {
          fill: lightestColor,
          height: 30,
          width: baseWidth - 30
        },
        '.uml-info-flow': {
          fill: darkestColor,
          'font-size': 14,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        }
      },
      name: ''
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:name': this.updateName
      },
      this
    )

    this.updateName()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },
  updateName: function () {
    this.attr('.uml-info-flow/text', this.get('name'))
  }
})

joint.shapes.uml.DiffInformation = joint.shapes.basic.Generic.extend({
  markup: `<svg class="uml-diff-info" width="100%" height="30" x="0" y="0">
  <text class="uml-diff-source-version" x="0%" y="35%"/>
  <line x1="0%" y1="50%" x2="100%" y2="50%" stroke-width="2" stroke="${lightestColor}"></line>
  <text class="uml-diff-comparison-version" x="0%" y="90%"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.DiffInformation',
      attrs: {
        '.uml-diff-info': {
          fill: lightestColor,
          'font-size': 14,
          'alignment-baseline': 'auto',
          'text-anchor': 'start'
        },
        '.uml-diff-source-version': {
          fill: lightestColor,
          'font-size': 14,
          'alignment-baseline': 'auto',
          'text-anchor': 'start'
        },
        '.uml-diff-comparison-version': {
          fill: lightestColor,
          'font-size': 14,
          'alignment-baseline': 'auto',
          'text-anchor': 'start'
        }
      },
      source_version: '',
      comparison_version: ''
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:source_version': this.updateSourceVersion,
        'change:comparison_version': this.updateComparisonVersion
      },
      this
    )

    this.updateSourceVersion()
    this.updateComparisonVersion()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },
  updateSourceVersion: function () {
    this.attr('.uml-diff-source-version/text', this.get('source_version'))
  },
  updateComparisonVersion: function () {
    this.attr('.uml-diff-comparison-version/text', this.get('comparison_version'))
  }
})

joint.shapes.uml.Kaizen = joint.shapes.basic.Generic.extend({
  markup: `<svg class="uml-kaizen-svg" width="150" height="100">
  <path class="uml-kaizen" d="M 10 10 L 42 20 L 53 13 L 65 18 L 90 4 L 97 22 L 124 23 L 114 33 L 142 41 L 115 49 L 138 64 L 109 68 L 120 95 L 91 71 L 85 85 L 65 70 L 56 90 L 44 67 L 27 76 L 32 61 L 5 60 L 28 47 L 6 39 L 30 36 Z"/>
  <text class="uml-kaizen-text" x="50%" y="40%"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.Kaizen',
      attrs: {
        '.uml-kaizen': {
          fill: trafficLightYellow
        },
        '.uml-kaizen-text': {
          'font-size': 9,
          'stroke-width': 0,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        }
      },
      z: 100,
      description: '',
      step: 1
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:description': this.updateDescription
      },
      this
    )

    this.updateDescription()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },
  updateDescription: function () {
    var description = this.get('description')
    var shortDescription = wordWrapToFit(description, 16)

    this.attr('.uml-kaizen-text/text', shortDescription)
    this.attr('.uml-kaizen-svg/title', description)
    this.attr('.uml-kaizen-svg/class', 'uml-kaizen-svg tooltip')
  }
})

joint.shapes.uml.Start = joint.shapes.basic.Generic.extend({
  markup: `<svg width="${baseWidth}" height="60" x="0" y="0">
  <path class="uml-start" d="M 0 20 L ${baseWidth / 3} 0 L ${baseWidth / 3} 20 L ${baseWidth /
    3 *
    2} 0 L ${baseWidth / 3 * 2} 20 L ${baseWidth} 0 L ${baseWidth} 60 L 1 60 Z" />
  <text class="uml-start-text" x="50%" y="70%"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.Start',
      attrs: {
        '.uml-start': {
          fill: tertiaryDarkColor
        },
        '.uml-start-text': {
          'font-size': 16,
          'stroke-width': 0,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        }
      },
      customer: 'Customer',
      step: 1
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:customer': this.updateCustomer
      },
      this
    )

    this.updateCustomer()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },
  updateCustomer: function () {
    this.attr('.uml-start-text/text', this.get('customer'))
  }
})

joint.shapes.uml.Ladder = joint.shapes.basic.Generic.extend({
  markup: `<svg class="uml-laddder-svg" width="${baseWidth + 82}" height="42" x="-39" y="250">
  <line x1="0" y1="20" x2="39" y2="20" stroke-width="2" stroke="${lightestColor}"/>
  <line x1="${baseWidth + 43}" y1="20" x2="${baseWidth +
    82}" y2="20" stroke-width="2" stroke="${lightestColor}"/>
  <line x1="55" y1="20" x2="${baseWidth + 27}" y2="20" stroke-width="2" stroke="${lightestColor}"/>
  <line x1="39" y1="20" x2="39" y2="40" stroke-width="2" stroke="${lightestColor}"/>
  <line x1="${baseWidth + 43}" y1="20" x2="${baseWidth +
    43}" y2="40" stroke-width="2" stroke="${lightestColor}"/>
  <line x1="39" y1="40" x2="${baseWidth + 43}" y2="40" stroke-width="2" stroke="${lightestColor}"/>
  <text class="uml-ladder-lead-time" x="50%" y="36%"/>
  <text class="uml-ladder-process-time" x="50%" y="83%"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.Ladder',
      attrs: {
        '.uml-ladder-lead-time': {
          'font-size': 16,
          'stroke-width': 0,
          fill: lightestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-ladder-process-time': {
          'font-size': 16,
          'stroke-width': 0,
          fill: lightestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        }
      },
      events: [],
      lead_time: '',
      process_time: ''
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:lead_time': this.updateLeadTime,
        'change:process_time': this.updateProcessTime
      },
      this
    )

    this.updateLeadTime()
    this.updateProcessTime()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },
  updateLeadTime: function () {
    this.attr('.uml-ladder-lead-time/text', this.get('lead_time'))
    this.attr('.uml-ladder-lead-time/title', 'lead time')
    this.attr('.uml-ladder-lead-time/class', 'uml-ladder-lead-time tooltip')
  },

  updateProcessTime: function () {
    this.attr('.uml-ladder-process-time/text', this.get('process_time'))
    this.attr('.uml-ladder-process-time/title', 'process time')
    this.attr('.uml-ladder-process-time/class', 'uml-ladder-process-time tooltip')
  }
})

joint.shapes.uml.Node = joint.shapes.basic.Generic.extend({
  markup: `<svg class="uml-node-title-svg" width="${baseWidth + 4}" height="84" x="0" y="1">
  <rect class="uml-node-title-rect" x="2" y="2"/>
  <text class="uml-node-title" x="50%" y="40%"/>
  <text class="uml-node-processID" x="96%" y="20%" />
</svg>
<svg class="uml-node-description-svg" width="${baseWidth + 4}" height="84" y="80" x="0">
  <rect class="uml-node-description-rect" x="2" y="2"/>
  <text class="uml-node-description" x="50%" y="24%"/>
  <svg class="uml-node-nos" width="20" height="20" x="4%" y="74%">
    <ellipse cx="10" cy="7" fill="none" stroke="#000000" stroke-linecap="butt" stroke-width="1" rx="6" ry="6"/>
    <path d="M1,7 a1,1 0 0,0 18,0" fill="none" stroke="#000" stroke-width=" 1" />
  </svg>
  <text class="uml-node-nos-text" x="16%" y="88%"/>
</svg>
<svg x="${baseWidth - 20}" y="144" height="30" width="24">
  <text class="uml-node-flow-blockers" x="2" y="46%"/>
</svg>
<svg class="uml-node-lead-time-svg" width="${baseWidth + 4}" height="20" y="164" x="6">
  <rect class="uml-node-lead-time-rect" x="1" y="1" width="${baseWidth - 10}"/>
  <text class="uml-node-lead-time" x="3%" y="14"/>
</svg>
<svg class="uml-node-process-time-svg" width="${baseWidth + 4}" height="20" y="185" x="6">
  <rect class="uml-node-process-time-rect" x="1" y="1" width="${baseWidth - 10}"/>
  <text class="uml-node-process-time" x="3%" y="14"/>
  </svg>
<svg class="uml-node-activity-ratio-svg" width="${baseWidth + 4}" height="20" y="206" x="6">
  <rect class="uml-node-activity-ratio-rect" x="1" y="1" width="${baseWidth - 10}"/>
  <text class="uml-node-activity-ratio" x="3%" y="14"/>
</svg>
<svg class="uml-node-ca-ratio-svg" width="${baseWidth + 4}" height="20" y="227" x="6">
  <rect class="uml-node-ca-ratio-rect" x="1" y="1" width="${baseWidth - 10}"/>
  <text class="uml-node-ca-ratio" x="3%" y="14"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.Node',
      attrs: {
        '.uml-node-processID': {
          'font-size': 14,
          'stroke-width': 0,
          fill: lightestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'end'
        },
        '.uml-node-description': {
          'font-size': 16,
          'stroke-width': 0,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-node-title': {
          'font-size': 16,
          'stroke-width': 0,
          fill: lightestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-node-nos': {
          display: 'none'
        },
        '.uml-node-nos-text': {
          'font-size': 12,
          'stroke-width': 0,
          fill: darkestColor,
          'alignment-baseline': 'baseline',
          'text-anchor': 'start'
        },
        '.uml-node-title-rect': {
          width: baseWidth,
          height: 80,
          fill: secondaryDarkColor
        },
        '.uml-node-description-rect': {
          width: baseWidth,
          height: 80,
          fill: lightestColor
        },
        '.uml-node-lead-time-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-node-lead-time': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-node-process-time-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-node-process-time': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-node-ca-ratio-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-node-ca-ratio': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-node-activity-ratio-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-node-activity-ratio': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-node-flow-blockers': {
          'font-size': 16,
          fill: darkestColor,
          'stroke-width': 0,
          'alignment-baseline': 'baseline',
          'text-anchor': 'start',
          'font-family': 'FontAwesome'
        }
      },
      title: 'Title',
      description: 'Description',
      lead_time: '',
      process_time: '',
      'c&a_ratio': '',
      'c&a_ratio_desc': '',
      activity_ratio: '',
      processID: '',
      numberOfStaff: 0,
      flow_blockers: '',
      events: []
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:title': this.updateTitle,
        'change:processID': this.updateProcessID,
        'change:description': this.updateDescription,
        'change:lead_time': this.updateLeadTime,
        'change:process_time': this.updateProcessTime,
        'change:c&a_ratio': this.updateCARatio,
        'change:activity_ratio': this.updateActivityRatio,
        'change:numberOfStaff': this.updateNumberOfStaff,
        'change:flow_blockers': this.updateFlowBlockers
      },
      this
    )

    this.updateTitle()
    this.updateProcessID()
    this.updateDescription()
    this.updateLeadTime()
    this.updateProcessTime()
    this.updateCARatio()
    this.updateActivityRatio()
    this.updateNumberOfStaff()
    this.updateFlowBlockers()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },

  updateTitle: function () {
    var title = this.get('title')
    var shortTitle = wordWrapToFit(title, 16)

    this.attr('.uml-node-title/text', shortTitle)
    this.attr('.uml-node-title-svg/title', title)
    this.attr('.uml-node-title-svg/class', 'uml-node-title-svg tooltip')
  },

  updateProcessID: function () {
    this.attr('.uml-node-processID/text', this.get('processID'))
  },

  updateDescription: function () {
    var description = this.get('description')
    var shortDescription = wordWrapToFit(description, 16)

    this.attr('.uml-node-description/text', shortDescription)
    this.attr('.uml-node-description-svg/title', description)
    this.attr('.uml-node-description-svg/class', 'uml-node-description-svg tooltip')
  },

  updateLeadTime: function () {
    var fieldText = `lead time = ${this.get('lead_time')}`
    this.attr('.uml-node-lead-time/text', fieldText)
    this.attr('.uml-node-lead-time-svg/title', fieldText)
    this.attr('.uml-node-lead-time-svg/class', 'uml-node-lead-time-svg tooltip')
  },

  updateProcessTime: function () {
    var fieldText = `process time = ${this.get('process_time')}`
    this.attr('.uml-node-process-time/text', fieldText)
    this.attr('.uml-node-process-time-svg/title', fieldText)
    this.attr('.uml-node-process-time-svg/class', 'uml-node-process-time-svg tooltip')
  },

  updateCARatio: function () {
    var fieldText = `c&a ratio = ${this.get('c&a_ratio')}`
    var title
    if (this.get('c&a_ratio_desc')) {
      title = this.get('c&a_ratio_desc')
    } else {
      title = fieldText
    }
    this.attr('.uml-node-ca-ratio/text', fieldText)
    this.attr('.uml-node-ca-ratio-svg/title', title)
    this.attr('.uml-node-ca-ratio-svg/class', 'uml-node-ca-ratio-svg tooltip')
    this.attr('.uml-node-ca-ratio-rect/fill', calculateFill(this.get('c&a_ratio')))
  },

  updateActivityRatio: function () {
    var fieldText = `activity ratio = ${this.get('activity_ratio')}`
    this.attr('.uml-node-activity-ratio/text', fieldText)
    this.attr('.uml-node-activity-ratio-svg/title', fieldText)
    this.attr('.uml-node-activity-ratio-svg/class', 'uml-node-activity-ratio-svg tooltip')
    this.attr('.uml-node-activity-ratio-rect/fill', calculateFill(this.get('activity_ratio')))
  },

  updateNumberOfStaff: function () {
    var numberOfStaff = this.get('numberOfStaff')
    if (numberOfStaff > 0) {
      this.attr('.uml-node-nos-text/text', numberOfStaff)
      this.attr('.uml-node-nos/display', 'block')
    }
  },

  updateFlowBlockers: function () {
    var flowBlockersText = this.get('flow_blockers')
    if (String(flowBlockersText) !== '') {
      this.attr('.uml-node-flow-blockers/text', '\uf071')
      this.attr('.uml-node-flow-blockers/title', flowBlockersText)
      this.attr('.uml-node-flow-blockers/class', 'uml-node-flow-blockers tooltip')
    }
  }
})

joint.shapes.uml.Summary = joint.shapes.basic.Generic.extend({
  markup: `<svg class="uml-summary-svg" width="${baseWidth + 60}" height="25" y="0" x="0">
  <rect class="uml-summary-rect" x="0" y="0" width="${baseWidth + 60}"/>
  <text class="uml-summary" x="3%" y="14">Summary</text>
</svg>
<svg class="uml-summary-lead-time-svg" width="${baseWidth + 60}" height="20" y="27" x="0">
  <rect class="uml-summary-lead-time-rect" x="0" y="0" width="${baseWidth + 60}"/>
  <text class="uml-summary-lead-time" x="3%" y="14"/>
</svg>
<svg class="uml-summary-process-time-svg" width="${baseWidth + 60}" height="20" y="49" x="0">
  <rect class="uml-summary-process-time-rect" x="0" y="0" width="${baseWidth + 60}"/>
  <text class="uml-summary-process-time" x="3%" y="14"/>
</svg>
<svg class="uml-summary-activity-ratio-svg" width="${baseWidth + 60}" height="20" y="71" x="0">
  <rect class="uml-summary-activity-ratio-rect" x="0" y="0" width="${baseWidth + 60}"/>
  <text class="uml-summary-activity-ratio" x="3%" y="14"/>
</svg>
<svg class="uml-summary-ca-ratio-svg" width="${baseWidth + 60}" height="20" y="93" x="0">
  <rect class="uml-summary-ca-ratio-rect" x="0" y="0" width="${baseWidth + 60}"/>
  <text class="uml-summary-ca-ratio" x="3%" y="14"/>
</svg>`,

  defaults: _.defaultsDeep(
    {
      type: 'uml.Summary',
      attrs: {
        '.uml-summary-rect': {
          height: 30,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle',
          fill: secondaryDarkColor
        },
        '.uml-summary': {
          'font-size': 15,
          fill: lightestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-summary-lead-time-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-summary-lead-time': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-summary-process-time-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-summary-process-time': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-summary-ca-ratio-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-summary-ca-ratio': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        },
        '.uml-summary-activity-ratio-rect': {
          height: 20,
          'font-size': 12,
          'stroke-width': 0,
          'alignment-baseline': 'middle',
          'text-anchor': 'middle'
        },
        '.uml-summary-activity-ratio': {
          'font-size': 13,
          fill: darkestColor,
          'alignment-baseline': 'middle',
          'text-anchor': 'start'
        }
      },
      lead_time: '',
      process_time: '',
      'c&a_ratio': '',
      'c&a_ratio_desc': '',
      activity_ratio: '',
      events: []
    },
    joint.shapes.basic.Generic.prototype.defaults
  ),

  initialize: function () {
    this.on(
      {
        'change:lead_time': this.updateLeadTime,
        'change:process_time': this.updateProcessTime,
        'change:c&a_ratio': this.updateCARatio,
        'change:activity_ratio': this.updateActivityRatio
      },
      this
    )

    this.updateLeadTime()
    this.updateProcessTime()
    this.updateCARatio()
    this.updateActivityRatio()

    joint.shapes.basic.Generic.prototype.initialize.apply(this, arguments)
  },

  updateLeadTime: function () {
    var fieldText = `total lead time = ${this.get('lead_time')}`
    this.attr('.uml-summary-lead-time/text', fieldText)
    this.attr('.uml-summary-lead-time-svg/title', fieldText)
  },

  updateProcessTime: function () {
    var fieldText = `total process time = ${this.get('process_time')}`
    this.attr('.uml-summary-process-time/text', fieldText)
    this.attr('.uml-summary-process-time-svg/title', fieldText)
  },

  updateCARatio: function () {
    var fieldText = `average c&a ratio = ${this.get('c&a_ratio')}`
    this.attr('.uml-summary-ca-ratio/text', fieldText)
    this.attr('.uml-summary-ca-ratio-svg/title', fieldText)
    this.attr('.uml-summary-ca-ratio-rect/fill', calculateFill(this.get('c&a_ratio')))
  },

  updateActivityRatio: function () {
    var fieldText = `overall activity ratio = ${this.get('activity_ratio')}`
    this.attr('.uml-summary-activity-ratio/text', fieldText)
    this.attr('.uml-summary-activity-ratio-svg/title', fieldText)
    this.attr('.uml-summary-activity-ratio-rect/fill', calculateFill(this.get('activity_ratio')))
  }
})

function calculateFill(percentValue) {
  var percentFloat = parseFloat(percentValue)
  var fill = lightestColor
  if (percentFloat >= 0 && percentFloat < 25) {
    fill = trafficLightRed
  } else if (percentFloat >= 25 && percentFloat < 50) {
    fill = trafficLightAmber
  } else if (percentFloat >= 50 && percentFloat < 75) {
    fill = trafficLightYellow
  } else if (percentFloat >= 75) {
    fill = trafficLightGreen
  }
  return fill
}

// Get the div that will hold the graph
var targetElement = $('#paper')[0]

function hideLeadTime() {
  var leadTimes = document.getElementsByClassName('uml-node-lead-time-svg')
  var leadTimeY

  for (let i = 0; i < leadTimes.length; i++) {
    var leadTime = leadTimes[i]
    leadTime.style.display = 'none'
    leadTimeY = leadTime.getAttribute('y')
  }
  var summaryLeadTimes = document.getElementsByClassName('uml-summary-lead-time-svg')
  var sumaryLeadTimeY

  for (let i = 0; i < summaryLeadTimes.length; i++) {
    var sumaryLeadTime = summaryLeadTimes[i]
    sumaryLeadTime.style.display = 'none'
    sumaryLeadTimeY = sumaryLeadTime.getAttribute('y') - 20
  }
  var processTimes = document.getElementsByClassName('uml-node-process-time-svg')
  var processTimeY
  for (let i = 0; i < processTimes.length; i++) {
    var processTime = processTimes[i]
    processTimeY = processTime.getAttribute('y')
    processTime.setAttribute('y', leadTimeY)
  }
  var activityRatios = document.getElementsByClassName('uml-node-activity-ratio-svg')
  var activityRatioY

  for (let i = 0; i < activityRatios.length; i++) {
    var activityRatio = activityRatios[i]
    activityRatioY = activityRatio.getAttribute('y')
    activityRatio.setAttribute('y', processTimeY)
  }
  var caRatios = document.getElementsByClassName('uml-node-ca-ratio-svg')
  for (let i = 0; i < caRatios.length; i++) {
    var caRatio = caRatios[i]
    caRatio.setAttribute('y', activityRatioY)
  }
  var summaryProcessTimes = document.getElementsByClassName('uml-summary-process-time-svg')
  var summaryProcessTimeY

  for (let i = 0; i < summaryProcessTimes.length; i++) {
    var summaryProcessTime = summaryProcessTimes[i]
    summaryProcessTimeY = summaryProcessTime.getAttribute('y') - 20
    summaryProcessTime.setAttribute('y', sumaryLeadTimeY)
  }
  var summaryActivityRatios = document.getElementsByClassName('uml-summary-activity-ratio-svg')
  var summaryActivityRatioY

  for (let i = 0; i < summaryActivityRatios.length; i++) {
    var summaryActivityRatio = summaryActivityRatios[i]
    summaryActivityRatioY = summaryActivityRatio.getAttribute('y') - 20
    summaryActivityRatio.setAttribute('y', summaryProcessTimeY)
  }
  var summaryCARatios = document.getElementsByClassName('uml-summary-ca-ratio-svg')
  for (let i = 0; i < summaryCARatios.length; i++) {
    var summaryCARatio = summaryCARatios[i]
    summaryCARatio.setAttribute('y', summaryActivityRatioY)
  }
  var summarySVGs = document.getElementsByClassName('uml-summary-svg')
  for (let i = 0; i < summarySVGs.length; i++) {
    var summarySVG = summarySVGs[i]
    var summarySVGY = summarySVG.getAttribute('y') - 20
    summarySVG.setAttribute('y', String(summarySVGY))
  }
}

function hideProcessTime() {
  var processTimes = document.getElementsByClassName('uml-node-process-time-svg')
  var processTimeY
  for (let i = 0; i < processTimes.length; i++) {
    var processTime = processTimes[i]
    processTime.style.display = 'none'
    processTimeY = processTime.getAttribute('y')
  }
  var summaryProcessTimes = document.getElementsByClassName('uml-summary-process-time-svg')
  var summaryProcessTimeY

  for (let i = 0; i < summaryProcessTimes.length; i++) {
    var summaryProcessTime = summaryProcessTimes[i]
    summaryProcessTime.style.display = 'none'
    summaryProcessTimeY = summaryProcessTime.getAttribute('y') - 20
  }
  var activityRatios = document.getElementsByClassName('uml-node-activity-ratio-svg')
  var activityRatioY

  for (let i = 0; i < activityRatios.length; i++) {
    var activityRatio = activityRatios[i]
    activityRatioY = activityRatio.getAttribute('y')
    activityRatio.setAttribute('y', processTimeY)
  }
  var caRatios = document.getElementsByClassName('uml-node-ca-ratio-svg')
  for (let i = 0; i < caRatios.length; i++) {
    var caRatio = caRatios[i]
    caRatio.setAttribute('y', activityRatioY)
  }
  var summaryActivityRatios = document.getElementsByClassName('uml-summary-activity-ratio-svg')
  var summaryActivityRatioY

  for (let i = 0; i < summaryActivityRatios.length; i++) {
    var summaryActivityRatio = summaryActivityRatios[i]
    summaryActivityRatioY = summaryActivityRatio.getAttribute('y') - 20
    summaryActivityRatio.setAttribute('y', summaryProcessTimeY)
  }
  var summaryCARatios = document.getElementsByClassName('uml-summary-ca-ratio-svg')
  for (let i = 0; i < summaryCARatios.length; i++) {
    var summaryCARatio = summaryCARatios[i]
    summaryCARatio.setAttribute('y', summaryActivityRatioY)
  }
  var summarySVGs = document.getElementsByClassName('uml-summary-svg')
  for (let i = 0; i < summarySVGs.length; i++) {
    var summarySVG = summarySVGs[i]
    var summarySVGY = summarySVG.getAttribute('y') - 20
    summarySVG.setAttribute('y', String(summarySVGY))
  }
  var summaryLeadTimes = document.getElementsByClassName('uml-summary-lead-time-svg')
  for (let i = 0; i < summaryLeadTimes.length; i++) {
    var summaryLeadTime = summaryLeadTimes[i]
    var summaryLeadTimeY = summaryLeadTime.getAttribute('y') - 20
    summaryLeadTime.setAttribute('y', String(summaryLeadTimeY))
  }
  var ladders = document.getElementsByClassName('uml-laddder-svg')
  for (let i = 0; i < ladders.length; i++) {
    var ladder = ladders[i]
    var ladderY = ladder.getAttribute('y') - 20
    ladder.setAttribute('y', String(ladderY))
  }
}

function hideActivityRatio() {
  var activityRatios = document.getElementsByClassName('uml-node-activity-ratio-svg')
  var activityRatioY

  for (let i = 0; i < activityRatios.length; i++) {
    var activityRatio = activityRatios[i]
    activityRatio.style.display = 'none'
    activityRatioY = activityRatio.getAttribute('y')
  }
  var summaryActivityRatios = document.getElementsByClassName('uml-summary-activity-ratio-svg')
  var summaryActivityRatioY

  for (let i = 0; i < summaryActivityRatios.length; i++) {
    var summaryActivityRatio = summaryActivityRatios[i]
    summaryActivityRatio.style.display = 'none'
    summaryActivityRatioY = summaryActivityRatio.getAttribute('y') - 20
  }
  var caRatios = document.getElementsByClassName('uml-node-ca-ratio-svg')
  for (let i = 0; i < caRatios.length; i++) {
    var caRatio = caRatios[i]
    caRatio.setAttribute('y', activityRatioY)
  }
  var summaryCARatios = document.getElementsByClassName('uml-summary-ca-ratio-svg')
  for (let i = 0; i < summaryCARatios.length; i++) {
    var summaryCARatio = summaryCARatios[i]
    summaryCARatio.setAttribute('y', summaryActivityRatioY)
  }
  var summarySVGs = document.getElementsByClassName('uml-summary-svg')
  for (let i = 0; i < summarySVGs.length; i++) {
    var summarySVG = summarySVGs[i]
    var summarySVGY = summarySVG.getAttribute('y') - 20
    summarySVG.setAttribute('y', String(summarySVGY))
  }
  var summaryLeadTimes = document.getElementsByClassName('uml-summary-lead-time-svg')
  for (let i = 0; i < summaryLeadTimes.length; i++) {
    var summaryLeadTime = summaryLeadTimes[i]
    var summaryLeadTimeY = summaryLeadTime.getAttribute('y') - 20
    summaryLeadTime.setAttribute('y', String(summaryLeadTimeY))
  }
  var ladders = document.getElementsByClassName('uml-laddder-svg')
  for (let i = 0; i < ladders.length; i++) {
    var ladder = ladders[i]
    var ladderY = ladder.getAttribute('y') - 20
    ladder.setAttribute('y', String(ladderY))
  }
}

function hideCARatio() {
  var caRatios = document.getElementsByClassName('uml-node-ca-ratio-svg')
  for (let i = 0; i < caRatios.length; i++) {
    var caRatio = caRatios[i]
    caRatio.style.display = 'none'
  }
  var summaryCARatios = document.getElementsByClassName('uml-summary-ca-ratio-svg')
  for (let i = 0; i < summaryCARatios.length; i++) {
    var summaryCARatio = summaryCARatios[i]
    summaryCARatio.style.display = 'none'
  }
  var summarySVGs = document.getElementsByClassName('uml-summary-svg')
  for (let i = 0; i < summarySVGs.length; i++) {
    var summarySVG = summarySVGs[i]
    var summarySVGY = summarySVG.getAttribute('y') - 20
    summarySVG.setAttribute('y', summarySVGY)
  }
  var summaryLeadTimes = document.getElementsByClassName('uml-summary-lead-time-svg')
  for (let i = 0; i < summaryLeadTimes.length; i++) {
    var summaryLeadTime = summaryLeadTimes[i]
    var summaryLeadTimeY = summaryLeadTime.getAttribute('y') - 20
    summaryLeadTime.setAttribute('y', summaryLeadTimeY)
  }
  var summaryProcessTimes = document.getElementsByClassName('uml-summary-process-time-svg')
  for (let i = 0; i < summaryProcessTimes.length; i++) {
    var summaryProcessTime = summaryProcessTimes[i]
    var summaryProcessTimeY = summaryProcessTime.getAttribute('y') - 20
    summaryProcessTime.setAttribute('y', summaryProcessTimeY)
  }
  var summaryActivityRatios = document.getElementsByClassName('uml-summary-activity-ratio-svg')
  for (let i = 0; i < summaryActivityRatios.length; i++) {
    var summaryActivityRatio = summaryActivityRatios[i]
    var summaryActivityRatioY = summaryActivityRatio.getAttribute('y') - 20
    summaryActivityRatio.setAttribute('y', summaryActivityRatioY)
  }
  var ladders = document.getElementsByClassName('uml-laddder-svg')
  for (let i = 0; i < ladders.length; i++) {
    var ladder = ladders[i]
    var ladderY = ladder.getAttribute('y') - 20
    ladder.setAttribute('y', ladderY)
  }
}

function hideSummary() {
  var summarys = document.getElementsByClassName('uml-summary-svg')
  for (let i = 0; i < summarys.length; i++) {
    var summary = summarys[i]
    summary.style.display = 'none'
  }
}

function hideLadder() {
  var ladders = document.getElementsByClassName('uml-laddder-svg')
  for (let i = 0; i < ladders.length; i++) {
    var ladder = ladders[i]
    ladder.style.display = 'none'
  }
}

var nonPaperPixelHeight = 97

function unhideLeadTimeRatioBar(panAndZoom) {
  nonPaperPixelHeight = nonPaperPixelHeight + 50
  document.getElementById('ratio-container').style.display = 'block'
  setDefaultPaperDimensions()
  zoomToFit(panAndZoom)
}

function hasMetric(json, metric) {
  var counter = 0
  for (let cellNum in json['cells']) {
    var cell = json['cells'][cellNum]
    if (cell[metric] && String(cell['type']) === 'uml.Node') {
      counter++
    }
  }
  if (counter > 0) {
    return true
  }
  return false
}

function hideEmptyMetrics(json) {
  var atLeastOneMetric = false
  if (!hasMetric(json, 'lead_time')) {
    hideLeadTime()
    hideLadder()
  } else {
    atLeastOneMetric = true
  }
  if (!hasMetric(json, 'process_time')) {
    hideProcessTime()
  } else {
    atLeastOneMetric = true
  }
  if (!hasMetric(json, 'process_time') || !hasMetric(json, 'lead_time')) {
    hideActivityRatio()
  }
  if (!hasMetric(json, 'c&a_ratio')) {
    hideCARatio()
  } else {
    atLeastOneMetric = true
  }
  if (!atLeastOneMetric) {
    hideSummary()
  }
}

var eventsHandler = {
  haltEventListeners: ['touchstart', 'touchend', 'touchmove', 'touchleave', 'touchcancel'],
  init: function (options) {
    var instance = options.instance
    var initialScale = 1
    var pannedX = 0
    var pannedY = 0
    // Init Hammer
    // Listen only for pointer and touch events
    this.hammer = Hammer(options.svgElement, {
      inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput
    })
    // Enable pinch
    this.hammer.get('pinch').set({
      enable: true
    })
    // Handle double tap
    this.hammer.on('doubletap', function () {
      instance.zoomIn()
    })
    // Handle pan
    this.hammer.on('panstart panmove', function (ev) {
      // On pan start reset panned variables
      if (String(ev.type) === 'panstart') {
        pannedX = 0
        pannedY = 0
      }
      // Pan only the difference
      instance.panBy({
        x: ev.deltaX - pannedX,
        y: ev.deltaY - pannedY
      })
      pannedX = ev.deltaX
      pannedY = ev.deltaY
    })
    // Handle pinch
    this.hammer.on('pinchstart pinchmove', function (ev) {
      // On pinch start remember initial zoom
      if (String(ev.type) === 'pinchstart') {
        initialScale = instance.getZoom()
        instance.zoom(initialScale * ev.scale)
      }
      instance.zoom(initialScale * ev.scale)
    })
    // Prevent moving the page on some devices when panning over SVG
    options.svgElement.addEventListener('touchmove', function (e) {
      e.preventDefault()
    })
  },
  destroy: function () {
    this.hammer.destroy()
  }
}

function updateTitle(title) {
  $('#title h1').text(title)
}

function updateWorkingDay(workingDay) {
  if (workingDay !== undefined) {
    $('#working-day').text(`Working Day: ${workingDay}`)
  } else {
    $('#working-day').text(`Working Day: 8h`)
  }
}

function updateWorkingWeek(workingWeek) {
  if (workingWeek !== undefined) {
    $('#working-week').text(`Working Week: ${workingWeek}`)
  } else {
    $('#working-week').text(`Working Week: 5d`)
  }
}

function updateSelectVersion(versions, mapName, currentVersion) {
  if (versions) {
    var form = document.createElement('form')
    form.id = 'change-version'
    var select = document.createElement('select')

    for (let i = 0; i < versions.length; i++) {
      var version = versions[i]
      var option = document.createElement('option')
      option.value = version
      option.text = version
      if (String(version) === String(currentVersion)) {
        option.selected = true
      }
      select.appendChild(option)
    }

    select.onchange = function () {
      var version = $('#change-version :selected').text()
      window.location.href = `?version=${version}`
    }
    form.appendChild(select)
    document.getElementById('select-version-selector').appendChild(form)
  }
}

function updateCompareVersion(versions, mapName, currentVersion, compareVersion) {
  if (versions) {
    var form = document.createElement('form')
    form.id = 'compare-version'
    var select = document.createElement('select')

    for (let i = 0; i < versions.length; i++) {
      var version = versions[i]
      var option = document.createElement('option')
      option.value = version
      option.text = version

      if (String(version) === String(compareVersion)) {
        option.selected = true
      }

      if (String(version) !== String(currentVersion)) {
        select.appendChild(option)
      }
    }

    var noComparisonOption = document.createElement('option')
    noComparisonOption.text = 'Diff ⇄'

    if (compareVersion === undefined) {
      noComparisonOption.selected = true
    }
    select.appendChild(noComparisonOption)

    select.onchange = function (e) {
      var comparedVersion = $(':selected', this).val()
      var qs = []

      if (comparedVersion !== noComparisonOption.text) {
        qs.push(`compare=${encodeURIComponent(comparedVersion)}`)
      }

      if (getUrlVars()['version'] !== undefined) {
        qs.push(`version=${getUrlVars()['version']}`)
      }

      if (qs.length > 0) {
        window.location.href = `?` + qs.join('&')
      } else {
        window.location = window.location.href.split('?')[0]
      }

      e.preventDefault()
    }

    form.appendChild(select)
    document.getElementById('compare-version-selector').appendChild(form)
  }
}

function getUrlVars() {
  var vars = []
  var hash
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=')
    vars.push(hash[0])
    vars[hash[0]] = hash[1]
  }
  return vars
}

function load() {
  var mapName = window.location.pathname.split('/').pop()
  var versionQuery = ''
  var currentVersion = getUrlVars()['version']
  var compareVersion = getUrlVars()['compare']
  $.getJSON(`/v1/versions/${mapName}`)
    .then(function (json) {
      var versions = json
      var latestVersion
      if (versions) {
        latestVersion = versions[versions.length - 1]
      }

      if (currentVersion === undefined) {
        currentVersion = latestVersion
      }

      if (currentVersion !== undefined) {
        versionQuery = `?version=${encodeURIComponent(currentVersion)}`
        updateSelectVersion(versions, mapName, currentVersion)
        updateCompareVersion(versions, mapName, currentVersion, compareVersion)
      }
    })
    .then(function () {
      return $.getJSON(`/v1/maps/${mapName}${versionQuery}`).then(function (json) {
        updateTitle(json['title'])
        updateWorkingDay(json['working_day'])
        updateWorkingWeek(json['working_week'])

        var graphJsonUrl = `${window.location.pathname}/json${versionQuery}`

        if (currentVersion !== undefined && compareVersion !== undefined) {
          graphJsonUrl = `${window.location.pathname}/${encodeURIComponent(currentVersion)}/comparison/${encodeURIComponent(compareVersion)}/json`
        }

        return $.getJSON(graphJsonUrl).then(function (json) {
          graph.fromJSON(json)
          hideEmptyMetrics(json)

          setDefaultPaperDimensions()

          var panAndZoom = svgPanZoom(targetElement.childNodes[2], {
            viewportSelector: targetElement.childNodes[2].childNodes[0],
            center: true,
            fit: true,
            minZoom: 0.2,
            zoomScaleSensitivity: 0.2,
            customEventsHandler: eventsHandler
          })

          paper.scaleContentToFit({
            maxScale: 1,
            minScale: 0.2
          })

          $(window).resize(function () {
            setDefaultPaperDimensions()
            zoomToFit(panAndZoom)
          })

          if (hasMetric(json, 'lead_time') && compareVersion === undefined) {
            if (!document.getElementById('ratio')) {
              var ratioDiv = document.createElement('div')
              ratioDiv.id = 'ratio'
              document.getElementById('ratio-container').appendChild(ratioDiv)
            }
            for (var cellNum in json['cells']) {
              var cell = json['cells'][cellNum]
              if (!cell['lead_time_weight'] || !cell['longest_path']) {
                continue
              }
              var leadTimeWeight = cell['lead_time_weight']
              var cellID = `ratio-bar-${cell['id']}`
              var cellDiv

              if (document.getElementById(cellID)) {
                cellDiv = document.getElementById(cellID)
              } else {
                cellDiv = document.createElement('div')
              }
              cellDiv.id = cellID
              cellDiv.className = 'tooltip'
              cellDiv.title = cell['title']
              cellDiv.style.width = `calc(${leadTimeWeight}% - 2px)`
              cellDiv.style.background = calculateFill(cell['activity_ratio'])

              cellDiv.onclick = panToCellCallback(cell, panAndZoom)

              if (!document.getElementById(cellID)) {
                document.getElementById('ratio').appendChild(cellDiv)
              }

              var svgComponents = document.querySelectorAll(`*[model-id='${cell['id']}'] > svg`)

              for (var index = 0; index < svgComponents.length; ++index) {
                svgComponents[index].onclick = panToCellCallback(cell, panAndZoom)
              }
            }
            unhideLeadTimeRatioBar(panAndZoom)
          }
          $('.tooltip').tooltipster({
            trigger: 'custom',
            triggerOpen: {
              mouseenter: true,
              tap: true,
              touchstart: true
            },
            triggerClose: {
              mouseleave: true,
              tap: true,
              touchleave: true
            }
          })

          $('#burger-button').click(function () {
            $('#burger-content').toggle()
          })

          $('#re-centre-button').click(function () {
            setDefaultPaperDimensions()
            zoomToFit(panAndZoom)
          })

          $('#home-button').click(function () {
            window.location.href = '/'
          })

          $('#print-button').click(function () {
            var realZoom = panAndZoom.getSizes().realZoom
            var printWidth = mmToPx(245)
            var printHeight = mmToPx(120)
            var viewBoxWidth = panAndZoom.getSizes().viewBox.width
            var viewBoxHeight = panAndZoom.getSizes().viewBox.height
            var scaler = Math.min(printWidth / viewBoxWidth, printHeight / viewBoxHeight)
            panAndZoom.zoom(scaler * (1 / realZoom))
            panAndZoom.pan({
              x: 60,
              y: 80
            })
            $(document).ready(function () {
              window.print()
              setDefaultPaperDimensions()
              zoomToFit(panAndZoom)
            })
          })
        })
      })
    })
}

function panToCellCallback(cell, panAndZoom) {
  return function (ev) {
    ev.preventDefault()

    var graphBB = graph.getCell(cell['id']).getBBox()
    var svgBB = SVG.select(`*[model-id='${cell['id']}'] .uml-node-description-svg`).bbox()
    var sizes = panAndZoom.getSizes()

    panAndZoom.pan(
      {
        x: -((graphBB.x + (svgBB.width / 2)) * sizes.realZoom) + (sizes.width / 2),
        y: -((graphBB.y + (svgBB.height / 2)) * sizes.realZoom) + (sizes.height / 2)
      }
    )

    return false
  }
}

function setDefaultPaperDimensions() {
  paper.setDimensions(window.innerWidth - 16, window.innerHeight - nonPaperPixelHeight)
}

function zoomToFit(panAndZoom) {
  panAndZoom.resize()
  panAndZoom.fit()
  panAndZoom.center()
}

function mmToPx(mm) {
  var div = document.createElement('div')
  div.style.display = 'block'
  div.style.height = '100mm'
  div.style.visibility = 'hidden'
  document.body.appendChild(div)
  var convert = div.offsetHeight * mm / 100
  div.parentNode.removeChild(div)
  return convert
}

function wordWrapToFit(text, breakPoint) {
  var splitText = text.match(/\S*\S/g)
  var processedSplitText = []
  for (let i = 0; i < splitText.length; i++) {
    var split = splitText[i]
    var processedLength = processedSplitText.length
    if (processedLength > 3) {
      break
    }
    if (processedLength === 3) {
      breakPoint = breakPoint - 3
    }
    if (
      processedLength === 0 ||
      processedSplitText[processedLength - 1].length + split.length + 1 > breakPoint
    ) {
      processedSplitText.push(split)
    } else {
      var lastProcessedSplitText = processedSplitText[processedLength - 1]
      processedSplitText[processedLength - 1] = `${lastProcessedSplitText} ${split}`
    }
  }
  var shortText = ''
  if (processedSplitText[0] != null) {
    shortText = processedSplitText[0]
  }
  if (processedSplitText[1] != null) {
    shortText = `${shortText}\n${processedSplitText[1]}`
  }
  if (processedSplitText[2] != null) {
    shortText = `${shortText}\n${processedSplitText[2]}`
  }
  if (processedSplitText[3] != null) {
    shortText = `${shortText}...`
  }
  return shortText
}

load()
