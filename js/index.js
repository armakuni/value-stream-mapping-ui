/* global $ jQuery document window */

var $ = require("jquery");

$.getJSON(`${window.location.origin}/v1/titles`, function (json) {
  $.each(json, function (key, value) {
    var newline = document.createElement('br')
    var link = document.createElement('a')
    link.href = 'maps/' + key
    link.text = value
    document.getElementById('titles').appendChild(link)
    document.getElementById('titles').appendChild(newline)
  })
})
