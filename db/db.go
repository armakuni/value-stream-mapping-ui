package db

import (
	"database/sql"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	"github.com/cloudfoundry-community/go-cfenv"
	"github.com/mattes/migrate"
	"github.com/mattes/migrate/database/postgres"

	sq "github.com/Masterminds/squirrel"
	// Used by the migrations
	_ "github.com/mattes/migrate/source/file"
)

// VsmDB - an interface describing VsmDB
type VsmDB interface {
	GetMapTitles(authentication.User) (map[string]string, error)
	GetMap(authentication.User, string, string) (mapper.ValueStream, error)
	GetVersions(authentication.User, string) ([]string, error)
	SetMap(authentication.User, string, mapper.ValueStream) error
	DeleteMap(authentication.User, string, string) error
}

// VsmDatabase - a vsm database object
type VsmDatabase struct {
	DB *sql.DB
}

// GetConnectionDetails - Loads database connection from first service with tag "postgresql"
func GetConnectionDetails() (string, error) {
	appEnv, err := cfenv.Current()
	if err != nil {
		return "", err
	}

	services, err := appEnv.Services.WithTag("postgresql")
	if err != nil {
		return "", err
	}

	return services[0].Credentials["uri"].(string), nil
}

// BuildMigrations creates a new instance of the migrations
func BuildMigrations(db *VsmDatabase, migrationURI string) (*migrate.Migrate, error) {
	row := psql.Select("current_database()").
		RunWith(db.DB).QueryRow()

	var currentDatabase string

	if err := row.Scan(&currentDatabase); err != nil {
		return nil, err
	}

	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})

	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(migrationURI, currentDatabase, driver)

	if err != nil {
		return nil, err
	}

	// BC No schema_migration table: This can be deleted when all instances have schema_migration tables
	if _, _, versionState := m.Version(); versionState == migrate.ErrNilVersion {
		versionCheck := psql.Select("COUNT(*)").
			From("pg_catalog.pg_tables").
			Where(sq.Eq{"tablename": "maps"}).
			RunWith(db.DB).QueryRow()

		var mapsTableExists string

		if versionCheck.Scan(&mapsTableExists) != nil {
			return nil, err
		}

		if mapsTableExists == "1" {
			m.Force(1521459437)
		}
	}
	// END BC No schema_migration table

	return m, nil
}
