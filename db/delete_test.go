package db_test

import (
	"database/sql"
	"fmt"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/encryption"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/DATA-DOG/go-sqlmock"

	vsmDB "bitbucket.org/armakuni/value-stream-mapping-ui/db"
)

var _ = Describe("VsmDatabase#DeleteMap", func() {
	var (
		db          *sql.DB
		mock        sqlmock.Sqlmock
		err         error
		title       = "test1"
		name        = "test-name"
		mockQuery   *sqlmock.ExpectedQuery
		rows        *sqlmock.Rows
		vsmDatabase vsmDB.VsmDatabase
		user        authentication.User
	)

	BeforeEach(func() {
		db, mock, err = sqlmock.New()
		if err != nil {
			defer GinkgoRecover()
			Fail(fmt.Sprintf("\nan error '%s' was not expected when opening a stub database connection\n", err))
		}

		mockQuery = mock.ExpectQuery("SELECT vsm_json FROM maps WHERE (.*)")
		vsmDatabase = vsmDB.VsmDatabase{DB: db}
		user = authentication.User{
			Encryption: encryption.Credentials{
				Key: "y*UFiWg*WDT:/E^.i?TN8o55tk}#+Q\\|",
				Pad: "Y=hw|7cl>T?sE)7?;=L/{IwcG3kR+$}G",
			},
			Namespace: "namespace",
		}
	})

	AfterEach(func() {
		db.Close()
	})

	Context("when checking if the map already exists error", func() {
		JustBeforeEach(func() {
			mockQuery.WillReturnError(fmt.Errorf("error getting existing map"))
		})

		It("returns an error", func() {
			Ω(vsmDatabase.DeleteMap(user, name, "")).Should(MatchError("error getting existing map"))
		})
	})

	Context("when the map has a version", func() {
		var version = "1.0.0"
		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})
		Context("when the map does not exist", func() {
			Context("and the data is in the clear", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"})
				})

				JustBeforeEach(func() {
					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
						AddRow(""))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name:1.0.0' as it did not exist"))
				})
			})
			Context("and the data is encrypted", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"})
				})

				JustBeforeEach(func() {
					encryptedVersion, _ := user.Encryption.Encrypt("")

					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
						AddRow(encryptedVersion))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name:1.0.0' as it did not exist"))
				})
			})

		})

		Context("when the map exists with a different version number", func() {
			Context("and is stored in the clear", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"})
				})

				JustBeforeEach(func() {
					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
						AddRow("0.0.1").
						AddRow("0.0.2"))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name:1.0.0' as it did not exist, found other versions: [0.0.1, 0.0.2]"))
				})
			})
			Context("and is encrypted", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"})
				})

				JustBeforeEach(func() {
					v001encrypted, _ := user.Encryption.Encrypt("0.0.1")
					v002encrypted, _ := user.Encryption.Encrypt("0.0.2")

					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
						AddRow(v001encrypted).
						AddRow(v002encrypted))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name:1.0.0' as it did not exist, found other versions: [0.0.1, 0.0.2]"))
				})

			})
		})

		Context("when the map exists with the same version number", func() {
			Context("and is stored in the clear", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"}).
						AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s"}`, title, version))

					mock.ExpectExec("DELETE FROM maps WHERE (.*)").WillReturnResult(sqlmock.NewResult(1, 1))
				})

				It("deletes the map", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(Succeed())
				})
			})
			Context("and is encrypted", func() {
				BeforeEach(func() {
					mapEncrypted, _ := user.Encryption.Encrypt(fmt.Sprintf(`{"title": "%s", "version": "%s"}`, title, version))

					rows = sqlmock.NewRows([]string{"vsm_maps"}).
						AddRow(mapEncrypted)

					mock.ExpectExec("DELETE FROM maps WHERE (.*)").WillReturnResult(sqlmock.NewResult(1, 1))
				})

				It("deletes the map", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(Succeed())
				})
			})

		})
	})

	Context("when the map has no version", func() {
		var version = ""
		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		Context("when the map does not exist", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			Context("when checking for other versions errors", func() {
				JustBeforeEach(func() {
					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnError(fmt.Errorf("error getting versions"))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("error getting versions"))
				})
			})

			Context("when checking for other versions is successful", func() {
				Context("and is stored in the clear", func() {
					JustBeforeEach(func() {
						encryptedVersion, _ := user.Encryption.Encrypt("")

						mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
						mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
							AddRow(encryptedVersion))
					})

					It("returns an error", func() {
						Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name' as it did not exist"))
					})
				})
				Context("and is encrypted", func() {
					JustBeforeEach(func() {
						mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
						mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
							AddRow(""))
					})

					It("returns an error", func() {
						Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name' as it did not exist"))
					})
				})

			})
		})

		Context("when the map exists but with a version number", func() {
			Context("and the data is in the clear", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"})
				})

				JustBeforeEach(func() {
					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
						AddRow("0.0.1").
						AddRow("0.0.2"))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name' as it did not exist, found with versions: [0.0.1, 0.0.2]"))
				})
			})
			Context("and the data is encrypted", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"})
				})

				JustBeforeEach(func() {
					encryptedVer001, _ := user.Encryption.Encrypt("0.0.1")
					encryptedVer002, _ := user.Encryption.Encrypt("0.0.2")

					mockVersionsQuery := mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
					mockVersionsQuery.WillReturnRows(sqlmock.NewRows([]string{"version"}).
						AddRow(encryptedVer001).
						AddRow(encryptedVer002))
				})

				It("returns an error", func() {
					Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete map 'test-name' as it did not exist, found with versions: [0.0.1, 0.0.2]"))
				})
			})
		})

		Context("when the map exists", func() {
			Context("when deleting fails", func() {
				Context("and the map is in the clear", func() {
					BeforeEach(func() {
						rows = sqlmock.NewRows([]string{"vsm_maps"}).
							AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s"}`, title, version))

						mock.ExpectExec("DELETE FROM maps WHERE (.*)").WillReturnError(fmt.Errorf("could not delete"))
					})

					It("deletes the map", func() {
						Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete"))
					})
				})

				Context("and the map is encrypted", func() {
					BeforeEach(func() {
						encryptedMap, _ := user.Encryption.Encrypt(fmt.Sprintf(`{"title": "%s", "version": "%s"}`, title, version))

						rows = sqlmock.NewRows([]string{"vsm_maps"}).
							AddRow(encryptedMap)

						mock.ExpectExec("DELETE FROM maps WHERE (.*)").WillReturnError(fmt.Errorf("could not delete"))
					})

					It("deletes the map", func() {
						Ω(vsmDatabase.DeleteMap(user, name, version)).Should(MatchError("could not delete"))
					})
				})
			})

			Context("when deleting is successful", func() {
				Context("and the map is not encrypted", func() {
					BeforeEach(func() {
						rows = sqlmock.NewRows([]string{"vsm_maps"}).
							AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s"}`, title, version))

						mock.ExpectExec("DELETE FROM maps WHERE (.*)").WillReturnResult(sqlmock.NewResult(1, 1))
					})

					It("deletes the map", func() {
						Ω(vsmDatabase.DeleteMap(user, name, version)).Should(Succeed())
					})
				})
				Context("and the map is encrypted", func() {
					BeforeEach(func() {
						encryptedMap, _ := user.Encryption.Encrypt(fmt.Sprintf(`{"title": "%s", "version": "%s"}`, title, version))

						rows = sqlmock.NewRows([]string{"vsm_maps"}).
							AddRow(encryptedMap)

						mock.ExpectExec("DELETE FROM maps WHERE (.*)").WillReturnResult(sqlmock.NewResult(1, 1))
					})

					It("deletes the map", func() {
						Ω(vsmDatabase.DeleteMap(user, name, version)).Should(Succeed())
					})
				})
			})
		})
	})
})
