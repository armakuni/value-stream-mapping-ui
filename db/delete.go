package db

import (
	"fmt"
	"strings"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	sq "github.com/Masterminds/squirrel"
)

// DeleteMap - deletes a map from the database
func (db *VsmDatabase) DeleteMap(user authentication.User, name string, version string) error {
	existingMap, err := db.GetMap(user, name, version)
	if err != nil {
		return err
	}

	if existingMap.Title == "" {
		existingVersions, err := db.GetVersions(user, name)
		if err != nil {
			return err
		}

		var additionalDetail string
		if len(existingVersions) != 0 {
			if version == "" {
				additionalDetail = ", found with versions: ["
			} else {
				additionalDetail = ", found other versions: ["
			}

			additionalDetail += strings.Join(existingVersions, ", ") + "]"
		}

		var separator string
		if version != "" {
			separator = ":"
		}
		return fmt.Errorf("could not delete map '%s%s%s' as it did not exist%s", name, separator, version, additionalDetail)
	}

	if _, err := psql.Delete("maps").Where(
		sq.And{
			sq.Or{
				sq.Eq{"name": name, "version": version},
				sq.Eq{"name_index": user.Encryption.Hash(name), "version_index": user.Encryption.Hash(version)},
			},
			sq.Eq{"namespace": user.Namespace},
		},
	).
		RunWith(db.DB).Exec(); err != nil {
		return err
	}

	return nil
}
