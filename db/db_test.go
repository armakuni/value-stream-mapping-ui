package db_test

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/mattes/migrate"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/DATA-DOG/go-sqlmock"

	vsmDB "bitbucket.org/armakuni/value-stream-mapping-ui/db"
)

var _ = Describe("#GetConnectionDetails", func() {
	var (
		connectionString string
		err              error
		vcapServices     string
	)

	JustBeforeEach(func() {
		os.Setenv("VCAP_APPLICATION", "{}")
		os.Setenv("VCAP_SERVICES", vcapServices)
		connectionString, err = vsmDB.GetConnectionDetails()
	})

	AfterEach(func() {
		os.Unsetenv("VCAP_APPLICATION")
		os.Unsetenv("VCAP_SERVICES")
	})

	Context("when loading cf env fails", func() {
		It("returns an error", func() {
			Ω(err).Should(MatchError("unexpected end of JSON input"))
			Ω(connectionString).Should(Equal(""))
		})
	})

	Context("when loading cf env is successful", func() {
		Context("when a postgresql service does not exist", func() {
			BeforeEach(func() {
				vcapServices = "{}"
			})

			It("returns an error", func() {
				Ω(err).Should(MatchError("no services with tag postgresql"))
				Ω(connectionString).Should(Equal(""))
			})
		})

		Context("when a postgresql service exists", func() {
			Context("when more than one service exists", func() {
				BeforeEach(func() {
					vcapServices = `{
						"psql1": [
							{
								"credentials": {
									"uri": "postgres://username:password@host:port/db1"
								},
								"tags": [
									"postgresql"
								]
							}
						],
						"psql2": [
							{
								"credentials": {
									"uri": "postgres://username:password@host:port/db2"
								},
								"tags": [
									"postgresql"
								]
							}
						]
					}`
				})

				// As services is a map golang will randomise the order, making it impossible to define a "first"
				// considering that two postgres services should never be configured on this app just making sure it
				// loads one of them is good enough.
				It("returns the connection details of one of the services", func() {
					Ω(err).Should(BeNil())
					Ω(connectionString).Should(MatchRegexp("postgres://username:password@host:port/db{0,2}"))
				})
			})

			Context("when exactly one service exists", func() {
				BeforeEach(func() {
					vcapServices = `{
						"psql": [
							{
								"credentials": {
									"uri": "postgres://username:password@host:port/db"
								},
								"tags": [
									"postgresql"
								]
							}
						]
					}`
				})

				It("returns the connection details", func() {
					Ω(err).Should(BeNil())
					Ω(connectionString).Should(Equal("postgres://username:password@host:port/db"))
				})
			})
		})
	})
})

var _ = Describe("BuildMigrations", func() {
	var (
		db          *sql.DB
		mock        sqlmock.Sqlmock
		err         error
		vsmDatabase vsmDB.VsmDatabase
	)

	BeforeEach(func() {
		db, mock, err = sqlmock.New()
		if err != nil {
			defer GinkgoRecover()
			Fail(fmt.Sprintf("\nan error '%s' was not expected when opening a stub database connection\n", err))
		}
		vsmDatabase = vsmDB.VsmDatabase{
			DB: db,
		}
	})

	AfterEach(func() {
		db.Close()
	})

	Context("when postgres does not error", func() {
		var migrationErr error
		var migration *migrate.Migrate
		JustBeforeEach(func() {
			mock.ExpectQuery("SELECT current_database()").
				WillReturnRows(sqlmock.NewRows([]string{"current_database"}).
					AddRow("postgres"))
			mock.ExpectQuery("SELECT CURRENT_DATABASE()").
				WillReturnRows(sqlmock.NewRows([]string{"current_database"}).
					AddRow("postgres"))
			mock.ExpectQuery("SELECT.*").
				WillReturnRows(sqlmock.NewRows([]string{"count"}).
					AddRow("0"))
			mock.ExpectExec("CREATE TABLE \"schema_migrations\".*").
				WillReturnResult(sqlmock.NewResult(1, 1))
			migration, migrationErr = vsmDB.BuildMigrations(&vsmDatabase, "file://../migrations")
		})

		It("succeeds", func() {
			Ω(migrationErr).Should(BeNil())
			Ω(migration).Should(Not(BeNil()))
		})
	})
})
