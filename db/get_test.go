package db_test

import (
	"database/sql"
	"fmt"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/encryption"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/DATA-DOG/go-sqlmock"

	vsmDB "bitbucket.org/armakuni/value-stream-mapping-ui/db"
)

var _ = Describe("VsmDatabase#GetMapTitles", func() {
	var (
		db          *sql.DB
		mock        sqlmock.Sqlmock
		err         error
		mockQuery   *sqlmock.ExpectedQuery
		vsmDatabase vsmDB.VsmDatabase
		user        authentication.User
	)

	BeforeEach(func() {
		db, mock, err = sqlmock.New()
		if err != nil {
			defer GinkgoRecover()
			Fail(fmt.Sprintf("\nan error '%s' was not expected when opening a stub database connection\n", err))
		}
		mockQuery = mock.ExpectQuery("SELECT name, version, vsm_json FROM map")
		vsmDatabase = vsmDB.VsmDatabase{DB: db}
		user = authentication.User{
			Encryption: encryption.Credentials{
				Key: "y*UFiWg*WDT:/E^.i?TN8o55tk}#+Q\\|",
				Pad: "Y=hw|7cl>T?sE)7?;=L/{IwcG3kR+$}G",
			},
			Namespace: "namespace",
		}
	})

	AfterEach(func() {
		db.Close()
	})

	Context("when postgres does not error", func() {
		Context("when vsm_json is invalid", func() {
			JustBeforeEach(func() {
				rows := sqlmock.NewRows([]string{"name", "version", "vsm_json"}).
					AddRow("test1", "", `{]`)

				mockQuery.WillReturnRows(rows)
			})

			It("returns an error", func() {
				namedTitles, err := vsmDatabase.GetMapTitles(user)
				Ω(err).Should(MatchError("invalid character ']' looking for beginning of object key string"))
				Ω(namedTitles).Should(HaveLen(0))
			})
		})

		Context("when vsm_json is valid", func() {
			Context("clear text", func() {
				JustBeforeEach(func() {
					rows := sqlmock.NewRows([]string{"name", "version", "vsm_json"}).
						AddRow("test1", "2.0.0", `{"title": "test1v2"}`).
						AddRow("test1", "1.0.0", `{"title": "test1v1"}`).
						AddRow("test2", "", `{"title": "test2"}`).
						AddRow("test3", "", `{"title": "test3"}`)

					mockQuery.WillReturnRows(rows)
				})

				It("returns all map titles", func() {
					namedTitles, err := vsmDatabase.GetMapTitles(user)
					Ω(err).Should(BeNil())
					Ω(namedTitles).Should(HaveLen(3))
					Ω(namedTitles["test1"]).Should(Equal("test1v2"))
					Ω(namedTitles["test2"]).Should(Equal("test2"))
					Ω(namedTitles["test3"]).Should(Equal("test3"))
				})
			})
			Context("encrypted", func() {
				JustBeforeEach(func() {
					name1v2, _ := user.Encryption.Encrypt("test1")
					version1v2, _ := user.Encryption.Encrypt("2.0.0")
					map1v2, _ := user.Encryption.Encrypt(`{"title": "test1v2"}`)
					name1v1, _ := user.Encryption.Encrypt("test1")
					version1v1, _ := user.Encryption.Encrypt("1.0.0")
					map1v1, _ := user.Encryption.Encrypt(`{"title": "test1v1"}`)
					name2, _ := user.Encryption.Encrypt("test2")
					version2, _ := user.Encryption.Encrypt("")
					map2, _ := user.Encryption.Encrypt(`{"title": "test2"}`)
					name3, _ := user.Encryption.Encrypt("test3")
					version3, _ := user.Encryption.Encrypt("")
					map3, _ := user.Encryption.Encrypt(`{"title": "test3"}`)

					rows := sqlmock.NewRows([]string{"name", "version", "vsm_json"}).
						AddRow(name1v2, version1v2, map1v2).
						AddRow(name1v1, version1v1, map1v1).
						AddRow(name2, version2, map2).
						AddRow(name3, version3, map3)

					mockQuery.WillReturnRows(rows)
				})

				It("returns all map titles", func() {
					namedTitles, err := vsmDatabase.GetMapTitles(user)
					Ω(err).Should(BeNil())
					Ω(namedTitles).Should(HaveLen(3))
					Ω(namedTitles["test1"]).Should(Equal("test1v2"))
					Ω(namedTitles["test2"]).Should(Equal("test2"))
					Ω(namedTitles["test3"]).Should(Equal("test3"))
				})
			})
		})
	})

	Context("when postgres returns an err", func() {
		JustBeforeEach(func() {
			mockQuery.WillReturnError(fmt.Errorf("could not run query"))
		})

		It("returns an error", func() {
			namedTitles, err := vsmDatabase.GetMapTitles(user)
			Ω(err).Should(MatchError("could not run query"))
			Ω(namedTitles).Should(HaveLen(0))
		})
	})

	Context("when a row cannot be scanned", func() {
		JustBeforeEach(func() {
			rows := sqlmock.NewRows([]string{"name", "version", "vsm_json", "extra"}).
				AddRow("test1", "1.0.0", "{}", "")

			mockQuery.WillReturnRows(rows)
		})

		It("returns an error", func() {
			titles, err := vsmDatabase.GetMapTitles(user)
			Ω(err).Should(MatchError("sql: expected 4 destination arguments in Scan, not 3"))
			Ω(titles).Should(HaveLen(0))
		})
	})
})

var _ = Describe("VsmDatabase#GetMap", func() {
	var (
		db          *sql.DB
		mock        sqlmock.Sqlmock
		err         error
		title       = "test1"
		customer    = "AnExampleCustomer"
		mockQuery   *sqlmock.ExpectedQuery
		rows        *sqlmock.Rows
		vsmDatabase vsmDB.VsmDatabase
		user        authentication.User
	)

	BeforeEach(func() {
		db, mock, err = sqlmock.New()
		if err != nil {
			defer GinkgoRecover()
			Fail(fmt.Sprintf("\nan error '%s' was not expected when opening a stub database connection\n", err))
		}

		mockQuery = mock.ExpectQuery("SELECT vsm_json FROM maps WHERE (.*)")
		vsmDatabase = vsmDB.VsmDatabase{DB: db}
		user = authentication.User{
			Encryption: encryption.Credentials{
				Key: "y*UFiWg*WDT:/E^.i?TN8o55tk}#+Q\\|",
				Pad: "Y=hw|7cl>T?sE)7?;=L/{IwcG3kR+$}G",
			},
			Namespace: "namespace",
		}
	})

	AfterEach(func() {
		db.Close()
	})

	Context("when the map has a version", func() {
		var version = "1.0.0"

		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		Context("and the map exists", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"}).
					AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))
			})

			It("returns the map", func() {
				vsMap, err := vsmDatabase.GetMap(user, title, version)
				Ω(err).Should(BeNil())
				Ω(vsMap.Title).Should(Equal(title))
				Ω(vsMap.Version).Should(Equal(version))
				Ω(vsMap.Customer).Should(Equal(customer))
			})
		})

		Context("and the map does not exist", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			It("returns nothing", func() {
				vsMap, err := vsmDatabase.GetMap(user, title, version)
				Ω(err).Should(BeNil())
				Ω(vsMap).Should(Equal(mapper.ValueStream{}))
			})
		})
	})

	Context("when the map is encrypted", func() {
		var version = "1.0.0"

		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		Context("and the map exists", func() {
			BeforeEach(func() {
				encryptedMap, _ := user.Encryption.Encrypt(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))

				rows = sqlmock.NewRows([]string{"vsm_maps"}).
					AddRow(encryptedMap)
			})

			It("returns the map", func() {
				vsMap, err := vsmDatabase.GetMap(user, title, version)
				Ω(err).Should(BeNil())
				Ω(vsMap.Title).Should(Equal(title))
				Ω(vsMap.Version).Should(Equal(version))
				Ω(vsMap.Customer).Should(Equal(customer))
			})
		})

		Context("and the map does not exist", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			It("returns nothing", func() {
				vsMap, err := vsmDatabase.GetMap(user, title, version)
				Ω(err).Should(BeNil())
				Ω(vsMap).Should(Equal(mapper.ValueStream{}))
			})
		})
	})

	Context("when the map does not have a version", func() {
		var version = ""

		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		Context("and the map exists but has a version", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			It("returns nothing", func() {
				vsMap, err := vsmDatabase.GetMap(user, title, version)
				Ω(err).Should(BeNil())
				Ω(vsMap).Should(Equal(mapper.ValueStream{}))
			})
		})

		Context("and the map exists without a version", func() {
			Context("encryption", func() {
				Context("and the vsm_maps json is valid", func() {
					BeforeEach(func() {
						encryptedMap, _ := user.Encryption.Encrypt(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))
						rows = sqlmock.NewRows([]string{"vsm_maps"}).
							AddRow(encryptedMap)
					})

					It("returns the map", func() {
						vsMap, err := vsmDatabase.GetMap(user, title, version)
						Ω(err).Should(BeNil())
						Ω(vsMap.Title).Should(Equal(title))
						Ω(vsMap.Version).Should(Equal(""))
						Ω(vsMap.Customer).Should(Equal(customer))
					})
				})
			})
			Context("clear text", func() {
				Context("and the vsm_maps json is valid", func() {
					BeforeEach(func() {
						rows = sqlmock.NewRows([]string{"vsm_maps"}).
							AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))
					})

					It("returns the map", func() {
						vsMap, err := vsmDatabase.GetMap(user, title, version)
						Ω(err).Should(BeNil())
						Ω(vsMap.Title).Should(Equal(title))
						Ω(vsMap.Version).Should(Equal(""))
						Ω(vsMap.Customer).Should(Equal(customer))
					})
				})
			})

			Context("and the vsm_maps json is invalid", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"}).
						AddRow(fmt.Sprintf(`{"title": "%s", version": "%s, "customer": "%s"}`, title, version, customer))
				})

				It("returns an error", func() {
					vsMap, err := vsmDatabase.GetMap(user, title, version)
					Ω(err).Should(MatchError("invalid character 'v' looking for beginning of object key string"))
					Ω(vsMap).Should(Equal(mapper.ValueStream{}))
				})
			})
		})

		Context("and the map does not exist", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			It("returns nothing", func() {
				vsMap, err := vsmDatabase.GetMap(user, title, version)
				Ω(err).Should(BeNil())
				Ω(vsMap).Should(Equal(mapper.ValueStream{}))
			})
		})
	})

	Context("when postgres raises an error", func() {
		JustBeforeEach(func() {
			mockQuery.WillReturnError(fmt.Errorf("could not run query"))
		})

		It("returns an error", func() {
			vsMap, err := vsmDatabase.GetMap(user, title, "")
			Ω(err).Should(MatchError("could not run query"))
			Ω(vsMap).Should(Equal(mapper.ValueStream{}))
		})
	})
})

var _ = Describe("VsmDatabase#GetVersions", func() {
	var (
		db          *sql.DB
		mock        sqlmock.Sqlmock
		err         error
		title       = "test1"
		mockQuery   *sqlmock.ExpectedQuery
		rows        *sqlmock.Rows
		vsmDatabase vsmDB.VsmDatabase
		user        authentication.User
	)

	BeforeEach(func() {
		db, mock, err = sqlmock.New()
		if err != nil {
			defer GinkgoRecover()
			Fail(fmt.Sprintf("\nan error '%s' was not expected when opening a stub database connection\n", err))
		}
		mockQuery = mock.ExpectQuery("SELECT version FROM maps WHERE (.*)")
		vsmDatabase = vsmDB.VsmDatabase{DB: db}
		user = authentication.User{
			Encryption: encryption.Credentials{
				Key: "y*UFiWg*WDT:/E^.i?TN8o55tk}#+Q\\|",
				Pad: "Y=hw|7cl>T?sE)7?;=L/{IwcG3kR+$}G",
			},
			Namespace: "namespace",
		}
	})

	AfterEach(func() {
		db.Close()
	})

	Context("when postgres errors", func() {
		JustBeforeEach(func() {
			mockQuery.WillReturnError(fmt.Errorf("error running SELECT"))
		})

		It("returns an error", func() {
			versions, err := vsmDatabase.GetVersions(user, title)
			Ω(err).Should(MatchError("error running SELECT"))
			Ω(versions).Should(HaveLen(0))
		})
	})

	Context("when the map does not exist", func() {
		BeforeEach(func() {
			rows = sqlmock.NewRows([]string{"version"})
		})

		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		It("returns an error", func() {
			versions, err := vsmDatabase.GetVersions(user, title)
			Ω(err).Should(MatchError("map with name: 'test1' does not exist, cannot list versions"))
			Ω(versions).Should(HaveLen(0))
		})
	})

	Context("when the map does exist", func() {
		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		Context("when there are no versions", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"version"}).
					AddRow("")
			})

			It("returns no versions", func() {
				versions, err := vsmDatabase.GetVersions(user, title)
				Ω(err).Should(BeNil())
				Ω(versions).Should(HaveLen(0))
			})
		})

		Context("when there are versions", func() {
			Context("and scanning the rows errors", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"title", "version"}).
						AddRow("1.0.0", "test")
				})

				It("returns the the versions", func() {
					versions, err := vsmDatabase.GetVersions(user, title)
					Ω(err).Should(MatchError("sql: expected 2 destination arguments in Scan, not 1"))
					Ω(versions).Should(HaveLen(0))
				})
			})

			Context("and scanning the rows is successful", func() {
				Context("without encryption", func() {
					BeforeEach(func() {
						rows = sqlmock.NewRows([]string{"version"}).
							AddRow("1.0.0").
							AddRow("2.0.0").
							AddRow("2.1.3")
					})

					It("returns the the versions", func() {
						versions, err := vsmDatabase.GetVersions(user, title)
						Ω(err).Should(BeNil())
						Ω(versions).Should(HaveLen(3))
					})
				})
				Context("with encryption", func() {
					BeforeEach(func() {
						v1, _ := user.Encryption.Encrypt("1.0.0")
						v2, _ := user.Encryption.Encrypt("2.0.0")
						v3, _ := user.Encryption.Encrypt("2.1.3")

						rows = sqlmock.NewRows([]string{"version"}).
							AddRow(v1).
							AddRow(v2).
							AddRow(v3)
					})

					It("returns the the versions", func() {
						versions, err := vsmDatabase.GetVersions(user, title)
						Ω(err).Should(BeNil())
						Ω(versions).Should(HaveLen(3))
					})
				})
			})
		})
	})
})
