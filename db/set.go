package db

import (
	"encoding/json"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	sq "github.com/Masterminds/squirrel"
)

// SetMap - inserts/ updates a value stream map
func (db *VsmDatabase) SetMap(user authentication.User, name string, valueStream mapper.ValueStream) error {
	existingMap, err := db.GetMap(user, name, valueStream.Version)
	if err != nil {
		return err
	}

	valueStreamJSON, _ := json.Marshal(valueStream)
	encryptedJSON, _ := user.Encryption.Encrypt(string(valueStreamJSON))

	if existingMap.Title == "" {
		cypherName, _ := user.Encryption.Encrypt(name)
		cypherVersion, _ := user.Encryption.Encrypt(valueStream.Version)

		if _, err := psql.Insert("maps").
			Columns("namespace", "name", "name_index", "version", "version_index", "vsm_json").
			Values(user.Namespace, cypherName, user.Encryption.Hash(name), cypherVersion, user.Encryption.Hash(valueStream.Version), encryptedJSON).
			RunWith(db.DB).Exec(); err != nil {
			return err
		}
	} else {
		if _, err := psql.Update("maps").
			Set("vsm_json", encryptedJSON).
			Where(
				sq.And{
					sq.Or{
						sq.Eq{"name": name, "version": valueStream.Version},
						sq.Eq{"name_index": user.Encryption.Hash(name), "version_index": user.Encryption.Hash(valueStream.Version)},
					},
					sq.Eq{"namespace": user.Namespace},
				},
			).
			RunWith(db.DB).Exec(); err != nil {
			return err
		}
	}

	return nil
}
