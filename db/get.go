package db

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"github.com/blang/semver"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	sq "github.com/Masterminds/squirrel"
)

type versionedTitle struct {
	Version string
	Title   string
}

// GetMapTitles - get all names from maps
func (db *VsmDatabase) GetMapTitles(user authentication.User) (map[string]string, error) {
	rows, err := psql.Select("name", "version", "vsm_json").
		From("maps").
		Where(sq.Eq{"namespace": user.Namespace}).
		RunWith(db.DB).Query()

	if err != nil {
		return nil, err
	}

	namedVersionedTitles := make(map[string][]versionedTitle)
	defer rows.Close()
	for rows.Next() {
		var (
			name                     string
			version                  string
			encryptedValueStreamJSON string
		)
		err := rows.Scan(&name, &version, &encryptedValueStreamJSON)
		if err != nil {
			return nil, err
		}
		var valueStreamJSON string

		if valueStreamJSON, err = user.Encryption.Decrypt(encryptedValueStreamJSON); err != nil {
			// For unencrypted keys fallback to the decrypted version, we will encrypt when they update
			valueStreamJSON = encryptedValueStreamJSON
		}

		var valueStream mapper.ValueStream
		if err := json.Unmarshal([]byte(valueStreamJSON), &valueStream); err != nil {
			return nil, err
		}

		decryptedName, decryptionErr := user.Encryption.Decrypt(name)

		if decryptionErr == nil {
			name = decryptedName
		}

		decryptedVersion, decryptionErr := user.Encryption.Decrypt(version)

		if decryptionErr == nil {
			version = decryptedVersion
		}

		namedVersionedTitles[name] = append(namedVersionedTitles[name], versionedTitle{Title: valueStream.Title, Version: version})
	}

	return getNamedTitlesFromVersionedTitles(namedVersionedTitles)
}

// GetMap gets value stream maps from the database based on name and version
func (db *VsmDatabase) GetMap(user authentication.User, name string, version string) (mapper.ValueStream, error) {
	row := psql.Select("vsm_json").
		From("maps").
		Where(sq.And{
			sq.Or{
				sq.Eq{"name": name, "version": version},
				sq.Eq{"name_index": user.Encryption.Hash(name), "version_index": user.Encryption.Hash(version)},
			},
			sq.Eq{"namespace": user.Namespace},
		}).
		RunWith(db.DB).QueryRow()

	var encryptedValueStreamJSON string
	err := row.Scan(&encryptedValueStreamJSON)

	switch {
	case err == sql.ErrNoRows:
		return mapper.ValueStream{}, nil
	case err != nil:
		return mapper.ValueStream{}, err
	}

	var valueStreamJSON string
	if valueStreamJSON, err = user.Encryption.Decrypt(encryptedValueStreamJSON); err != nil {
		// For unencrypted keys fallback to the decrypted version, we will encrypt when they update
		valueStreamJSON = encryptedValueStreamJSON
	}

	var valueStream mapper.ValueStream
	if err := json.Unmarshal([]byte(valueStreamJSON), &valueStream); err != nil {
		return mapper.ValueStream{}, err
	}

	return valueStream, nil
}

// GetVersions get all versions for a specific name
func (db *VsmDatabase) GetVersions(user authentication.User, name string) ([]string, error) {
	rows, err := psql.Select("version").
		From("maps").
		Where(sq.And{
			sq.Or{
				sq.Eq{"name": name},
				sq.Eq{"name_index": user.Encryption.Hash(name)},
			},
			sq.Eq{"namespace": user.Namespace},
		}).
		RunWith(db.DB).Query()

	if err != nil {
		return nil, err
	}

	var versions []string
	defer rows.Close()
	var rowCount = 0
	for rows.Next() {
		rowCount++
		var version string
		err := rows.Scan(&version)
		if err != nil {
			return nil, err
		}

		decryptedVersion, decryptionErr := user.Encryption.Decrypt(version)

		if decryptionErr == nil {
			version = decryptedVersion
		}

		if version != "" {
			versions = append(versions, version)
		}
	}
	if rowCount == 0 {
		return nil, fmt.Errorf("map with name: '%s' does not exist, cannot list versions", name)
	}

	return versions, nil
}

func getNamedTitlesFromVersionedTitles(namedVersionedTitles map[string][]versionedTitle) (map[string]string, error) {
	namedTitles := make(map[string]string)
	for name, versionedTitles := range namedVersionedTitles {
		var versions []semver.Version
		for _, versionedTitle := range versionedTitles {
			if versionedTitle.Version == "" && len(versionedTitles) == 1 {
				namedTitles[name] = versionedTitle.Title
			} else {
				if versionedTitle.Version == "" {
					continue
				}
				semverVersion, err := semver.Parse(versionedTitle.Version)
				if err != nil {
					return nil, err
				}
				versions = append(versions, semverVersion)
			}
		}
		if len(versions) != 0 {
			semver.Sort(versions)
			latestVersion := versions[len(versions)-1].String()
			for _, versionedTitle := range versionedTitles {
				if versionedTitle.Version == latestVersion {
					namedTitles[name] = versionedTitle.Title
				}
			}
		}
	}

	return namedTitles, nil
}
