package db_test

import (
	"database/sql"
	"fmt"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/encryption"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/DATA-DOG/go-sqlmock"

	vsmDB "bitbucket.org/armakuni/value-stream-mapping-ui/db"
)

var _ = Describe("VsmDatabase#SetMap", func() {
	var (
		db          *sql.DB
		mock        sqlmock.Sqlmock
		err         error
		valueStream mapper.ValueStream
		title       = "test1"
		customer    = "AnExampleCustomer"
		mockQuery   *sqlmock.ExpectedQuery
		rows        *sqlmock.Rows
		vsmDatabase vsmDB.VsmDatabase
		user        authentication.User
		name        = "test-name"
	)

	BeforeEach(func() {
		db, mock, err = sqlmock.New()
		if err != nil {
			defer GinkgoRecover()
			Fail(fmt.Sprintf("\nan error '%s' was not expected when opening a stub database connection\n", err))
		}

		mockQuery = mock.ExpectQuery("SELECT vsm_json FROM maps WHERE (.*)")
		user = authentication.User{
			Encryption: encryption.Credentials{
				Key: "y*UFiWg*WDT:/E^.i?TN8o55tk}#+Q\\|",
				Pad: "Y=hw|7cl>T?sE)7?;=L/{IwcG3kR+$}G",
			},
			Namespace: "namespace",
		}
		vsmDatabase = vsmDB.VsmDatabase{DB: db}

		valueStream = mapper.ValueStream{
			Title:    title,
			Customer: customer,
		}
	})

	AfterEach(func() {
		db.Close()
	})

	Context("when checking if the map already exists error", func() {
		JustBeforeEach(func() {
			mockQuery.WillReturnError(fmt.Errorf("error getting existing map"))
		})

		It("returns an error", func() {
			Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(MatchError("error getting existing map"))
		})
	})

	Context("when the map has a version", func() {
		var version = "1.0.0"
		JustBeforeEach(func() {
			valueStream.Version = version
			mockQuery.WillReturnRows(rows)
		})

		Context("and the version is new", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			JustBeforeEach(func() {
				mock.ExpectExec("INSERT INTO maps .*").
					WillReturnResult(sqlmock.NewResult(1, 1))
			})

			It("adds a new map", func() {
				Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(Succeed())
			})
		})

		Context("and the version already exists", func() {
			Context("the map is encrypted", func() {
				BeforeEach(func() {
					encryptedMap, _ := user.Encryption.Encrypt(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))

					rows = sqlmock.NewRows([]string{"vsm_maps"}).
						AddRow(encryptedMap)
				})

				JustBeforeEach(func() {
					mock.ExpectExec("UPDATE maps SET vsm_json .*").
						WillReturnResult(sqlmock.NewResult(1, 1))
				})

				It("overrides the existing map", func() {
					Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(Succeed())
				})
			})
			Context("the map is not encrypted", func() {
				BeforeEach(func() {
					rows = sqlmock.NewRows([]string{"vsm_maps"}).
						AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))
				})

				JustBeforeEach(func() {
					mock.ExpectExec("UPDATE maps SET vsm_json .*").
						WillReturnResult(sqlmock.NewResult(1, 1))
				})

				It("overrides the existing map", func() {
					Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(Succeed())
				})
			})
		})
	})

	Context("when the map does not have a version", func() {
		var version = ""
		JustBeforeEach(func() {
			mockQuery.WillReturnRows(rows)
		})

		Context("and it is new", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"})
			})

			Context("and the inset errors", func() {
				JustBeforeEach(func() {
					mock.ExpectExec("INSERT INTO maps .*").
						WillReturnError(fmt.Errorf("insert error"))
				})

				It("overrides the existing map", func() {
					Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(MatchError("insert error"))
				})
			})

			Context("and the insert is successful", func() {
				JustBeforeEach(func() {
					mock.ExpectExec("INSERT INTO maps .*").
						WillReturnResult(sqlmock.NewResult(1, 1))
				})

				It("adds a new map", func() {
					Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(Succeed())
				})
			})
		})

		Context("and it already exists", func() {
			BeforeEach(func() {
				rows = sqlmock.NewRows([]string{"vsm_maps"}).
					AddRow(fmt.Sprintf(`{"title": "%s", "version": "%s", "customer": "%s"}`, title, version, customer))
			})

			Context("and the update errors", func() {
				JustBeforeEach(func() {
					mock.ExpectExec("UPDATE maps SET vsm_json .*").
						WillReturnError(fmt.Errorf("update error"))
				})

				It("overrides the existing map", func() {
					Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(MatchError("update error"))
				})
			})

			Context("and the update is successful", func() {
				JustBeforeEach(func() {
					mock.ExpectExec("UPDATE maps SET vsm_json .*").
						WillReturnResult(sqlmock.NewResult(1, 1))
				})

				It("overrides the existing map", func() {
					Ω(vsmDatabase.SetMap(user, name, valueStream)).Should(Succeed())
				})
			})
		})
	})
})
