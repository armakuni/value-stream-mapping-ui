package mapper

// Cells a collection of Cell
type Cells []Cell

// NewFlowCells - Build connector cells that indicate information flow
func NewFlowCells(informationFlows FlowResources, center int, y int, processCells Cells, size int) Cells {
	const offset = 40
	var cells Cells
	uniqueFlows := informationFlows.InformationFlows()
	x := center - ((size - 30) / 4)
	x -= ((size - 30) - offset) * (len(uniqueFlows) - 1)
	if len(uniqueFlows) > 1 {
		x += 15 * (len(uniqueFlows) - 1)
	}
	xCoordinates := make(map[string]int)
	for _, uniqueFlow := range uniqueFlows {
		cells = append(cells, Cell{
			Type:     "uml.InformationFlow",
			ID:       uniqueFlow,
			Name:     uniqueFlow,
			Position: CellPosition{X: x, Y: y},
		})
		xCoordinates[uniqueFlow] = x
		x += (size - 30) + offset
	}
	for _, informationFlow := range informationFlows {
		resourceCell := FindProcessCell(informationFlow.Resource, processCells)
		resourceCellCenter := resourceCell.Position.X + (size / 2)
		flowCenter := xCoordinates[informationFlow.Flow] + ((size - 30) / 2)
		xVertex := (resourceCellCenter + flowCenter) / 2
		yVertex := (resourceCell.Position.Y + y + 30) / 2
		if xVertex < (resourceCell.Position.X - 82) {
			xVertex = resourceCell.Position.X - 82
		}
		if xVertex > (resourceCell.Position.X + size + 82) {
			xVertex = resourceCell.Position.X + size + 82
		}
		var cellType = "uml.SimpleTransition"
		if informationFlow.Automated {
			cellType = "uml.SimpleAutomatedTransition"
		}
		cells = append(cells, Cell{
			Type:   cellType,
			Source: Source{ID: informationFlow.Relation.From},
			Target: Target{ID: informationFlow.Relation.To},
			Vertices: []Vertex{
				{
					X: xVertex,
					Y: yVertex,
				},
			},
		})
	}
	return cells
}

// NewStartCells - Draw the factory cells that indicates the the business area
func NewStartCells(valueStream ValueStream, xPosition int, yPosition int, processCells Cells, size int) Cells {
	var cells Cells
	resources := valueStream.Resources
	firstResource := resources.Start()
	lastResource := resources.End()
	firstProccessCell := FindProcessCell(*firstResource, processCells)
	lastProcessCell := FindProcessCell(*lastResource, processCells)
	cells = append(cells, Cell{
		Type:     "uml.Start",
		ID:       "source",
		Customer: valueStream.Customer,
		Position: CellPosition{X: xPosition, Y: yPosition},
	})

	cells = append(cells, Cell{
		Source: Source{ID: "source"},
		Target: Target{ID: firstResource.Process},
		Vertices: []Vertex{
			{
				X: firstProccessCell.Position.X + (size / 2),
				Y: firstProccessCell.Position.Y - 30,
			},
		},
		Type: "uml.SimpleTransition",
	})

	cells = append(cells, Cell{
		Source: Source{ID: lastResource.Process},
		Target: Target{ID: "source"},
		Vertices: []Vertex{
			{
				X: lastProcessCell.Position.X + (size / 2),
				Y: lastProcessCell.Position.Y - 30,
			},
		},
		Type: "uml.SimpleTransition",
	})

	return cells
}

// UpdateYPosition - Updates all the Y positions of a specific type
func (cells Cells) UpdateYPosition(cellType string, newY int) {
	for index, cell := range cells {
		if cell.Type == cellType {
			cells[index].Position.Y = newY + 58
		}
	}
}

// FindProcessCell - locate the graph cell that relates to a resource
func FindProcessCell(resource Resource, cells Cells) Cell {
	for _, cell := range cells {
		if cell.ID == resource.Process {
			return cell
		}
	}
	return Cell{}
}

// NewRelationCells - Build connector cells that show process flow
func NewRelationCells(resources Resources) Cells {
	var seenIds map[string]bool
	seenIds = make(map[string]bool)

	var cells Cells

	for _, resource := range resources {
		for _, outRelation := range resource.Out {
			if _, ok := seenIds[resource.Process]; ok {
				continue
			}

			cells = append(cells, Cell{
				Source: Source{ID: resource.Process},
				Target: Target{ID: outRelation.Process},
				Labels: []Label{
					{
						Attrs: map[string]map[string]string{
							"text": {
								"text": outRelation.Note,
							},
						},
					},
				},
				Type: "uml.DashedTransition",
			})
		}

		seenIds[resource.Process] = true
	}

	return cells
}

// NewLadderCells - Create the new ladder cells from a resource
func NewLadderCells(hasMetrics bool, resource *Resource, currentX int, currentY int) Cells {
	var cells Cells

	if hasMetrics && resource.LongestPath == true {
		ladderCell := NewLadderCell(*resource, currentX, currentY+58)
		cells = append(cells, ladderCell)
	}

	return cells
}

func (cells Cells) prefixIDs(idPrefix string) Cells {
	renamedCells := make(Cells, len(cells))

	for index, cell := range cells {
		if cell.ID != "" {
			cell.ID = idPrefix + cell.ID
		}

		if cell.Source != (Source{}) {
			cell.Source = Source{ID: idPrefix + cell.Source.ID}
		}
		if cell.Target != (Target{}) {
			cell.Target = Target{ID: idPrefix + cell.Target.ID}
		}

		renamedCells[index] = cell
	}

	return renamedCells
}

func (cells Cells) offset(position CellPosition) Cells {
	newCells := make(Cells, len(cells))
	for index, cell := range cells {
		if cell.Position.X != 0 && cell.Position.Y != 0 {
			cell.Position = cell.Position.offset(position)
		}

		newVerticies := make([]Vertex, len(cell.Vertices))
		for vIndex, vertex := range cell.Vertices {
			newVerticies[vIndex] = vertex.offset(position)
		}
		cell.Vertices = newVerticies

		newCells[index] = cell
	}

	return newCells
}
