package mapper

// Cell a struct representation of a JointJS cell
type Cell struct {
	ID                string                       `json:"id,omitempty"`
	Position          CellPosition                 `json:"position,omitempty"`
	Description       string                       `json:"description,omitempty"`
	Title             string                       `json:"title,omitempty"`
	Type              string                       `json:"type"`
	Days              string                       `json:"days,omitempty"`
	Hours             string                       `json:"hours,omitempty"`
	ProcessID         string                       `json:"processID,omitempty"`
	LeadTime          string                       `json:"lead_time,omitempty"`
	ProcessTime       string                       `json:"process_time,omitempty"`
	ActivityRatio     string                       `json:"activity_ratio,omitempty"`
	CARatio           string                       `json:"c&a_ratio,omitempty"`
	CARatioDesc       string                       `json:"c&a_ratio_desc,omitempty"`
	Attrs             map[string]map[string]string `json:"attrs,omitempty"`
	Size              CellSize                     `json:"size,omitempty"`
	Source            Source                       `json:"source,omitempty"`
	Target            Target                       `json:"target,omitempty"`
	Vertices          []Vertex                     `json:"vertices,omitempty"`
	Labels            Labels                       `json:"labels,omitempty"`
	Customer          string                       `json:"customer,omitempty"`
	LeadTimeWeight    float64                      `json:"lead_time_weight,omitempty"`
	NumberOfStaff     int                          `json:"numberOfStaff,omitempty"`
	FlowBlockers      string                       `json:"flow_blockers,omitempty"`
	Name              string                       `json:"name,omitempty"`
	SourceVersion     string                       `json:"source_version,omitempty"`
	ComparisonVersion string                       `json:"comparison_version,omitempty"`
	LongestPath       bool                         `json:"longest_path,omitempty"`
}

// CellPosition - defines a cell position for JointJS
type CellPosition struct {
	X int `json:"x"`
	Y int `json:"y"`
}

func (position CellPosition) offset(offset CellPosition) CellPosition {
	return CellPosition{
		X: position.X + offset.X,
		Y: position.Y + offset.Y,
	}
}

// CellSize a struct representation of a JointJS cell sizing
type CellSize struct {
	Width  int `json:"width,omitempty"`
	Height int `json:"height,omitempty"`
}

// Source - a transition link source
type Source struct {
	ID string `json:"id,omitempty"`
}

// Target - a transition link target
type Target struct {
	ID string `json:"id,omitempty"`
}

// Vertex - adds a vertex point to a connector
type Vertex struct {
	X int `json:"x"`
	Y int `json:"y"`
}

func (vertex Vertex) offset(position CellPosition) Vertex {
	return Vertex{
		X: vertex.X + position.X,
		Y: vertex.Y + position.Y,
	}
}

// AreValid - checks the validity of all Cells
func (cells Cells) AreValid() bool {
	for _, cell := range cells {
		if !cell.IsValid() {
			return false
		}
	}

	return true
}

// IsValid - checks the validity of fields on the Cell struct
func (cell Cell) IsValid() bool {
	if cell.Labels.AreValid() &&
		cell.Size.IsValid() {
		return true
	}

	return false
}

// IsValid - checks the validity of a cell size
func (cellSize CellSize) IsValid() bool {
	if cellSize.Width >= 0 && cellSize.Height >= 0 {
		return true
	}

	return false
}

// NewProcessToCell - Convert a resource into a process cell
func NewProcessToCell(resource Resource, xPosition int, yPosition int, renderProcessKeys bool) (Cell, error) {
	var computedType = "basic.rect"

	if resource.Type == "process" {
		computedType = "uml.Node"
	}

	var processID string
	if renderProcessKeys {
		processID = resource.Process
	}

	cell := Cell{
		ID:             resource.Process,
		Position:       CellPosition{X: xPosition, Y: yPosition},
		Description:    resource.Description,
		Title:          resource.Title,
		ProcessID:      processID,
		Type:           computedType,
		LeadTimeWeight: resource.LeadTimeWeight,
		NumberOfStaff:  resource.NumberOfStaff,
		FlowBlockers:   resource.FlowBlockers,
		LongestPath:    resource.LongestPath,
	}

	if resource.Metrics != nil {
		if err := resource.Metrics.Calculate(); err != nil {
			return Cell{}, err
		}
		cell.LeadTime = resource.Metrics.LeadTime
		cell.ProcessTime = resource.Metrics.ProcessTime
		cell.ActivityRatio = resource.Metrics.ActivityRatio
		cell.CARatio = resource.Metrics.CARatio
		cell.CARatioDesc = resource.Metrics.CARatioDesc
	}
	return cell, nil
}

// NewLadderCell - Draw the ladders at the bottom of the processes
func NewLadderCell(resource Resource, xPosition int, yPosition int) Cell {
	cell := Cell{
		ID:       "ladder-" + resource.Process,
		Position: CellPosition{X: xPosition, Y: yPosition},
		Type:     "uml.Ladder",
	}

	if resource.Metrics != nil {
		cell.ProcessTime = resource.Metrics.ProcessTime
		cell.LeadTime = resource.Metrics.LeadTime
	}
	return cell
}

// NewDiffInformationCell is a new cell
func NewDiffInformationCell(source ValueStream, comparison ValueStream, xPosition int, yPosition int) Cell {
	cell := Cell{
		Position:          CellPosition{X: xPosition, Y: yPosition},
		Type:              "uml.DiffInformation",
		SourceVersion:     source.Version,
		ComparisonVersion: comparison.Version,
	}

	return cell
}

// NewKaizenCell - Draw the kaizen burst cells at the top of the processes
func NewKaizenCell(resource Resource, xPosition int, yPosition int) Cell {
	cell := Cell{
		ID:          "kaizen-" + resource.Process,
		Position:    CellPosition{X: xPosition, Y: yPosition},
		Description: resource.Kaizen,
		Type:        "uml.Kaizen",
	}
	return cell
}

// NewSummaryCell - Build the summary cell that is on the right of the svg
func NewSummaryCell(valueStream ValueStream, lastCellX int, y int) (Cell, error) {
	x := lastCellX - 58
	x += 285
	return Cell{
		Position:      CellPosition{X: x, Y: y},
		Type:          "uml.Summary",
		LeadTime:      valueStream.MetricsSummary.LeadTime,
		ProcessTime:   valueStream.MetricsSummary.ProcessTime,
		ActivityRatio: valueStream.MetricsSummary.ActivityRatio,
		CARatio:       valueStream.MetricsSummary.CARatio,
	}, nil
}
