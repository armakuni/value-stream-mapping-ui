package mapper_test

import (
	"time"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Metrics#Calculate", func() {
	var metrics *mapper.Metrics

	BeforeEach(func() {
		metrics = &mapper.Metrics{
			LeadTimeDuration:    10 * time.Minute,
			ProcessTimeDuration: 10 * time.Minute,
			ActivityRatio:       "",
			CARatio:             "10",
		}
	})

	Context("when lead time is less than process time", func() {
		JustBeforeEach(func() {
			metrics.LeadTimeDuration = 5 * time.Minute
			metrics.ProcessTimeDuration = 12 * time.Hour
		})

		It("returns an error", func() {
			Ω(metrics.Calculate()).Should(MatchError("lead time cannot be less than process time"))
		})
	})

	Context("when lead time is equal to process time", func() {
		It("calculates activity ratio", func() {
			Ω(metrics.ActivityRatio).Should(Equal(""))
			Ω(metrics.Calculate()).Should(BeNil())
			Ω(metrics.ActivityRatio).Should(Equal("100%"))
		})
	})

	Context("when lead time is greater than process time", func() {
		JustBeforeEach(func() {
			metrics.LeadTimeDuration = 24 * time.Hour
			metrics.ProcessTimeDuration = 2 * time.Hour
		})

		It("calculates activity ratio", func() {
			Ω(metrics.ActivityRatio).Should(Equal(""))
			Ω(metrics.Calculate()).Should(BeNil())
			Ω(metrics.ActivityRatio).Should(Equal("8%"))
		})
	})

	Context("when the lead time is in days with a decimal", func() {
		JustBeforeEach(func() {
			metrics.LeadTimeDuration = 120 * time.Hour
			metrics.ProcessTimeDuration = 60 * time.Hour
		})

		It("calculates activity ratio", func() {
			Ω(metrics.ActivityRatio).Should(Equal(""))
			Ω(metrics.Calculate()).Should(BeNil())
			Ω(metrics.ActivityRatio).Should(Equal("50%"))
		})
	})

	Context("when the lead time is in days without a decimal", func() {
		JustBeforeEach(func() {
			metrics.LeadTimeDuration = 24 * time.Hour
			metrics.ProcessTimeDuration = 8 * time.Hour
		})

		It("calculates activity ratio", func() {
			Ω(metrics.ActivityRatio).Should(Equal(""))
			Ω(metrics.Calculate()).Should(BeNil())
			Ω(metrics.ActivityRatio).Should(Equal("33%"))
		})
	})
})

var _ = Describe("Metrics#CalculateDurations", func() {
	var metrics *mapper.Metrics
	var err error
	var workDay time.Duration
	var workWeek time.Duration
	var leadTime string
	var processTime string

	JustBeforeEach(func() {
		metrics = &mapper.Metrics{
			LeadTime:    leadTime,
			ProcessTime: processTime,
		}

		workDuration, _ := mapper.NewWorkDuration(workDay, workWeek)
		err = metrics.CalculateDurations(workDuration)
	})

	Context("when lead time is invalid", func() {
		BeforeEach(func() {
			leadTime = "notAValidDuration"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError(`time: invalid duration notAValidDuration`))
		})
	})

	Context("when process time is invalid", func() {
		BeforeEach(func() {
			leadTime = "2h"
			processTime = "invalidDuration"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError(`time: invalid duration invalidDuration`))
		})
	})

	Context("when both are valid", func() {
		BeforeEach(func() {
			leadTime = "2h"
			processTime = ""
			workDay = 8 * time.Hour
			workWeek = 5 * workDay
		})

		It("returns no error", func() {
			Ω(err).ShouldNot(HaveOccurred())
		})

		It("populates the lead time", func() {
			Ω(metrics.LeadTimeDuration).Should(Equal(2 * time.Hour))
		})
		It("populates the process time", func() {
			Ω(metrics.ProcessTimeDuration).Should(Equal(0 * time.Hour))
		})
	})
})

var _ = Describe("Metrics#Validate", func() {
	var (
		leadTime    string
		processTime string
		caRatio     string
		err         error
	)

	JustBeforeEach(func() {
		metrics := &mapper.Metrics{
			LeadTime:    leadTime,
			ProcessTime: processTime,
			CARatio:     caRatio,
		}
		err = metrics.Validate()
	})

	Context("when lead time is invalid", func() {
		BeforeEach(func() {
			leadTime = "notADuration"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"lead-time-validation-error":"lead time (notADuration) is not in the supported format is: 2d3h1m2s"` +
				"}"))
		})

		Context("and process time is invalid", func() {
			BeforeEach(func() {
				processTime = "invalidDuration"
			})

			Context("and ca ratio is valid", func() {
				BeforeEach(func() {
					caRatio = ""
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError("{" +
						`"lead-time-validation-error":"lead time (notADuration) is not in the supported format is: 2d3h1m2s",` +
						`"process-time-validation-error":"process time (invalidDuration) is not in the supported format is: 2d3h1m2s"` +
						"}"))
				})
			})

			Context("and ca ratio is invalid", func() {
				BeforeEach(func() {
					caRatio = "364"
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError("{" +
						`"lead-time-validation-error":"lead time (notADuration) is not in the supported format is: 2d3h1m2s",` +
						`"process-time-validation-error":"process time (invalidDuration) is not in the supported format is: 2d3h1m2s",` +
						`"ca-ratio-validation-error":"c\u0026a ratio must be a valid percentage but was: 364"` +
						"}"))
				})
			})
		})

		Context("and process time is valid", func() {
			BeforeEach(func() {
				processTime = "20m"
			})

			Context("and ca ratio is valid", func() {
				BeforeEach(func() {
					caRatio = ""
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError(`{"lead-time-validation-error":"lead time (notADuration) is not in the supported format is: 2d3h1m2s"}`))
				})
			})

			Context("and ca ratio is invalid", func() {
				BeforeEach(func() {
					caRatio = "364"
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError("{" +
						`"lead-time-validation-error":"lead time (notADuration) is not in the supported format is: 2d3h1m2s",` +
						`"ca-ratio-validation-error":"c\u0026a ratio must be a valid percentage but was: 364"` +
						"}"))
				})
			})
		})
	})

	Context("when lead time is valid", func() {
		BeforeEach(func() {
			leadTime = "10m"
		})

		Context("and process time is invalid", func() {
			BeforeEach(func() {
				processTime = "invalidDuration"
			})

			Context("and ca ratio is valid", func() {
				BeforeEach(func() {
					caRatio = "0%"
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError("{" +
						`"process-time-validation-error":"process time (invalidDuration) is not in the supported format is: 2d3h1m2s"` +
						"}"))
				})
			})

			Context("and ca ratio is invalid", func() {
				BeforeEach(func() {
					caRatio = "364"
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError("{" +
						`"process-time-validation-error":"process time (invalidDuration) is not in the supported format is: 2d3h1m2s",` +
						`"ca-ratio-validation-error":"c\u0026a ratio must be a valid percentage but was: 364"` +
						"}"))
				})
			})
		})

		Context("and process time is valid", func() {
			Context("and process time is more than lead time", func() {
				BeforeEach(func() {
					processTime = "20m"
				})

				Context("and ca ratio is valid", func() {
					BeforeEach(func() {
						caRatio = "100%"
					})

					It("returns an error", func() {
						Ω(err).Should(MatchError("{" +
							`"activity-ratio-validation-error":"lead time must be longer than process time, lead time was: 10m, process time was: 20m"` +
							"}"))
					})
				})

				Context("and ca ratio is invalid", func() {
					BeforeEach(func() {
						caRatio = "364"
					})

					It("returns an error", func() {
						Ω(err).Should(MatchError("{" +
							`"activity-ratio-validation-error":"lead time must be longer than process time, lead time was: 10m, process time was: 20m",` +
							`"ca-ratio-validation-error":"c\u0026a ratio must be a valid percentage but was: 364"` +
							"}"))
					})
				})

			})

			Context("and process time is less than lead time", func() {
				BeforeEach(func() {
					processTime = "5m"
				})

				Context("and ca ratio is invalid", func() {
					BeforeEach(func() {
						caRatio = "364"
					})

					It("returns an error", func() {
						Ω(err).Should(MatchError("{" +
							`"ca-ratio-validation-error":"c\u0026a ratio must be a valid percentage but was: 364"` +
							"}"))
					})
				})

				Context("and ca ratio is valid", func() {
					BeforeEach(func() {
						caRatio = "20%"
					})

					It("returns nil", func() {
						Ω(err).Should(BeNil())
					})
				})
			})
		})
	})
})
