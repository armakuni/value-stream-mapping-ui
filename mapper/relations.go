package mapper

// Relations a collection of relation
type Relations []Relation

// Relation the links between resources
type Relation struct {
	From string `json:"from"`
	To   string `json:"to"`
	Note string `json:"note,omitempty"`
}
