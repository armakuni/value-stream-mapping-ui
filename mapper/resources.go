package mapper

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/RyanCarrier/dijkstra"
)

// Resources a collection of resource
type Resources []*Resource

// Resource -
type Resource struct {
	Type              string   `json:"type"`
	Title             string   `json:"title"`
	Function          string   `json:"function,omitempty"`
	Process           string   `json:"process"`
	Description       string   `json:"description,omitempty"`
	NumberOfStaff     int      `json:"number_of_staff,omitempty"`
	Kaizen            string   `json:"kaizen,omitempty"`
	FlowBlockers      string   `json:"flow_blockers,omitempty"`
	PreviousProcess   string   `json:"previous_process,omitempty"`
	PreviousProcesses []*Edge  `json:"previous_processes,omitempty"`
	Metrics           *Metrics `json:"metrics,omitempty"`
	Flow              *Flow    `json:"flow,omitempty"`
	AutomatedFlow     *Flow    `json:"automated_flow,omitempty"`
	LongestPath       bool     `json:"-"`
	LeadTimeWeight    float64  `json:"-"`
	Out               []Edge   `json:"-"`
	In                []Edge   `json:"-"`
}

// Edge the links between resources
type Edge struct {
	Process   string `json:"process"`
	*Resource `json:"-"`
	Note      string `json:"note,omitempty"`
}

// Flow - defines information flows for a resource
type Flow struct {
	In  []string `json:"in,omitempty"`
	Out []string `json:"out,omitempty"`
}

// FlowResource - an enhanced flow struct that links flows to a resource
type FlowResource struct {
	Resource  Resource `json:"-"`
	Relation  Relation `json:"-"`
	Flow      string   `json:"-"`
	Automated bool     `json:"-"`
}

// FlowResources - a collection of FlowResource
type FlowResources []FlowResource

type resourceValidatorErr struct {
	Metrics *metricsValidatorErr `json:"metrics-validation-error,omitempty"`
	Process string               `json:"process-validation-error,omitempty"`
	Type    string               `json:"type-validation-error,omitempty"`
}

func (r resourceValidatorErr) Error() string {
	return basicMarshal(r)
}

type resourcesValidatorErr struct {
	Resources        map[string]resourceValidatorErr `json:"resources-validation-error,omitempty"`
	EmptyResources   string                          `json:"empty-resources-validation-error,omitempty"`
	DuplicateProcess []string                        `json:"duplicate-process-validation-error,omitempty"`
	SkippedProcesses []string                        `json:"skipped-process-validation-error,omitempty"`
}

func (r resourcesValidatorErr) Error() string {
	return basicMarshal(r)
}

func (r resourcesValidatorErr) IsNil() bool {
	if len(r.Resources) == 0 && r.EmptyResources == "" && len(r.DuplicateProcess) == 0 && len(r.SkippedProcesses) == 0 {
		return true
	}
	return false
}

// Validate - validates a resource struct
func (resource Resource) Validate() error {
	var resourceValidation resourceValidatorErr

	if resource.Metrics != nil {
		err := resource.Metrics.Validate()
		if err != nil {
			metricsErr := err.(metricsValidatorErr)
			resourceValidation.Metrics = &metricsErr
		}
	}

	if resource.Process == "" {
		resourceValidation.Process = "process must have a value"
	}

	if resource.Type != "process" {
		resourceValidation.Type = "resource type must be one of: ['process']"
	}

	if resourceValidation != (resourceValidatorErr{}) {
		return resourceValidation
	}

	return nil
}

// Validate - validates a resource struct
func (resources Resources) Validate() error {
	var resourcesValidation resourcesValidatorErr
	if len(resources) == 0 {
		resourcesValidation.EmptyResources = "maps must have at least one resource"
	}

	resourcesValidationMap := make(map[string]resourceValidatorErr)

	for i, resource := range resources {
		err := resource.Validate()
		if err != nil {
			resourcesValidationMap[fmt.Sprintf("resource at index '%v' was invalid", i)] = err.(resourceValidatorErr)
		}
	}

	if len(resourcesValidationMap) != 0 {
		resourcesValidation.Resources = resourcesValidationMap
	}

	duplicateProcesses := resources.validateDuplicateProcessNames()
	if len(duplicateProcesses) > 0 {
		resourcesValidation.DuplicateProcess = duplicateProcesses
	}

	if len(resources) > 0 {
		skippedProcesses := resources.validateSkippedProcesses()
		if len(skippedProcesses) > 0 {
			resourcesValidation.SkippedProcesses = skippedProcesses
		}
	}

	if !resourcesValidation.IsNil() {
		return resourcesValidation
	}

	return nil
}

// CalculateEdges - Convert text previous process to in memory relations between objects
func (resources Resources) CalculateEdges() {
	resourceMap := make(map[string]*Resource)
	for _, resource := range resources {
		resourceMap[resource.Process] = resource
		resource.Out = make([]Edge, 0)
		resource.In = make([]Edge, 0)
	}

	for _, resource := range resources {
		if len(resource.PreviousProcesses) > 0 {
			for _, previousProcess := range resource.PreviousProcesses {

				if resourceMap[previousProcess.Process].Out == nil {
					resourceMap[previousProcess.Process].Out = make([]Edge, 0)
				}

				resourceMap[previousProcess.Process].Out = append(resourceMap[previousProcess.Process].Out, Edge{Process: resource.Process, Resource: resource, Note: previousProcess.Note})

				if resource.In == nil {
					resource.In = make([]Edge, 0)
				}

				resource.In = append(resource.In, Edge{Process: resourceMap[previousProcess.Process].Process, Resource: resourceMap[previousProcess.Process], Note: previousProcess.Note})
			}
		} else if resource.PreviousProcess != "" {

			if resourceMap[resource.PreviousProcess].Out == nil {
				resourceMap[resource.PreviousProcess].Out = make([]Edge, 0)
			}

			resourceMap[resource.PreviousProcess].Out = append(
				resourceMap[resource.PreviousProcess].Out,
				Edge{Process: resource.Process, Resource: resource})

			if resource.In == nil {
				resource.In = make([]Edge, 0)
			}

			resource.In = append(resource.In, Edge{Process: resourceMap[resource.PreviousProcess].Process, Resource: resourceMap[resource.PreviousProcess]})
		}
	}
}

// BuildRelations - generates relation objects based on resources
func (resources Resources) BuildRelations() Relations {
	resourceMap := make(map[string]*Resource)
	for _, resource := range resources {
		resourceMap[resource.Process] = resource
	}

	var relations Relations
	for _, resource := range resources {
		if len(resource.PreviousProcesses) > 0 {
			for _, previousProcess := range resource.PreviousProcesses {
				relations = append(relations, Relation{To: resource.Process, From: previousProcess.Process})
			}
		} else if resource.PreviousProcess != "" {
			relations = append(relations, Relation{To: resource.Process, From: resource.PreviousProcess})
		}
	}
	return relations
}

// CalculateLeadTimeWeights calculates the weighting of each object based on lead time
func (resources Resources) CalculateLeadTimeWeights() {
	totalLeadTime := resources.calcTotalLeadTime()

	if totalLeadTime == 0 {
		return
	}

	for _, resource := range resources {
		if resource.Metrics == nil {
			continue
		}
		resource.LeadTimeWeight = (resource.Metrics.LeadTimeDuration.Seconds() / totalLeadTime.Seconds()) * 100
	}
}

// HaveFlows returns true if at least one resource has a flow
func (resources Resources) HaveFlows() bool {
	for _, resource := range resources {
		if resource.HasFlows() {
			return true
		}
	}
	return false
}

// HasFlows returns true if the resource has a flow
func (resource Resource) HasFlows() bool {
	if resource.Flow != nil &&
		(len(resource.Flow.In) > 0 || len(resource.Flow.Out) > 0) {
		return true
	}

	if resource.AutomatedFlow != nil &&
		(len(resource.AutomatedFlow.In) > 0 || len(resource.AutomatedFlow.Out) > 0) {
		return true
	}

	return false
}

// BuildFlowResources builds Flows relationships with resources and relations
func (resource Resource) BuildFlowResources() FlowResources {
	var flowResource []FlowResource

	if resource.Flow != nil {
		for _, in := range resource.Flow.In {
			relation := Relation{
				From: in,
				To:   resource.Process,
			}
			flowResource = append(flowResource, FlowResource{Resource: resource, Relation: relation, Flow: in})
		}

		for _, out := range resource.Flow.Out {
			relation := Relation{
				From: resource.Process,
				To:   out,
			}
			flowResource = append(flowResource, FlowResource{Resource: resource, Relation: relation, Flow: out})
		}
	}

	if resource.AutomatedFlow != nil {
		for _, in := range resource.AutomatedFlow.In {
			relation := Relation{
				From: in,
				To:   resource.Process,
			}
			flowResource = append(flowResource, FlowResource{
				Resource:  resource,
				Relation:  relation,
				Flow:      in,
				Automated: true,
			})
		}

		for _, out := range resource.AutomatedFlow.Out {
			relation := Relation{
				From: resource.Process,
				To:   out,
			}
			flowResource = append(flowResource, FlowResource{
				Resource:  resource,
				Relation:  relation,
				Flow:      out,
				Automated: true,
			})
		}
	}
	return flowResource
}

// BuildFlowResources builds Flow relationships for all resources
func (resources Resources) BuildFlowResources() FlowResources {
	var totalFlowResources []FlowResource
	for _, resource := range resources {
		flowResources := resource.BuildFlowResources()
		totalFlowResources = append(totalFlowResources, flowResources...)
	}
	return totalFlowResources
}

// InformationFlows - returns a slice of strings for the names of all unique information flows
func (flowResources FlowResources) InformationFlows() []string {
	flowsMap := make(map[string]struct{})
	for _, flowResource := range flowResources {
		flowsMap[flowResource.Flow] = struct{}{}
	}
	var flows []string
	for flow := range flowsMap {
		flows = append(flows, flow)
	}
	sort.Strings(flows)
	return flows
}

func (resources Resources) calcTotalLeadTime() time.Duration {
	var totalLeadTime time.Duration

	for _, resource := range resources {
		if resource.Metrics == nil || !resource.LongestPath {
			continue
		}

		totalLeadTime += resource.Metrics.LeadTimeDuration
	}

	return totalLeadTime
}

func (resources Resources) calcTotalProcessTime() time.Duration {
	var totalProcessTime time.Duration

	for _, resource := range resources {
		if resource.Metrics == nil || !resource.LongestPath {
			continue
		}
		totalProcessTime += resource.Metrics.ProcessTimeDuration
	}

	return totalProcessTime
}

func (resources Resources) calcCARatioSummary() (string, error) {
	var (
		caRatioTotal float64
		caRatioCount int
	)

	for _, resource := range resources {
		if resource.Metrics == nil ||
			resource.Metrics.CARatio == "" || !resource.LongestPath {
			continue
		}
		caRatio, err := strconv.ParseFloat(strings.Split(resource.Metrics.CARatio, "%")[0], 64)
		if err != nil {
			return "", err
		}
		caRatioTotal += caRatio
		caRatioCount++
	}
	var ratio int
	if caRatioCount != 0 {
		ratio = int(caRatioTotal / float64(caRatioCount))
	}
	return fmt.Sprintf("%v%%", ratio), nil
}
func (resources Resources) validateDuplicateProcessNames() []string {
	resourceNameMap := make(map[string][]int)
	for i, resource := range resources {
		resourceNameMap[resource.Process] = append(resourceNameMap[resource.Process], i)
	}

	var duplicateProcessNames []string
	for processName, indexes := range resourceNameMap {
		if len(indexes) > 1 {
			duplicateProcessNames = append(duplicateProcessNames,
				fmt.Sprintf("processes at indexes %v have the same process name of '%s', process names must be unique",
					indexes,
					processName,
				),
			)
		}
	}
	sort.Strings(duplicateProcessNames)
	return duplicateProcessNames
}
func (resources Resources) validateSkippedProcesses() []string {
	resources.CalculateEdges()

	var processToWalk []*Resource
	var nextProcessesRow []*Resource
	var skippedProcesses []string

	var seenIds map[string]bool
	seenIds = make(map[string]bool)

	processToWalk = append(processToWalk, resources.Start())

	// BFS
	for len(processToWalk) > 0 {
		resource := processToWalk[0]
		seenIds[resource.Process] = true

		for _, linkedRelation := range resource.Out {
			for _, sameColumnProcess := range processToWalk {
				// Check resource isn't related to element in same column
				//  A -> B -> E
				//    \  ^
				//     \ C
				if sameColumnProcess.Process == linkedRelation.Process {
					skippedProcesses = append(skippedProcesses,
						fmt.Sprintf("processes can only be related to processes ahead of them resource %s links to process %s which is not ahead of it",
							resource.Process,
							sameColumnProcess.Process,
						))
				}
			}
		}

		for _, nextResource := range resource.Out {
			// Check resource isn't present in previous column
			//  A -> B  -> E
			//    \     \
			//     \ C ->  D
			if _, ok := seenIds[nextResource.Process]; ok {
				skippedProcesses = append(skippedProcesses,
					fmt.Sprintf("processes can only be related to processes ahead of them resource %s links to process %s which is not ahead of it",
						resource.Process,
						nextResource.Process,
					))
				continue
			}

			nextProcessesRow = append(nextProcessesRow, nextResource.Resource)
		}

		if len(processToWalk) > 1 {
			processToWalk = processToWalk[1:]
		} else {
			processToWalk = nextProcessesRow
			nextProcessesRow = []*Resource{}
		}
	}

	return skippedProcesses
}

// CalculateDurations ensures that if there are metrics, they have been associated with the work day and durations generated
func (resources Resources) CalculateDurations(workDuration WorkDuration) error {
	for _, resource := range resources {
		if resource.Metrics != nil {
			if err := resource.Metrics.CalculateDurations(workDuration); err != nil {
				return err
			}

			if resource.Metrics.LeadTime != "" {
				resource.Metrics.LeadTime = workDuration.DurationToString(resource.Metrics.LeadTimeDuration)
			}
			if resource.Metrics.ProcessTime != "" {
				resource.Metrics.ProcessTime = workDuration.DurationToString(resource.Metrics.ProcessTimeDuration)
			}
		}
	}

	return nil
}

// Start - Get the first node in the resource graph
func (resources Resources) Start() *Resource {
	resources.CalculateEdges()
	for _, element := range resources {
		if len(element.In) == 0 {
			return element
		}
	}

	return nil
}

// End - Get the last node in the resource graph, preferring the longest path
func (resources Resources) End() *Resource {
	resources.CalculateEdges()
	var potentialEnds []*Resource
	for _, element := range resources {
		if len(element.Out) == 0 {
			potentialEnds = append(potentialEnds, element)
		}
	}

	for _, potentialEnd := range potentialEnds {
		if potentialEnd.LongestPath == true {
			return potentialEnd
		}
	}

	if len(potentialEnds) > 0 {
		return potentialEnds[0]
	}

	return nil
}

// HasMetrics - Does any resource has metrics
func (resources Resources) HasMetrics() bool {
	for _, resource := range resources {
		if resource.Metrics != nil {
			return true
		}
	}

	return false
}

// OutSorted - Gets the resources with the longest node first
func (resource Resource) OutSorted(skipIds map[string]bool) ([]Edge, map[string]bool) {
	var sortedOut []Edge
	var seenIds = skipIds

	for _, nextResource := range resource.Out {
		if _, ok := seenIds[nextResource.Process]; ok {
			continue
		}

		if nextResource.Resource.LongestPath == true {
			sortedOut = append([]Edge{nextResource}, sortedOut...)
		} else {
			sortedOut = append(sortedOut, nextResource)
		}

		seenIds[nextResource.Process] = true
	}

	return sortedOut, seenIds
}

// CalculateLongestPath - Calculate the longest way through the graph
func (resources Resources) CalculateLongestPath() {
	var processToIndexMap map[string]int
	processToIndexMap = make(map[string]int)
	graph := dijkstra.NewGraph()
	for index, resource := range resources {
		graph.AddVertex(index)

		processToIndexMap[resource.Process] = index
	}

	for index, resource := range resources {
		for outIndex, out := range resource.Out {
			if out.Metrics != nil && resource.Metrics != nil {
				weight := int64(resource.Metrics.LeadTimeDuration.Seconds() + out.Metrics.LeadTimeDuration.Seconds())
				graph.AddArc(index, processToIndexMap[out.Process], weight)
			} else {
				// This ensures we always prioritise any metrics, over none at all.
				graph.AddArc(index, processToIndexMap[out.Process], int64(0-outIndex))
			}
		}
	}

	startProcess := resources.Start()
	endProcess := resources.End()

	if startProcess == nil || endProcess == nil {
		return
	}

	bestPath, _ := graph.Longest(processToIndexMap[startProcess.Process], processToIndexMap[endProcess.Process])

	for _, index := range bestPath.Path {
		resources[index].LongestPath = true
	}
}
