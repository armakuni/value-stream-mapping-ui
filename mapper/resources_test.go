package mapper_test

import (
	"time"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Resources#CalculateDurations", func() {
	var (
		resources mapper.Resources
	)

	Context("when there are no metrics", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Type:        "process",
					Title:       "test1",
					Description: "test",
				},
				{
					Type:        "process",
					Title:       "test2",
					Description: "test",
				},
			}
			workDuration, _ := mapper.NewWorkDuration(7*time.Hour, 5*7*time.Hour)
			resources.CalculateDurations(workDuration)
		})

		It("if there are none at all nothing should happen", func() {
			Ω(resources[0].Metrics).Should(BeNil())
			Ω(resources[1].Metrics).Should(BeNil())
		})
	})
	Context("when there are metrics", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						ProcessTime: "10h",
						LeadTime:    "12h",
					},
				},
				{
					Type:        "process",
					Title:       "test2",
					Description: "test",
					Metrics: &mapper.Metrics{
						ProcessTime: "10h",
						LeadTime:    "12h",
					},
				},
			}
			workDuration, _ := mapper.NewWorkDuration(7*time.Hour, 5*7*time.Hour)
			resources.CalculateDurations(workDuration)
		})

		It("should calculate the process time duration", func() {
			expected, _ := time.ParseDuration("10h")

			Ω(resources[0].Metrics.ProcessTimeDuration).Should(Equal(expected))
			Ω(resources[1].Metrics.ProcessTimeDuration).Should(Equal(expected))
		})

		It("should calculate the lead time duration", func() {
			expected, _ := time.ParseDuration("12h")

			Ω(resources[0].Metrics.LeadTimeDuration).Should(Equal(expected))
			Ω(resources[1].Metrics.LeadTimeDuration).Should(Equal(expected))
		})

		It("should normalise the text based lead time into days", func() {
			Ω(resources[0].Metrics.LeadTime).Should(Equal("1d5h"))
			Ω(resources[1].Metrics.LeadTime).Should(Equal("1d5h"))
		})

		It("should normalise the text based lead time into days", func() {
			Ω(resources[0].Metrics.ProcessTime).Should(Equal("1d3h"))
			Ω(resources[1].Metrics.ProcessTime).Should(Equal("1d3h"))
		})
	})
	Context("when there are a mixture of metrics and not", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						ProcessTime: "2h",
						LeadTime:    "3h",
					},
				},
				{
					Type:        "process",
					Title:       "test2",
					Description: "test",
				},
			}
			workDuration, _ := mapper.NewWorkDuration(7*time.Hour, 5*7*time.Hour)
			resources.CalculateDurations(workDuration)
		})

		It("should calculate the process time duration for those with metrics", func() {
			expected, _ := time.ParseDuration("2h")

			Ω(resources[0].Metrics.ProcessTimeDuration).Should(Equal(expected))
		})

		It("should calculate the lead time duration for those with metrics", func() {
			expected, _ := time.ParseDuration("3h")

			Ω(resources[0].Metrics.LeadTimeDuration).Should(Equal(expected))
		})
		It("if there are none at all nothing should happen", func() {
			Ω(resources[1].Metrics).Should(BeNil())
		})
	})

	Context("metrics with invalid durations", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						ProcessTime: "broken",
						LeadTime:    "3h",
					},
				},
			}
			workDuration, _ := mapper.NewWorkDuration(7*time.Hour, 5*7*time.Hour)
			resources.CalculateDurations(workDuration)
		})
	})
})

var _ = Describe("Resources#BuildRelations", func() {
	var (
		relations mapper.Relations
		resources mapper.Resources
	)

	JustBeforeEach(func() {
		resources.CalculateEdges()
		relations = resources.BuildRelations()
	})

	Context("when there are no relations", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Type:        "process",
					Title:       "test1",
					Description: "test",
				},
			}
		})

		It("returns an empty relations object", func() {
			Ω(relations).Should(HaveLen(0))
		})
	})

	Context("when there are liner relations", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
				},
				{
					Process:         "2",
					Type:            "process",
					Title:           "test2",
					Description:     "test",
					PreviousProcess: "1",
				},
			}
			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(relations).Should(HaveLen(1))
			Ω(relations[0]).Should(Equal(mapper.Relation{From: "1", To: "2"}))
		})

		It("generates from relations on the resources", func() {
			Ω(resources[0].In).Should(HaveLen(0))
			Ω(resources[0].Out).Should(HaveLen(1))
			Ω(resources[0].Out[0].Resource).Should(Equal(resources[1]))
			Ω(resources[1].In).Should(HaveLen(1))
			Ω(resources[1].In[0].Resource).Should(Equal(resources[0]))
			Ω(resources[1].Out).Should(HaveLen(0))
		})
	})

	Context("when there are parallel relations", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:     "3",
					Type:        "process",
					Title:       "test1",
					Description: "test",
					PreviousProcesses: []*mapper.Edge{
						{Process: "2-1", Note: "10%"},
						{Process: "2-2", Note: "90%"},
					},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(relations).Should(HaveLen(4))
			Ω(relations[0]).Should(Equal(mapper.Relation{From: "1", To: "2-1"}))
			Ω(relations[1]).Should(Equal(mapper.Relation{From: "1", To: "2-2"}))
			Ω(relations[2]).Should(Equal(mapper.Relation{From: "2-1", To: "3"}))
			Ω(relations[3]).Should(Equal(mapper.Relation{From: "2-2", To: "3"}))
		})

		It("generates from relations on the resources", func() {
			Ω(resources[0].In).Should(HaveLen(0))
			Ω(resources[0].Out).Should(HaveLen(2))
			Ω(resources[0].Out[0].Process).Should(Equal(resources[1].Process))
			Ω(resources[0].Out[1].Process).Should(Equal(resources[2].Process))

			Ω(resources[1].In).Should(HaveLen(1))
			Ω(resources[1].In[0].Process).Should(Equal(resources[0].Process))
			Ω(resources[1].Out).Should(HaveLen(1))
			Ω(resources[1].Out[0].Process).Should(Equal(resources[3].Process))

			Ω(resources[2].In).Should(HaveLen(1))
			Ω(resources[2].In[0].Process).Should(Equal(resources[0].Process))
			Ω(resources[2].Out).Should(HaveLen(1))
			Ω(resources[2].Out[0].Process).Should(Equal(resources[3].Process))

			Ω(resources[3].In).Should(HaveLen(2))
			Ω(resources[3].In[0].Process).Should(Equal(resources[1].Process))
			Ω(resources[3].In[0].Note).Should(Equal("10%"))
			Ω(resources[3].In[1].Process).Should(Equal(resources[2].Process))
			Ω(resources[3].In[1].Note).Should(Equal("90%"))
			Ω(resources[3].Out).Should(HaveLen(0))
		})
	})
})

var _ = Describe("Resources#CalculateLeadTimeWeights", func() {
	Context("when at least one resource has a lead time", func() {
		var resources = mapper.Resources{
			{
				Metrics: &mapper.Metrics{
					LeadTimeDuration: 1 * time.Hour,
				},
				LongestPath: true,
			},
			{
				Metrics: &mapper.Metrics{
					LeadTimeDuration: 1 * time.Hour,
				},
				LongestPath: true,
			},
			{
				Metrics: &mapper.Metrics{
					LeadTimeDuration: 2 * time.Hour,
				},
				LongestPath: true,
			},
			{
				Metrics:     &mapper.Metrics{},
				LongestPath: true,
			},
			{},
		}

		It("calculates the weights from lead times", func() {
			resources.CalculateLeadTimeWeights()
			Ω(resources).Should(HaveLen(5))
			Ω(resources[0].LeadTimeWeight).Should(Equal(25.0))
			Ω(resources[1].LeadTimeWeight).Should(Equal(25.0))
			Ω(resources[2].LeadTimeWeight).Should(Equal(50.0))
			Ω(resources[3].LeadTimeWeight).Should(Equal(0.0))
			Ω(resources[4].LeadTimeWeight).Should(Equal(0.0))
		})
	})

	Context("when no resources have a lead time", func() {
		var resources = mapper.Resources{
			{
				Metrics: &mapper.Metrics{},
			},
			{
				Metrics: &mapper.Metrics{},
			},
			{
				Metrics: &mapper.Metrics{},
			},
			{
				Metrics: &mapper.Metrics{},
			},
		}

		It("calculates the weights from leadtimes", func() {
			resources.CalculateLeadTimeWeights()
			Ω(resources).Should(HaveLen(4))
			Ω(resources[0].LeadTimeWeight).Should(Equal(0.0))
			Ω(resources[1].LeadTimeWeight).Should(Equal(0.0))
			Ω(resources[2].LeadTimeWeight).Should(Equal(0.0))
			Ω(resources[3].LeadTimeWeight).Should(Equal(0.0))
		})
	})
})

var _ = Describe("Resources#HaveFlows", func() {
	Context("when at least one resource has an in flow", func() {
		var resources = mapper.Resources{
			{
				Title: "test1",
			},
			{
				Title: "test2",
				Flow: &mapper.Flow{
					In:  []string{"test"},
					Out: []string{},
				},
			},
		}

		It("returns true", func() {
			Ω(resources.HaveFlows()).Should(BeTrue())
		})
	})

	Context("when at least one resource has an out flow", func() {
		var resources = mapper.Resources{
			{
				Title: "test1",
			},
			{
				Title: "test2",
				Flow: &mapper.Flow{
					In:  []string{},
					Out: []string{"test"},
				},
			},
		}

		It("returns true", func() {
			Ω(resources.HaveFlows()).Should(BeTrue())
		})
	})

	Context("when no resources have a flow", func() {
		var resources = mapper.Resources{
			{
				Title: "test1",
			},
			{
				Title: "test2",
			},
		}

		It("returns false", func() {
			Ω(resources.HaveFlows()).Should(BeFalse())
		})
	})
})

var _ = Describe("Resource#HasFlows", func() {
	Context("when the resource has an in flow", func() {
		var resource = mapper.Resource{
			Title: "test2",
			Flow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{},
			},
		}

		It("returns true", func() {
			Ω(resource.HasFlows()).Should(BeTrue())
		})
	})

	Context("when the resource has an out flow", func() {
		var resource = mapper.Resource{
			Title: "test",
			Flow: &mapper.Flow{
				In:  []string{},
				Out: []string{"test"},
			},
		}

		It("returns true", func() {
			Ω(resource.HasFlows()).Should(BeTrue())
		})
	})

	Context("when the resource has an automated out flow", func() {
		var resource = mapper.Resource{
			Title: "test",
			AutomatedFlow: &mapper.Flow{
				In:  []string{},
				Out: []string{"test"},
			},
		}

		It("returns true", func() {
			Ω(resource.HasFlows()).Should(BeTrue())
		})
	})

	Context("when the resource has an automated in flow", func() {
		var resource = mapper.Resource{
			Title: "test",
			AutomatedFlow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{},
			},
		}

		It("returns true", func() {
			Ω(resource.HasFlows()).Should(BeTrue())
		})
	})

	Context("when he resource has no flows", func() {
		var resource = mapper.Resource{
			Title: "test",
		}

		It("returns false", func() {
			Ω(resource.HasFlows()).Should(BeFalse())
		})
	})
})

var _ = Describe("Resource#BuildFlowResources", func() {
	Context("when the resource has a flow in", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{},
			},
		}

		It("returns populated FlowResources", func() {
			Ω(resource.BuildFlowResources()).Should(Equal(mapper.FlowResources{
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: "test",
						To:   resource.Process,
					},
					Flow: "test",
				},
			}))
		})
	})

	Context("when the resource has a flow out", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{},
				Out: []string{"test"},
			},
			AutomatedFlow: &mapper.Flow{
				In:  []string{},
				Out: []string{"automated-test"},
			},
		}

		It("returns populated FlowResources", func() {
			Ω(resource.BuildFlowResources()).Should(Equal(mapper.FlowResources{
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: resource.Process,
						To:   "test",
					},
					Flow: "test",
				},
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: resource.Process,
						To:   "automated-test",
					},
					Flow:      "automated-test",
					Automated: true,
				},
			}))
		})
	})

	Context("when the resource has a flow in and out", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{"test"},
			},
			AutomatedFlow: &mapper.Flow{
				In:  []string{"automated-test"},
				Out: []string{},
			},
		}

		It("returns populated FlowResources", func() {
			Ω(resource.BuildFlowResources()).Should(Equal(mapper.FlowResources{
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: "test",
						To:   resource.Process,
					},
					Flow: "test",
				},
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: resource.Process,
						To:   "test",
					},
					Flow: "test",
				},
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: "automated-test",
						To:   resource.Process,
					},
					Automated: true,
					Flow:      "automated-test",
				},
			}))
		})
	})

	Context("when the resource has no flows", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
		}

		It("returns no FlowResources", func() {
			Ω(resource.BuildFlowResources()).Should(HaveLen(0))
		})
	})
})
var _ = Describe("Resources#BuildFlowResources", func() {
	Context("when the resource has a flow in", func() {
		resource := mapper.Resource{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{},
			},
		}
		resources := mapper.Resources{&resource}

		It("returns populated FlowResources", func() {
			Ω(resources.BuildFlowResources()).Should(Equal(mapper.FlowResources{
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: "test",
						To:   "123",
					},
					Flow: "test",
				},
			}))
		})
	})

	Context("when the resource has a flow out", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{},
				Out: []string{"test"},
			},
			AutomatedFlow: &mapper.Flow{
				In:  []string{},
				Out: []string{"automated-test"},
			},
		}
		resources := mapper.Resources{&resource}

		It("returns populated FlowResources", func() {
			Ω(resources.BuildFlowResources()).Should(Equal(mapper.FlowResources{
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: resource.Process,
						To:   "test",
					},
					Flow: "test",
				},
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: resource.Process,
						To:   "automated-test",
					},
					Flow:      "automated-test",
					Automated: true,
				},
			}))
		})
	})

	Context("when the resource has a flow in and out", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{"test"},
			},
			AutomatedFlow: &mapper.Flow{
				In:  []string{"automated-test"},
				Out: []string{},
			},
		}
		resources := mapper.Resources{&resource}

		It("returns populated FlowResources", func() {
			Ω(resources.BuildFlowResources()).Should(Equal(mapper.FlowResources{
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: "test",
						To:   resource.Process,
					},
					Flow: "test",
				},
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: resource.Process,
						To:   "test",
					},
					Flow: "test",
				},
				{
					Resource: resource,
					Relation: mapper.Relation{
						From: "automated-test",
						To:   resource.Process,
					},
					Automated: true,
					Flow:      "automated-test",
				},
			}))
		})
	})

	Context("when the resource has no flows", func() {
		var resource = mapper.Resource{
			Title:   "test",
			Process: "123",
		}
		resources := mapper.Resources{&resource}

		It("returns no FlowResources", func() {
			Ω(resources.BuildFlowResources()).Should(HaveLen(0))
		})
	})
})

var _ = Describe("FlowResources#InformationFlows", func() {
	var flowResources = &mapper.FlowResources{
		{
			Flow: "flow1",
		},
		{
			Flow: "flow1",
		},
		{
			Flow: "flow2",
		},
		{
			Flow: "flow1",
		},
		{
			Flow: "flow3",
		},
		{
			Flow: "flow2",
		},
	}
	It("returns slice of names of all unique flows", func() {
		flows := flowResources.InformationFlows()
		Ω(flows).Should(HaveLen(3))
		Ω(flows).Should(ContainElement("flow1"))
		Ω(flows).Should(ContainElement("flow2"))
		Ω(flows).Should(ContainElement("flow3"))
	})
})

var _ = Describe("Resources#BuildFlowResources", func() {
	var resources = mapper.Resources{
		{
			Title:   "test",
			Process: "123",
			Flow: &mapper.Flow{
				In:  []string{"test"},
				Out: []string{"test"},
			},
		},
		{
			Title:   "test2",
			Process: "456",
		},
		{
			Title:   "test3",
			Process: "789",
			Flow: &mapper.Flow{
				In:  []string{"test2"},
				Out: []string{},
			},
		},
	}

	It("creates flowResources for all Resources", func() {
		Ω(resources.BuildFlowResources()).Should(Equal(mapper.FlowResources{
			{
				Resource: *resources[0],
				Relation: mapper.Relation{
					From: "test",
					To:   resources[0].Process,
				},
				Flow: "test",
			},
			{
				Resource: *resources[0],
				Relation: mapper.Relation{
					From: resources[0].Process,
					To:   "test",
				},
				Flow: "test",
			},
			{
				Resource: *resources[2],
				Relation: mapper.Relation{
					From: "test2",
					To:   resources[2].Process,
				},
				Flow: "test2",
			},
		}))
	})
})

var _ = Describe("Resource#Validate", func() {
	var (
		metrics      *mapper.Metrics
		resourceType = "process"
		process      = "aProcess"
		err          error
	)

	JustBeforeEach(func() {
		resource := mapper.Resource{
			Metrics: metrics,
			Type:    resourceType,
			Process: process,
		}
		err = resource.Validate()
	})

	AfterEach(func() {
		metrics = &mapper.Metrics{}
		resourceType = "process"
		process = "aProcess"
	})

	Context("when metrics are invalid", func() {
		BeforeEach(func() {
			metrics = &mapper.Metrics{
				LeadTime: "notADuration",
			}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"metrics-validation-error":{` +
				`"lead-time-validation-error":"lead time (notADuration) is not in the supported format is: 2d3h1m2s"` +
				"}" +
				"}"))
		})
	})

	Context("when process is invalid", func() {
		BeforeEach(func() {
			process = ""
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"process-validation-error":"process must have a value"` +
				"}"))
		})
	})

	Context("when type is invalid", func() {
		BeforeEach(func() {
			resourceType = "notDefined"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"type-validation-error":"resource type must be one of: ['process']"` +
				"}"))
		})
	})

	Context("when a resource is valid", func() {
		It("returns nil", func() {
			Ω(err).Should(BeNil())
		})
	})
})

var _ = Describe("Resources#validate", func() {
	var (
		resources mapper.Resources
		err       error
	)

	JustBeforeEach(func() {
		err = resources.Validate()
	})

	Context("when there are no resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"empty-resources-validation-error":"maps must have at least one resource"` +
				"}"))
		})
	})

	Context("when a resource is invalid", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "",
					Type:    "process",
				},
			}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"resources-validation-error":{` +
				`"resource at index '0' was invalid":{` +
				`"process-validation-error":"process must have a value"` +
				"}" +
				"}" +
				"}"))
		})
	})

	Context("when two resources have the same name", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "process1",
					Type:    "process",
				},
				{
					Process: "process1",
					Type:    "process",
				},
				{
					Process: "process2",
					Type:    "process",
				},
				{
					Process: "process1",
					Type:    "process",
				},
				{
					Process: "process3",
					Type:    "process",
				},
				{
					Process: "process3",
					Type:    "process",
				},
			}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"duplicate-process-validation-error":[` +
				`"processes at indexes [0 1 3] have the same process name of 'process1', process names must be unique",` +
				`"processes at indexes [4 5] have the same process name of 'process3', process names must be unique"` +
				"]" +
				"}"))
		})
	})

	Context("when a split is used to skip a resource", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "a",
					Type:    "process",
				},
				{
					Process: "b",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "a",
						},
					},
				},
				{
					Process: "c",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "b",
						},
					},
				},
				{
					Process: "d",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "b",
						},
						{
							Process: "c",
						},
						{
							Process: "a",
						},
					},
				},
				{
					Process: "e",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "c",
						},
						{
							Process: "d",
						},
					},
				},
			}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"skipped-process-validation-error":[` +
				`"processes can only be related to processes ahead of them resource b links to process d which is not ahead of it",` +
				`"processes can only be related to processes ahead of them resource c links to process d which is not ahead of it",` +
				`"processes can only be related to processes ahead of them resource c links to process e which is not ahead of it",` +
				`"processes can only be related to processes ahead of them resource c links to process d which is not ahead of it",` +
				`"processes can only be related to processes ahead of them resource d links to process e which is not ahead of it"` +
				"]" +
				"}"))
		})
	})

	Context("when a split is used to skip back to a previous resource", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "a",
					Type:    "process",
				},
				{
					Process: "b",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "a",
						},
					},
				},
				{
					Process: "c",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "b",
						},
					},
				},
				{
					Process: "d",
					Type:    "process",
					Metrics: &mapper.Metrics{
						ProcessTime: "1h",
						LeadTime:    "2h",
					},
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "c",
						},
						{
							Process: "a",
						},
					},
				},
				{
					Process: "e",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "d",
						},
					},
				},
			}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"skipped-process-validation-error":[` +
				`"processes can only be related to processes ahead of them resource c links to process d which is not ahead of it"` +
				"]" +
				"}"))
		})
	})

	Context("when a split is used to skip to a process in the same column", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "a",
					Type:    "process",
				},
				{
					Process: "b",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "a",
						},
					},
				},
				{
					Process: "c",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "b",
						},
					},
				},
				{
					Process: "d",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "b",
						},
						{
							Process: "a",
						},
					},
				},
				{
					Process: "e",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "d",
						},
						{
							Process: "c",
						},
					},
				},
			}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"skipped-process-validation-error":[` +
				`"processes can only be related to processes ahead of them resource b links to process d which is not ahead of it",` +
				`"processes can only be related to processes ahead of them resource c links to process e which is not ahead of it",` +
				`"processes can only be related to processes ahead of them resource d links to process e which is not ahead of it"` +
				"]" +
				"}"))
		})
	})

	Context("when a normal split is used", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "a",
					Type:    "process",
				},
				{
					Process: "b",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "a",
						},
					},
				},
				{
					Process: "c",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "a",
						},
					},
				},
				{
					Process: "d",
					Type:    "process",
					PreviousProcesses: []*mapper.Edge{
						{
							Process: "b",
						},
					},
				},
			}
		})

		It("returns an error", func() {
			Ω(err).Should(BeNil())
		})
	})

	Context("when all resources are valid", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "process1",
					Type:    "process",
				},
				{
					Process: "process2",
					Type:    "process",
				},
			}
		})

		It("returns nil", func() {
			Ω(err).Should(BeNil())
		})
	})
})

var _ = Describe("Resources#Start", func() {
	Context("nodes have had the longest route calculation run on them", func() {
		It("chooses the first ", func() {
			var resources = mapper.Resources{
				{
					Process:         "5",
					PreviousProcess: "4",
					LongestPath:     true,
				},
				{
					Process:         "1",
					PreviousProcess: "0",
					LongestPath:     true,
				},
				{
					Process:         "st-cr",
					PreviousProcess: "1",
				},
				{
					Process:         "3",
					PreviousProcess: "st-cr",
					LongestPath:     true,
				},
				{
					Process:         "4",
					PreviousProcess: "3",
					LongestPath:     true,
				},

				{
					Process:     "0",
					LongestPath: true,
				},
				{
					Process:         "6",
					PreviousProcess: "5",
					LongestPath:     true,
				},

				{
					Process:         "10",
					PreviousProcess: "9",
					LongestPath:     true,
				},
				{
					Process:         "7",
					PreviousProcess: "6",
					LongestPath:     true,
				},
				{
					Process:         "8",
					PreviousProcess: "7",
					LongestPath:     true,
				},
				{
					Process:         "9",
					PreviousProcess: "8",
					LongestPath:     true,
				},
				{
					Process:         "11",
					PreviousProcess: "10",
					LongestPath:     true,
				},
				{
					Process:         "12",
					PreviousProcess: "11",
					LongestPath:     true,
				},
				{
					Process:         "13",
					PreviousProcess: "12",
					LongestPath:     true,
				},
				{
					Process:         "14",
					PreviousProcess: "13",
					LongestPath:     true,
				},
				{
					Process:         "15",
					PreviousProcess: "14",
					LongestPath:     true,
				},
				{
					Process:         "16",
					PreviousProcess: "15",
					LongestPath:     true,
				},
			}
			resource := resources.Start()
			Ω(resource.Process).Should(Equal("0"))
		})
	})
})
var _ = Describe("Resources#Start", func() {
	Context("nodes have had the longest route calculation run on them", func() {
		It("chooses the last ", func() {
			var resources = mapper.Resources{
				{
					Process:         "5",
					PreviousProcess: "4",
					LongestPath:     true,
				},
				{
					Process:         "1",
					PreviousProcess: "0",
					LongestPath:     true,
				},
				{
					Process:         "st-cr",
					PreviousProcess: "15",
				},
				{
					Process:         "2",
					PreviousProcess: "1",
					LongestPath:     true,
				},
				{
					Process:         "3",
					PreviousProcess: "2",
					LongestPath:     true,
				},
				{
					Process:         "4",
					PreviousProcess: "3",
					LongestPath:     true,
				},

				{
					Process:     "0",
					LongestPath: true,
				},
				{
					Process:         "6",
					PreviousProcess: "5",
					LongestPath:     true,
				},

				{
					Process:         "10",
					PreviousProcess: "9",
					LongestPath:     true,
				},
				{
					Process:         "7",
					PreviousProcess: "6",
					LongestPath:     true,
				},
				{
					Process:         "8",
					PreviousProcess: "7",
					LongestPath:     true,
				},
				{
					Process:         "9",
					PreviousProcess: "8",
					LongestPath:     true,
				},
				{
					Process:         "11",
					PreviousProcess: "10",
					LongestPath:     true,
				},
				{
					Process:         "12",
					PreviousProcess: "11",
					LongestPath:     true,
				},
				{
					Process:         "13",
					PreviousProcess: "12",
					LongestPath:     true,
				},
				{
					Process:         "14",
					PreviousProcess: "13",
					LongestPath:     true,
				},
				{
					Process:         "15",
					PreviousProcess: "14",
					LongestPath:     true,
				},
				{
					Process:         "16",
					PreviousProcess: "15",
					LongestPath:     true,
				},
			}
			resource := resources.End()
			Ω(resource.Process).Should(Equal("16"))
		})
	})
})
var _ = Describe("Resources#CalculateLongestPath", func() {
	var resources mapper.Resources
	JustBeforeEach(func() {
		workDuration, _ := mapper.NewWorkDuration(7*time.Hour, 5*7*time.Hour)
		resources.CalculateEdges()
		resources.CalculateDurations(workDuration)
		resources.CalculateLongestPath()
	})
	Context("simple longest path calculation without metrics", func() {

		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "1",
				},
				{
					Process:         "2",
					PreviousProcess: "1",
				},
				{
					Process:         "3",
					PreviousProcess: "2",
				},
			}
		})

		It("all elements are on the longest path", func() {
			Ω(resources[0].Process).Should(Equal("1"))
			Ω(resources[0].LongestPath).Should(BeTrue())
			Ω(resources[1].Process).Should(Equal("2"))
			Ω(resources[1].LongestPath).Should(BeTrue())
			Ω(resources[2].Process).Should(Equal("3"))
			Ω(resources[2].LongestPath).Should(BeTrue())
		})
	})
	Context("simple longest path calculation with split", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "1",
				},
				{
					Process:         "2",
					PreviousProcess: "1",
				},
				{
					Process:         "3",
					PreviousProcess: "1",
				},
				{
					Process:           "4",
					PreviousProcesses: []*mapper.Edge{{Process: "3"}, {Process: "2"}},
				},
			}
		})
		It("only has one longest path", func() {
			Ω(resources[0].Process).Should(Equal("1"))
			Ω(resources[0].LongestPath).Should(BeTrue())
			Ω(resources[1].Process).Should(Equal("2"))
			Ω(resources[1].LongestPath).Should(BeTrue())
			Ω(resources[2].Process).Should(Equal("3"))
			Ω(resources[2].LongestPath).Should(BeFalse())
			Ω(resources[3].Process).Should(Equal("4"))
			Ω(resources[3].LongestPath).Should(BeTrue())
		})
	})
	Context("metrics are respected", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "3",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "4h",
						ProcessTime: "1h",
					},
				},
				{
					Process:           "4",
					PreviousProcesses: []*mapper.Edge{{Process: "3"}, {Process: "2"}},
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
			}
		})
		It("only has one longest path", func() {
			Ω(resources[0].Process).Should(Equal("1"))
			Ω(resources[0].LongestPath).Should(BeTrue())
			Ω(resources[1].Process).Should(Equal("2"))
			Ω(resources[1].LongestPath).Should(BeFalse())
			Ω(resources[2].Process).Should(Equal("3"))
			Ω(resources[2].LongestPath).Should(BeTrue())
			Ω(resources[3].Process).Should(Equal("4"))
			Ω(resources[3].LongestPath).Should(BeTrue())
		})
	})
	Context("a mixture of metrics and non-metrics still calculate longest route", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2",
					PreviousProcess: "1",
				},
				{
					Process:         "3",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "4h",
						ProcessTime: "1h",
					},
				},
				{
					Process:           "4",
					PreviousProcesses: []*mapper.Edge{{Process: "3"}, {Process: "2"}},
					LongestPath:       true,
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
			}
		})
		It("only has one longest path", func() {
			Ω(resources[0].Process).Should(Equal("1"))
			Ω(resources[0].LongestPath).Should(BeTrue())
			Ω(resources[1].Process).Should(Equal("2"))
			Ω(resources[1].LongestPath).Should(BeFalse())
			Ω(resources[2].Process).Should(Equal("3"))
			Ω(resources[2].LongestPath).Should(BeTrue())
			Ω(resources[3].Process).Should(Equal("4"))
			Ω(resources[3].LongestPath).Should(BeTrue())
		})
	})
})

var _ = Describe("Resources#HasMetrics", func() {
	var resources mapper.Resources
	Context("when there are metrics on none of the resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:           "3",
					Type:              "process",
					Title:             "test1",
					Description:       "test",
					PreviousProcesses: []*mapper.Edge{{Process: "2-1"}, {Process: "2-2"}},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(resources.HasMetrics()).Should(BeFalse())
		})
	})
	Context("when there are metrics on some of the resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:           "3",
					Type:              "process",
					Title:             "test1",
					Description:       "test",
					PreviousProcesses: []*mapper.Edge{{Process: "2-1"}, {Process: "2-2"}},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(resources.HasMetrics()).Should(BeTrue())
		})
	})
	Context("when there are metrics on all of the resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:           "3",
					Type:              "process",
					Title:             "test1",
					Description:       "test",
					PreviousProcesses: []*mapper.Edge{{Process: "2-1"}, {Process: "2-2"}},
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(resources.HasMetrics()).Should(BeTrue())
		})
	})
})

var _ = Describe("Resources#HasMetrics", func() {
	var resources mapper.Resources
	Context("when there are metrics on none of the resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:           "3",
					Type:              "process",
					Title:             "test1",
					Description:       "test",
					PreviousProcesses: []*mapper.Edge{{Process: "2-1"}, {Process: "2-2"}},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(resources.HasMetrics()).Should(BeFalse())
		})
	})
	Context("when there are metrics on some of the resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
				},
				{
					Process:           "3",
					Type:              "process",
					Title:             "test1",
					Description:       "test",
					PreviousProcesses: []*mapper.Edge{{Process: "2-1"}, {Process: "2-2"}},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(resources.HasMetrics()).Should(BeTrue())
		})
	})
	Context("when there are metrics on all of the resources", func() {
		BeforeEach(func() {
			resources = mapper.Resources{
				{
					Process:     "1",
					Type:        "process",
					Title:       "test1",
					Description: "test",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2-1",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:         "2-2",
					Type:            "process",
					Title:           "test1",
					Description:     "test",
					PreviousProcess: "1",
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
				{
					Process:           "3",
					Type:              "process",
					Title:             "test1",
					Description:       "test",
					PreviousProcesses: []*mapper.Edge{{Process: "2-1"}, {Process: "2-2"}},
					Metrics: &mapper.Metrics{
						LeadTime:    "1h",
						ProcessTime: "1h",
					},
				},
			}

			resources.CalculateEdges()
		})

		It("generates relations for processes", func() {
			Ω(resources.HasMetrics()).Should(BeTrue())
		})
	})
})
