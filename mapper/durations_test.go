package mapper_test

import (
	"time"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("#NewWorkDuration", func() {
	var workDay time.Duration
	var workWeek time.Duration
	var initErr error

	JustBeforeEach(func() {
		_, initErr = mapper.NewWorkDuration(workDay, workWeek)
	})

	Context("when week is greater than day", func() {
		BeforeEach(func() {
			workWeek = 8 * time.Hour
			workDay = 5 * workWeek
		})

		It("should have an init error", func() {
			Ω(initErr).Should(MatchError("work day must be less than work week"))
		})
	})
})

var _ = Describe("#NewWorkDurationFromDurationStrings", func() {
	var workDuration mapper.WorkDuration
	var workDay string
	var workWeek string
	var initErr error

	JustBeforeEach(func() {
		workDuration, initErr = mapper.NewWorkDurationFromDurationStrings(workDay, workWeek)
	})

	Context("when week is greater than day", func() {
		Context("both present", func() {
			BeforeEach(func() {
				workWeek = "8h"
				workDay = "10h"
			})

			It("should have an init error", func() {
				Ω(initErr).Should(MatchError("work day must be less than work week"))
			})
		})
		Context("week only present", func() {
			BeforeEach(func() {
				workWeek = "4h"
				workDay = ""
			})

			It("should have an init error", func() {
				Ω(initErr).Should(MatchError("work day must be less than work week"))
			})
		})

	})

	Context("when week is less than day", func() {
		BeforeEach(func() {
			workWeek = "4d"
			workDay = "7h"
		})

		It("should have no error", func() {
			Ω(initErr).ShouldNot(HaveOccurred())
		})

		It("work week should be correct", func() {
			Ω(workDuration.WorkWeek).Should(Equal(28 * time.Hour))
		})

		It("work day should be correct", func() {
			Ω(workDuration.WorkDay).Should(Equal(7 * time.Hour))
		})
	})
	Context("defaults on empty", func() {
		BeforeEach(func() {
			workWeek = ""
			workDay = ""
		})

		It("should have no error", func() {
			Ω(initErr).ShouldNot(HaveOccurred())
		})

		It("work week should be correct", func() {
			Ω(workDuration.WorkWeek).Should(Equal(0 * time.Hour))
		})

		It("work day should be correct", func() {
			Ω(workDuration.WorkDay).Should(Equal(0 * time.Hour))
		})
	})
})

var _ = Describe("workDuration#DurationToString", func() {
	var workDuration mapper.WorkDuration
	var workDay time.Duration
	var workWeek time.Duration
	var duration time.Duration
	var actual string
	var initErr error

	JustBeforeEach(func() {
		workDuration, initErr = mapper.NewWorkDuration(workDay, workWeek)
		actual = workDuration.DurationToString(duration)
	})

	Context("when the week is longer than the day", func() {
		Context("when a duration contains hours", func() {
			Context("and the hours are less than a day", func() {
				Context("and a day is much longer", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = 3 * time.Hour
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("3h"))
					})
				})
				Context("and a day is just longer", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = 3 * time.Hour
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay

						Ω(actual).Should(Equal("3h"))
					})
				})
			})

			Context("and the hours add up to an exact number of days", func() {
				Context("for a long day", func() {
					BeforeEach(func() {
						workDay = 24 * time.Hour
						workWeek = 5 * workDay
						duration = 48 * time.Hour
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("2d"))
					})
				})
				Context("for a short day", func() {
					BeforeEach(func() {
						workDay = 2 * time.Hour
						workWeek = 5 * workDay
						duration = 8 * time.Hour
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("4d"))
					})
				})
			})

			Context("and the hours add up to over a day with a remainder", func() {
				Context("for a standard week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = 9 * time.Hour
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("1d1h"))
					})
				})
				Context("for a shorter week", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = 9 * time.Hour
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("2d1h"))
					})
				})
			})

			Context("and the days are less than a week", func() {
				Context("and a week is much longer", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = 3 * workDay
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("3d"))
					})
				})
				Context("and a week is just longer", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = 4 * workDay
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("4d"))
					})
				})
			})

			Context("and the days add up to an exact number of weeks", func() {
				Context("for a long week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 7 * workDay
						duration = 14 * workDay
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("2w"))
					})
				})
				Context("for a short week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 4 * workDay
						duration = 8 * workDay
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("2w"))
					})
				})
			})

			Context("and the hours add up to over a week with a remainder", func() {
				Context("for a standard week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = 8 * workDay
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("1w3d"))
					})
				})
				Context("for a shorter week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 3 * workDay
						duration = 8 * workDay
					})

					It("should not have an error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("returns a time string", func() {
						Ω(actual).Should(Equal("2w2d"))
					})
				})
			})
		})

		Context("when a duration contains only min", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = 48 * time.Minute
			})

			It("should not have an error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("returns a time string", func() {
				Ω(actual).Should(Equal("48m"))
			})
		})

		Context("when a duration contains only sec", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = 48 * time.Second
			})

			It("should not have an error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("returns a time string", func() {
				Ω(actual).Should(Equal("48s"))
			})
		})

		Context("when a duration contains only ms", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = 48 * time.Millisecond
			})

			It("should not have an error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("returns a time string", func() {
				Ω(actual).Should(Equal("48ms"))
			})
		})

		Context("when a duration contains only micro sec", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = 48 * time.Microsecond
			})

			It("should not have an error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("returns a time string", func() {
				Ω(actual).Should(Equal("48µs"))
			})
		})
		Context("when a duration contains only nano sec", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = 48 * time.Nanosecond
			})

			It("should not have an error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("returns a time string", func() {
				Ω(actual).Should(Equal("48ns"))
			})
		})
	})
	Context("when day is greater than week", func() {
		BeforeEach(func() {
			workWeek = 8 * time.Hour
			workDay = 5 * workWeek
			duration = 48 * time.Nanosecond
		})

		It("should have an error", func() {
			Ω(initErr).Should(MatchError("work day must be less than work week"))
		})
	})
	Context("resulting duration has a power of 10 number of minutes", func() {
		BeforeEach(func() {
			workDay = 8 * time.Hour
			workWeek = 5 * workDay
			duration = workWeek + (5 * time.Hour) + (40 * time.Minute)
		})

		It("should not have an error", func() {
			Ω(initErr).ShouldNot(HaveOccurred())
		})

		It("returns a time string", func() {
			Ω(actual).Should(Equal("1w5h40m"))
		})
	})
	Context("zero seconds duration", func() {
		BeforeEach(func() {
			workDay = 8 * time.Hour
			workWeek = 5 * workDay
			duration = time.Duration(0)
		})

		It("should not have an error", func() {
			Ω(initErr).ShouldNot(HaveOccurred())
		})

		It("returns a empty time string", func() {
			Ω(actual).Should(Equal("0s"))
		})
	})
})

var _ = Describe("workDuration#StringToDuration", func() {
	var workDuration mapper.WorkDuration
	var workDay time.Duration
	var workWeek time.Duration
	var actual time.Duration
	var duration string
	var initErr error
	var err error

	JustBeforeEach(func() {
		workDuration, initErr = mapper.NewWorkDuration(workDay, workWeek)

		if initErr == nil {
			actual, err = workDuration.StringToDuration(duration)
		}
	})

	Context("when the week is longer than the day", func() {
		Context("when a actual contains hours", func() {
			Context("and the hours are less than a day", func() {
				Context("and a day is much longer", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = "3h"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(3 * time.Hour))
					})
				})
				Context("and a day is just longer", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "3h"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(3 * time.Hour))
					})
				})
			})

			Context("and the hours add up to an exact number of days", func() {
				Context("for a long day", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "2d"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(8 * time.Hour))
					})
				})
				Context("for just under a week", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "4d"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(16 * time.Hour))
					})
				})
			})

			Context("and the hours add up to over a day with a remainder", func() {
				Context("for a standard week", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "1d1h"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(5 * time.Hour))
					})
				})

				Context("for a shorter week", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "2d1h"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(9 * time.Hour))
					})
				})
			})

			Context("and the days are less than a week", func() {
				Context("and a week is much longer", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "3d"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(3 * workDay))
					})
				})
				Context("and a week is just longer", func() {
					BeforeEach(func() {
						workDay = 4 * time.Hour
						workWeek = 5 * workDay
						duration = "4d"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(4 * workDay))
					})
				})
			})

			Context("and the days add up to an exact number of weeks", func() {
				Context("for a long week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 7 * workDay
						duration = "2w"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(14 * workDay))
					})
				})
				Context("for a short week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 4 * workDay
						duration = "2w"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(2 * 4 * 8 * time.Hour))
					})
				})
			})

			Context("and the hours add up to over a week with a remainder", func() {
				Context("for a standard week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 5 * workDay
						duration = "1w3d"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(8 * workDay))
					})
				})
				Context("for a shorter week", func() {
					BeforeEach(func() {
						workDay = 8 * time.Hour
						workWeek = 3 * workDay

						duration = "2w2d"
					})

					It("should not have an init error", func() {
						Ω(initErr).ShouldNot(HaveOccurred())
					})

					It("should not have an error", func() {
						Ω(err).ShouldNot(HaveOccurred())
					})

					It("returns a duration", func() {
						Ω(actual).Should(Equal(8 * workDay))
					})
				})
			})
		})

		Context("when a actual contains only min", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = "48m"
			})

			It("should not have an init error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("should not have an error", func() {
				Ω(err).ShouldNot(HaveOccurred())
			})

			It("returns a duration", func() {
				Ω(actual).Should(Equal(48 * time.Minute))
			})
		})

		Context("when a actual contains only sec", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = "48s"
			})

			It("should not have an init error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("should not have an error", func() {
				Ω(err).ShouldNot(HaveOccurred())
			})

			It("returns a duration", func() {
				Ω(actual).Should(Equal(48 * time.Second))
			})
		})

		Context("when a actual contains only ms", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = "48ms"
			})

			It("should not have an init error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("should not have an error", func() {
				Ω(err).ShouldNot(HaveOccurred())
			})

			It("returns a duration", func() {
				Ω(actual).Should(Equal(48 * time.Millisecond))
			})
		})

		Context("when a actual contains only micro sec", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = "48µs"
			})

			It("should not have an init error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("should not have an error", func() {
				Ω(err).ShouldNot(HaveOccurred())
			})

			It("returns a duration", func() {
				Ω(actual).Should(Equal(48 * time.Microsecond))
			})
		})
		Context("when a actual contains only nano sec", func() {
			BeforeEach(func() {
				workDay = 8 * time.Hour
				workWeek = 5 * workDay
				duration = "48ns"
			})

			It("should not have an init error", func() {
				Ω(initErr).ShouldNot(HaveOccurred())
			})

			It("should not have an error", func() {
				Ω(err).ShouldNot(HaveOccurred())
			})

			It("returns a duration", func() {
				Ω(actual).Should(Equal(48 * time.Nanosecond))
			})
		})
	})
	Context("when week is greater than day", func() {
		BeforeEach(func() {
			workWeek = 8 * time.Hour
			workDay = 5 * workWeek
			duration = "48m"
		})

		It("should have an init error", func() {
			Ω(initErr).Should(MatchError("work day must be less than work week"))
		})
	})
})
var _ = Describe("workDuration#GetWorkDayWeek", func() {
	var workDuration mapper.WorkDuration
	var defaultedWorkingWeek time.Duration
	var defaultedWorkingDay time.Duration

	JustBeforeEach(func() {
		defaultedWorkingDay, defaultedWorkingWeek = workDuration.GetWorkDayWeek()
	})

	Context("No work day or work week set", func() {
		It("Returns 8 hours", func() {
			Ω(defaultedWorkingDay).Should(Equal(8 * time.Hour))
		})
		It("Returns 5 * 8 hours", func() {
			Ω(defaultedWorkingWeek).Should(Equal(5 * 8 * time.Hour))
		})
	})
	Context("Work day set and work week not", func() {
		BeforeEach(func() {
			workDuration, _ = mapper.NewWorkDuration(5*time.Hour, time.Duration(0))
		})

		It("Returns 8 hours", func() {
			Ω(defaultedWorkingDay).Should(Equal(5 * time.Hour))
		})
		It("Returns 5 * 5 hours", func() {
			Ω(defaultedWorkingWeek).Should(Equal(5 * 5 * time.Hour))
		})
	})
	Context("Work day not set and work week set", func() {
		BeforeEach(func() {
			workDuration, _ = mapper.NewWorkDuration(time.Duration(0), 4*8*time.Hour)
		})

		It("Returns 8 hours", func() {
			Ω(defaultedWorkingDay).Should(Equal(8 * time.Hour))
		})
		It("Returns 4 * 8 hours", func() {
			Ω(defaultedWorkingWeek).Should(Equal(4 * 8 * time.Hour))
		})
	})
	Context("Work day not set and work week set", func() {
		BeforeEach(func() {
			workDuration, _ = mapper.NewWorkDuration(6*time.Hour, 4*8*time.Hour)
		})

		It("Returns 6 hours", func() {
			Ω(defaultedWorkingDay).Should(Equal(6 * time.Hour))
		})
		It("Returns 4 * 8 hours", func() {
			Ω(defaultedWorkingWeek).Should(Equal(4 * 8 * time.Hour))
		})
	})

})
