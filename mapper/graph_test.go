package mapper

import (
	"fmt"
	"reflect"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/types"
)

var _ = Describe("Graph#IsValid", func() {
	Context("when all cells are invalid", func() {
		It("returns false", func() {
			Ω(Graph{
				Cells: []Cell{
					{
						Size: CellSize{
							Width: -1,
						},
					},
					{
						Size: CellSize{
							Height: -1,
						},
					},
				},
			}.IsValid()).Should(BeFalse())
		})
	})

	Context("when at least one cell is invalid", func() {
		It("returns false", func() {
			Ω(Graph{
				Cells: []Cell{
					{
						Size: CellSize{
							Width: -1,
						},
					},
					{
						Size: CellSize{
							Height: 1,
						},
					},
				},
			}.IsValid()).Should(BeFalse())
		})
	})

	Context("when all cells are valid", func() {
		It("returns false", func() {
			Ω(Graph{
				Cells: []Cell{
					{
						Size: CellSize{
							Width: 1,
						},
					},
					{
						Size: CellSize{
							Height: 1,
						},
					},
				},
			}.IsValid()).Should(BeTrue())
		})
	})
})

var _ = Describe("NewGraph", func() {
	Context("when everything works", func() {
		Context("and render process ids is enabled", func() {
			var valueStream = ValueStream{
				RenderProcessKeys: true,
				Resources: Resources{
					{
						Process: "process3",
						Type:    "process",
						Title:   "test",
						Metrics: &Metrics{
							LeadTime:    "2m",
							ProcessTime: "2m",
						},
						PreviousProcess: "process2",
					},
					{
						Process: "process2",
						Title:   "test",
						Kaizen:  "An example action",
						Metrics: &Metrics{
							LeadTime:    "2m",
							ProcessTime: "2m",
						},
						Flow: &Flow{
							In: []string{"flow1", "flow3", "flow5"},
						},
						AutomatedFlow: &Flow{
							Out: []string{"flow2", "flow4"},
						},
						PreviousProcess: "process1",
					},
					{
						Process: "process1",
						Type:    "process",
						Title:   "test",
						Metrics: &Metrics{
							LeadTime:    "2m",
							ProcessTime: "2m",
						},
					},
				},
			}

			It("creates a graph object", func() {
				valueStream.CalculateValueStream()

				graphObj, err := NewGraph(valueStream)
				Ω(graphObj.Cells).Should(HaveUniqueIds())

				Ω(graphObj.Cells).Should(HaveCellIDAt("process1", 10, 290))
				Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-process1", 10, 366))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process1", "process2", "uml.DashedTransition"))

				Ω(graphObj.Cells).Should(HaveCellIDAt("process2", 272, 290))
				Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-process2", 272, 366))
				Ω(graphObj.Cells).Should(HaveCellIDAt("kaizen-process2", 197, 240))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process2", "process3", "uml.DashedTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("flow1", "process2", "uml.SimpleTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("flow3", "process2", "uml.SimpleTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("flow5", "process2", "uml.SimpleTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process2", "flow2", "uml.SimpleAutomatedTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process2", "flow4", "uml.SimpleAutomatedTransition"))

				Ω(graphObj.Cells).Should(HaveCellIDAt("process3", 534, 290))
				Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-process3", 534, 366))

				Ω(graphObj.Cells).Should(HaveCellIDAt("flow1", -90, 160))
				Ω(graphObj.Cells).Should(HaveCellIDAt("flow2", 100, 160))
				Ω(graphObj.Cells).Should(HaveCellIDAt("flow3", 290, 160))
				Ω(graphObj.Cells).Should(HaveCellIDAt("flow4", 480, 160))
				Ω(graphObj.Cells).Should(HaveCellIDAt("flow5", 670, 160))

				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process3", "source", "uml.SimpleTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("source", "process1", "uml.SimpleTransition"))

				Ω(graphObj.Cells).Should(HaveCellIDAt("source", 275, 10))
				Ω(graphObj.Cells).Should(HaveCellTypeAt("uml.Summary", 761, 590))

				Ω(graphObj.Cells).Should(HaveLen(23))
				Ω(err).Should(BeNil())
			})
		})

		Context("and render process ids is disabled", func() {
			var valueStream = ValueStream{
				Resources: Resources{
					{
						Process: "process3",
						Type:    "process",
						Title:   "test",
						Metrics: &Metrics{
							LeadTime:    "2m",
							ProcessTime: "2m",
						},
						PreviousProcess: "process2",
					},
					{
						Process: "process2",
						Title:   "test",
						Kaizen:  "An example action",
						Metrics: &Metrics{
							LeadTime:    "2m",
							ProcessTime: "2m",
						},
						Flow: &Flow{
							In:  []string{"flow1"},
							Out: []string{"flow2"},
						},
						PreviousProcess: "process1",
					},
					{
						Process:       "process1",
						Type:          "process",
						Title:         "test",
						NumberOfStaff: 2,
						Metrics: &Metrics{
							LeadTime:    "2m",
							ProcessTime: "2m",
						},
					},
				},
			}

			It("creates a graph object", func() {
				valueStream.CalculateValueStream()

				graphObj, err := NewGraph(valueStream)
				Ω(graphObj.Cells).Should(HaveUniqueIds())

				Ω(graphObj.Cells).Should(HaveCellIDAt("process1", 10, 290))
				Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-process1", 10, 366))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process1", "process2", "uml.DashedTransition"))

				Ω(graphObj.Cells).Should(HaveCellIDAt("process2", 272, 290))
				Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-process2", 272, 366))
				Ω(graphObj.Cells).Should(HaveCellIDAt("kaizen-process2", 197, 240))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process2", "process3", "uml.DashedTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("flow1", "process2", "uml.SimpleTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process2", "flow2", "uml.SimpleTransition"))

				Ω(graphObj.Cells).Should(HaveCellIDAt("process3", 534, 290))
				Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-process3", 534, 366))

				Ω(graphObj.Cells).Should(HaveCellIDAt("flow1", 195, 160))
				Ω(graphObj.Cells).Should(HaveCellIDAt("flow2", 385, 160))

				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("process3", "source", "uml.SimpleTransition"))
				Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("source", "process1", "uml.SimpleTransition"))

				Ω(graphObj.Cells).Should(HaveCellIDAt("source", 275, 10))
				Ω(graphObj.Cells).Should(HaveCellTypeAt("uml.Summary", 761, 590))

				Ω(graphObj.Cells).Should(HaveLen(17))
				Ω(err).Should(BeNil())
			})
		})
	})

	Context("when there are parallel relations", func() {
		var (
			valueStream ValueStream
		)
		BeforeEach(func() {
			valueStream = ValueStream{
				Resources: Resources{
					{
						Process:     "1",
						Type:        "process",
						Title:       "test1",
						Description: "test",
					},
					{
						Process:         "2-1",
						Type:            "process",
						Title:           "test1",
						Description:     "test",
						PreviousProcess: "1",
					},
					{
						Process:         "2-2",
						Type:            "process",
						Title:           "test1",
						Description:     "test",
						PreviousProcess: "1",
					},
					{
						Process:           "3",
						Type:              "process",
						Title:             "test1",
						Description:       "test",
						PreviousProcesses: []*Edge{{Process: "2-1"}, {Process: "2-2"}},
					},
				},
			}

			valueStream.CalculateValueStream()
		})

		It("generates a canvas with parallel tracks", func() {
			graphObj, err := NewGraph(valueStream)
			Ω(graphObj.Cells).Should(HaveUniqueIds())

			Ω(graphObj.Cells).Should(HaveCellIDAt("1", 10, 180))
			Ω(graphObj.Cells).Should(Not(HaveCellID("ladder-1")))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("1", "2-1", "uml.DashedTransition"))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("1", "2-2", "uml.DashedTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("2-1", 272, 180))
			Ω(graphObj.Cells).Should(Not(HaveCellID("ladder-2-1")))

			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("2-1", "3", "uml.DashedTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("2-2", 272, 430))
			Ω(graphObj.Cells).Should(Not(HaveCellID("ladder-2-2")))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("2-2", "3", "uml.DashedTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("3", 534, 180))
			Ω(graphObj.Cells).Should(Not(HaveCellID("ladder-3")))

			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("source", "1", "uml.SimpleTransition"))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("3", "source", "uml.SimpleTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("source", 275, 10))
			Ω(graphObj.Cells).Should(HaveCellTypeAt("uml.Summary", 761, 730))

			Ω(graphObj.Cells).Should(HaveLen(12))
			Ω(err).Should(BeNil())
		})
	})
	Context("when there are parallel relations with metrics", func() {
		var (
			valueStream ValueStream
		)
		BeforeEach(func() {
			valueStream = ValueStream{
				Resources: Resources{
					{
						Process:     "1",
						Type:        "process",
						Title:       "test1",
						Description: "test",
						Metrics: &Metrics{
							LeadTime:      "2h",
							ProcessTime:   "1h",
							ActivityRatio: "90%",
							CARatio:       "100%",
						},
					},
					{
						Process:         "2-1",
						Type:            "process",
						Title:           "test1",
						Description:     "test",
						PreviousProcess: "1",
						Metrics: &Metrics{
							LeadTime:      "2h",
							ProcessTime:   "1h",
							ActivityRatio: "90%",
							CARatio:       "100%",
						},
					},
					{
						Process:         "2-2",
						Type:            "process",
						Title:           "test1",
						Description:     "test",
						PreviousProcess: "1",
						Metrics: &Metrics{
							LeadTime:      "1h",
							ProcessTime:   "30m",
							ActivityRatio: "90%",
							CARatio:       "100%",
						},
					},
					{
						Process:           "3",
						Type:              "process",
						Title:             "test1",
						Description:       "test",
						PreviousProcesses: []*Edge{{Process: "2-1"}, {Process: "2-2"}},
						Metrics: &Metrics{
							LeadTime:      "2h",
							ProcessTime:   "1h",
							ActivityRatio: "90%",
							CARatio:       "100%",
						},
					},
				},
			}

			valueStream.CalculateValueStream()
		})

		It("generates a canvas with parallel tracks", func() {
			graphObj, err := NewGraph(valueStream)
			Ω(graphObj.Cells).Should(HaveUniqueIds())

			Ω(graphObj.Cells).Should(HaveCellIDAt("1", 10, 180))
			Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-1", 10, 566))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("1", "2-1", "uml.DashedTransition"))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("1", "2-2", "uml.DashedTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("2-1", 272, 180))
			Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-2-1", 272, 566))

			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("2-1", "3", "uml.DashedTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("2-2", 272, 490))
			Ω(graphObj.Cells).Should(Not(HaveCellID("ladder-2-2")))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("2-2", "3", "uml.DashedTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("3", 534, 180))
			Ω(graphObj.Cells).Should(HaveCellIDAt("ladder-3", 534, 566))

			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("source", "1", "uml.SimpleTransition"))
			Ω(graphObj.Cells).Should(HaveRelationshipBetweenWithType("3", "source", "uml.SimpleTransition"))

			Ω(graphObj.Cells).Should(HaveCellIDAt("source", 275, 10))
			Ω(graphObj.Cells).Should(HaveCellTypeAt("uml.Summary", 761, 790))

			Ω(graphObj.Cells).Should(HaveLen(15))
			Ω(err).Should(BeNil())
		})
	})
})

var _ = Describe("#NewCompareGraph", func() {
	var source ValueStream
	var input ValueStream
	var output Graph
	var err error

	JustBeforeEach(func() {
		source.CalculateValueStream()
		input.CalculateValueStream()
		output, err = NewCompareGraph(source, input)
		Ω(err).To(BeNil())
	})

	Context("Two simple graphs", func() {
		BeforeEach(func() {
			source = ValueStream{
				Resources: Resources{
					{
						Process: "example-1",
						Type:    "process",
					},
					{
						Process:         "example-2",
						Type:            "process",
						PreviousProcess: "example-1",
					},
					{
						Process:         "example-3",
						Type:            "process",
						PreviousProcess: "example-2",
					},
				},
			}
			input = ValueStream{
				Resources: Resources{
					{
						Process: "example-4",
						Type:    "process",
					},
					{
						Process:         "example-5",
						Type:            "process",
						PreviousProcess: "example-4",
					},
				},
			}
		})

		It("has all unique ids", func() {
			Ω(output.Cells).Should(HaveUniqueIds())
		})

		It("has graph 1 + graph 2's cells", func() {
			Ω(output.Cells).Should(HaveLen(17))
		})

		It("offsets graph 2's cells while it maintains graph 1's cells", func() {
			Ω(output.Cells).Should(HaveCellIDAt("source-example-1", 10, 180))
			Ω(output.Cells).Should(HaveCellIDAt("source-example-2", 272, 180))
			Ω(output.Cells).Should(HaveCellIDAt("source-example-3", 534, 180))
			Ω(output.Cells).Should(HaveCellIDAt("comparison-example-4", 10, 840))
			Ω(output.Cells).Should(HaveCellIDAt("comparison-example-5", 272, 840))
		})
		It("contains diff information", func() {
			Ω(output.Cells).Should(HaveCellTypeAt("uml.DiffInformation", 0, 620))
		})
		It("preserves relationships", func() {
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("source-example-1", "source-example-2", "uml.DashedTransition"))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("source-example-2", "source-example-3", "uml.DashedTransition"))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("comparison-example-4", "comparison-example-5", "uml.DashedTransition"))
		})
	})

	Context("Two graphs with information flows", func() {
		BeforeEach(func() {
			source = ValueStream{
				Resources: Resources{
					{
						Process: "example-1",
						Type:    "process",
					},
					{
						Process:         "example-2",
						Type:            "process",
						PreviousProcess: "example-1",
						Flow: &Flow{
							Out: []string{"example"},
						},
					},
					{
						Process:         "example-3",
						Type:            "process",
						PreviousProcess: "example-2",
						Flow: &Flow{
							In: []string{"example"},
						},
					},
				},
			}
			input = ValueStream{
				Resources: Resources{
					{
						Process: "example-4",
						Type:    "process",
						Flow: &Flow{
							Out: []string{"example"},
						},
					},
					{
						Process:         "example-5",
						Type:            "process",
						PreviousProcess: "example-4",
						Flow: &Flow{
							In: []string{"example"},
						},
					},
				},
			}
		})

		It("has all unique ids", func() {
			Ω(output.Cells).Should(HaveUniqueIds())
		})
		It("has graph 1 + graph 2's cells", func() {
			Ω(output.Cells).Should(HaveLen(23))
		})
		It("contains diff information", func() {
			Ω(output.Cells).Should(HaveCellTypeAt("uml.DiffInformation", 0, 730))
		})
		It("offsets graph 2's cells while it maintains graph 1's cells", func() {
			Ω(output.Cells).Should(HaveCellIDAt("source-example-1", 10, 290))
			Ω(output.Cells).Should(HaveCellIDAt("source-example-2", 272, 290))
			Ω(output.Cells).Should(HaveCellIDAt("source-example-3", 534, 290))
			Ω(output.Cells).Should(HaveCellIDAt("comparison-example-4", 10, 1060))
			Ω(output.Cells).Should(HaveCellIDAt("comparison-example-5", 272, 1060))
		})
		It("preserves relationships", func() {
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("source-example-2", "source-example", "uml.SimpleTransition"))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("source-example", "source-example-3", "uml.SimpleTransition"))

			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("source-example-1", "source-example-2", "uml.DashedTransition"))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("source-example-2", "source-example-3", "uml.DashedTransition"))

			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("comparison-example-4", "comparison-example", "uml.SimpleTransition"))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("comparison-example", "comparison-example-5", "uml.SimpleTransition"))

			Ω(output.Cells).Should(HaveRelationshipBetweenWithType("comparison-example-4", "comparison-example-5", "uml.DashedTransition"))

		})
		It("corrects Vertexes", func() {
			Ω(output.Cells).Should(HaveRelationshipBetweenWithVertex("source-example-2", "source-example", Vertex{X: 363, Y: 240}))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithVertex("source-example", "source-example-3", Vertex{X: 494, Y: 240}))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithVertex("comparison-example-4", "comparison-example", Vertex{X: 167, Y: 1010}))
			Ω(output.Cells).Should(HaveRelationshipBetweenWithVertex("comparison-example", "comparison-example-5", Vertex{X: 298, Y: 1010}))
		})
	})
})

func HaveCellIDAt(ID string, x int, y int) types.GomegaMatcher {
	return &representHaveCellIDAtMatcher{
		expectedX:  x,
		expectedY:  y,
		expectedID: ID,
	}
}

type representHaveCellIDAtMatcher struct {
	expectedX  int
	expectedY  int
	expectedID string
}

func (matcher *representHaveCellIDAtMatcher) Match(actual interface{}) (success bool, err error) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return false, fmt.Errorf("HaveCellIDAt matcher expects an mapper.Cells")
	}

	for _, cell := range actualCells {
		if cell.ID == matcher.expectedID {
			return reflect.DeepEqual(cell.Position, CellPosition{X: matcher.expectedX, Y: matcher.expectedY}), nil
		}
	}
	return false, nil
}

func (matcher *representHaveCellIDAtMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell %#v", actual, matcher)
}

func (matcher *representHaveCellIDAtMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to not contain the Cell %#v", actual, matcher)
}

func HaveCellID(ID string) types.GomegaMatcher {
	return &representHaveCellIDMatcher{
		expectedID: ID,
	}
}

type representHaveCellIDMatcher struct {
	expectedID string
}

func (matcher *representHaveCellIDMatcher) Match(actual interface{}) (success bool, err error) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return false, fmt.Errorf("HaveCellID matcher expects an mapper.Cells")
	}

	for _, cell := range actualCells {
		if cell.ID == matcher.expectedID {
			return true, nil
		}
	}
	return false, nil
}

func (matcher *representHaveCellIDMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell %#v", actual, matcher)
}

func (matcher *representHaveCellIDMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to not contain the Cell %#v", actual, matcher)
}

func HaveCellTypeAt(expectedType string, x int, y int) types.GomegaMatcher {
	return &representHaveCellTypeAtMatcher{
		expectedX:    x,
		expectedY:    y,
		expectedType: expectedType,
	}
}

type representHaveCellTypeAtMatcher struct {
	expectedX    int
	expectedY    int
	expectedType string
}

func (matcher *representHaveCellTypeAtMatcher) Match(actual interface{}) (success bool, err error) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return false, fmt.Errorf("HaveCellTypeAt matcher expects an mapper.Cells")
	}

	for _, cell := range actualCells {
		if cell.Type == matcher.expectedType {
			return reflect.DeepEqual(cell.Position, CellPosition{X: matcher.expectedX, Y: matcher.expectedY}), nil
		}
	}
	return false, nil
}

func (matcher *representHaveCellTypeAtMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell %#v", actual, matcher)
}

func (matcher *representHaveCellTypeAtMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to not contain the Cell %#v", actual, matcher)
}

func HaveRelationshipBetweenWithType(source string, target string, nodeType string) types.GomegaMatcher {
	return &representHaveRelationshipBetweenWithTypeMatcher{
		source:   source,
		target:   target,
		nodeType: nodeType,
	}
}

type representHaveRelationshipBetweenWithTypeMatcher struct {
	source   string
	target   string
	nodeType string
}

func (matcher *representHaveRelationshipBetweenWithTypeMatcher) Match(actual interface{}) (success bool, err error) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return false, fmt.Errorf("HaveRelationshipBetweenWithType matcher expects an mapper.Cells")
	}

	for _, cell := range actualCells {
		if cell.Source.ID == matcher.source && cell.Target.ID == matcher.target && cell.Type == matcher.nodeType {
			return true, nil
		}
	}

	return false, nil
}

func (matcher *representHaveRelationshipBetweenWithTypeMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell the relationship %#v", actual, matcher)
}

func (matcher *representHaveRelationshipBetweenWithTypeMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to not contain the Cell the relationship %#v", actual, matcher)
}

func HaveRelationshipBetweenWithVertex(source string, target string, vertex Vertex) types.GomegaMatcher {
	return &representHaveRelationshipBetweenWithVertexMatcher{
		source: source,
		target: target,
		vertex: vertex,
	}
}

type representHaveRelationshipBetweenWithVertexMatcher struct {
	source string
	target string
	vertex Vertex
}

func (matcher *representHaveRelationshipBetweenWithVertexMatcher) Match(actual interface{}) (success bool, err error) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return false, fmt.Errorf("HaveRelationshipBetweenWithVertex matcher expects an mapper.Cells")
	}

	for _, cell := range actualCells {
		if cell.Source.ID == matcher.source && cell.Target.ID == matcher.target {
			for _, vertex := range cell.Vertices {
				if vertex == matcher.vertex {
					return true, nil
				}
			}
		}
	}

	return false, nil
}

func (matcher *representHaveRelationshipBetweenWithVertexMatcher) FailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell the relationship %#v", actual, matcher)
}

func (matcher *representHaveRelationshipBetweenWithVertexMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return fmt.Sprintf("Expected\n\t%#v\n to not contain the Cell the relationship %#v", actual, matcher)
}

func HaveUniqueIds() types.GomegaMatcher {
	return &representHaveUniqueIdsMatcher{}
}

type representHaveUniqueIdsMatcher struct {
}

func (matcher *representHaveUniqueIdsMatcher) Match(actual interface{}) (success bool, err error) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return false, fmt.Errorf("HaveUniqueIds matcher expects an mapper.Cells")
	}

	var ids map[string]bool
	ids = make(map[string]bool)

	for _, cell := range actualCells {
		if cell.ID == "" {
			continue
		}

		if _, ok := ids[cell.ID]; ok {
			return false, nil
		}

		ids[cell.ID] = true
	}

	return true, nil
}

func (matcher *representHaveUniqueIdsMatcher) FailureMessage(actual interface{}) (message string) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return fmt.Sprintf("HaveUniqueIds matcher expects an mapper.Cells")
	}

	var ids []string

	for _, cell := range actualCells {
		if cell.ID == "" {
			continue
		}

		ids = append(ids, cell.ID)
	}

	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell ids to be unique", ids)
}

func (matcher *representHaveUniqueIdsMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	actualCells, ok := actual.(Cells)
	if !ok {
		return fmt.Sprintf("HaveUniqueIds matcher expects an mapper.Cells")
	}

	var ids []string

	for _, cell := range actualCells {
		if cell.ID == "" {
			continue
		}

		ids = append(ids, cell.ID)
	}

	return fmt.Sprintf("Expected\n\t%#v\n to contain the Cell ids to contain at least 1 duplicate", ids)
}
