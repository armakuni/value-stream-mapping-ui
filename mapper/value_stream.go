package mapper

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/blang/semver"
)

// ValueStream a struct representation of a value stream map
type ValueStream struct {
	Title             string         `json:"title"`
	Customer          string         `json:"customer"`
	Version           string         `json:"version,omitempty"`
	WorkWeekString    string         `json:"working_week,omitempty"`
	WorkDayString     string         `json:"working_day,omitempty"`
	RenderProcessKeys bool           `json:"render_process_keys,omitempty"`
	Resources         Resources      `json:"resources"`
	WorkDuration      WorkDuration   `json:"-"`
	MetricsSummary    MetricsSummary `json:"-"`
}

// MetricsSummary is a summary of the statistical information associated with the whole value stream
type MetricsSummary struct {
	LeadTime      string `json:"leadtime"`
	ProcessTime   string `json:"processtime"`
	ActivityRatio string `json:"activityratio"`
	CARatio       string `json:"c&aratio"`
}

type valueStreamValidatorErr struct {
	WorkDuration string                 `json:"work-duration-validation-error,omitempty"`
	Resources    *resourcesValidatorErr `json:"resource-validation-errors,omitempty"`
	Version      string                 `json:"version-validation-error,omitempty"`
}

// Error Marshal error to string
func (v valueStreamValidatorErr) Error() string {
	return basicMarshal(v)
}

// ValidateVersion - validate that version is a semver
func ValidateVersion(version string) error {
	if _, err := semver.Parse(version); err != nil {
		return fmt.Errorf("version must be semver compliant, was: %s", version)
	}
	return nil
}

// Validate - validates a ValueStream
func (valueStream *ValueStream) Validate() error {
	var valueStreamValidation valueStreamValidatorErr

	_, err := NewWorkDurationFromDurationStrings(valueStream.WorkDayString, valueStream.WorkWeekString)
	if err != nil {
		valueStreamValidation.WorkDuration = err.Error()
	}

	err = valueStream.Resources.Validate()
	if err != nil {
		resourcesErr := err.(resourcesValidatorErr)
		valueStreamValidation.Resources = &resourcesErr
	}

	if valueStream.Version != "" {
		if err := ValidateVersion(valueStream.Version); err != nil {
			valueStreamValidation.Version = err.Error()
		}
	}

	if valueStreamValidation != (valueStreamValidatorErr{}) {
		return valueStreamValidation
	}

	return nil
}

// CalculateSummaryMetrics calculates summary metrics
func (valueStream *ValueStream) CalculateSummaryMetrics() error {
	workDuration, err := NewWorkDurationFromDurationStrings(valueStream.WorkDayString, valueStream.WorkWeekString)

	resources := valueStream.Resources
	totalLeadTime := resources.calcTotalLeadTime()
	totalProcessTime := resources.calcTotalProcessTime()

	summaryCARatio, err := resources.calcCARatioSummary()
	if err != nil {
		return err
	}

	summaryActivityRatio, err := calcActivityRatioSummary(totalLeadTime, totalProcessTime)
	if err != nil {
		return err
	}

	valueStream.MetricsSummary = MetricsSummary{
		LeadTime:      workDuration.DurationToString(totalLeadTime),
		ProcessTime:   workDuration.DurationToString(totalProcessTime),
		CARatio:       summaryCARatio,
		ActivityRatio: summaryActivityRatio,
	}

	return nil
}

// CalculateValueStream - convert value stream struct to JointJS compatible JSON
func (valueStream *ValueStream) CalculateValueStream() error {
	workDuration, err := NewWorkDurationFromDurationStrings(valueStream.WorkDayString, valueStream.WorkWeekString)

	if err != nil {
		return err
	}

	valueStream.Resources.CalculateEdges()
	err = valueStream.Resources.CalculateDurations(workDuration)

	if err != nil {
		return err
	}

	valueStream.Resources.CalculateLongestPath()
	valueStream.Resources.CalculateLeadTimeWeights()
	return valueStream.CalculateSummaryMetrics()
}

func calcActivityRatioSummary(totalLeadTimeDuration, totalProcessTimeDuration time.Duration) (string, error) {
	metricsTotal := &Metrics{
		LeadTimeDuration:    totalLeadTimeDuration,
		ProcessTimeDuration: totalProcessTimeDuration,
	}

	err := metricsTotal.Calculate()
	if err != nil {
		return "", err
	}

	return metricsTotal.ActivityRatio, nil
}

func basicMarshal(obj interface{}) string {
	str, _ := json.Marshal(obj)
	return string(str)
}
