package mapper

import (
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

// WorkDuration is the period of time that a person is at work
type WorkDuration struct {
	WorkDay  time.Duration
	WorkWeek time.Duration
}

// NewWorkDuration constructs a WorkDuration from durations
func NewWorkDuration(workDay time.Duration, workWeek time.Duration) (WorkDuration, error) {
	var err error

	workDuration := WorkDuration{
		WorkDay:  workDay,
		WorkWeek: workWeek,
	}

	defaultedWorkDay, defaultedWorkWeek := workDuration.GetWorkDayWeek()

	if defaultedWorkDay > defaultedWorkWeek {
		err = fmt.Errorf("work day must be less than work week")
	}

	return workDuration, err
}

// NewWorkDurationFromDurationStrings constructs a WorkDuration from strings
func NewWorkDurationFromDurationStrings(workDay string, workWeek string) (WorkDuration, error) {
	var (
		workDayDuration time.Duration
		err             error
	)
	if workDay != "" {
		workDayDuration, err = time.ParseDuration(workDay)

		if err != nil {
			return WorkDuration{}, errors.Errorf("work day %s does not follow format 10h3m4s", workDay)
		}
	}

	tmpWorkDuration, _ := NewWorkDuration(workDayDuration, workDayDuration*5)
	workWeekDuration, err := tmpWorkDuration.StringToDuration(workWeek)

	if err != nil {
		return WorkDuration{}, errors.Errorf("work week %s does not follow format 3d10h3m4s", workWeek)
	}

	return NewWorkDuration(workDayDuration, workWeekDuration)
}

// DurationToString - convert a duration to string, including days and stripping any trailing 0 durations
func (w WorkDuration) DurationToString(duration time.Duration) string {
	if duration == time.Duration(0) {
		return "0s"
	}

	workDay, workWeek := w.GetWorkDayWeek()

	prefix := ""
	var stringDuration string

	switch {
	case duration >= workWeek:
		weeks := int64(duration / workWeek)
		duration = time.Duration(duration % workWeek)

		if weeks > 0 {
			prefix = fmt.Sprintf("%s%dw", prefix, weeks)
		}

		fallthrough
	case duration >= workDay:
		days := int64(duration / workDay)
		duration = time.Duration(duration % workDay)

		if days > 0 {
			prefix = fmt.Sprintf("%s%dd", prefix, days)
		}

		fallthrough
	default:
		stringDuration = fmt.Sprintf("%s%s", prefix, duration.String())
	}

	re := regexp.MustCompile("[0-9]+[^0-9]+")
	fragments := re.FindAllString(stringDuration, -1)
	stringDuration = ""

	for _, fragment := range fragments {
		switch {
		case len(fragment) > 2:
			stringDuration += fragment
		case string([]rune(fragment)[0]) != "0":
			stringDuration += fragment
		}
	}

	return stringDuration
}

// GetWorkDayWeek returns the default durations for working week
func (w WorkDuration) GetWorkDayWeek() (time.Duration, time.Duration) {
	workDay := w.WorkDay
	if w.WorkDay == time.Duration(0) {
		workDay = 8 * time.Hour
	}
	workWeek := w.WorkWeek
	if w.WorkWeek == time.Duration(0) {
		workWeek = workDay * 5
	}
	return workDay, workWeek
}

// StringToDuration - convert a duration to string, with additional support for working weeks and days
func (w WorkDuration) StringToDuration(duration string) (time.Duration, error) {
	workDay, workWeek := w.GetWorkDayWeek()

	realDurationString := duration
	weekMatchRegex := regexp.MustCompile("^([0-9]+w)")
	weeksDuration := time.Duration(0)

	if weekMatchRegex.MatchString(realDurationString) {
		weeksDurationString := weekMatchRegex.FindString(realDurationString)
		additionalWeeks, _ := strconv.ParseInt(weeksDurationString[:len(weeksDurationString)-1], 10, 64)
		weeksDuration = time.Duration(additionalWeeks) * workWeek

		realDurationString = weekMatchRegex.ReplaceAllString(realDurationString, "")
	}

	dayMatchRegex := regexp.MustCompile("^([0-9]+d)")
	daysDuration := time.Duration(0)

	if dayMatchRegex.MatchString(realDurationString) {
		daysDurationString := dayMatchRegex.FindString(realDurationString)
		additionalDays, _ := strconv.ParseInt(daysDurationString[:len(daysDurationString)-1], 10, 64)
		daysDuration = time.Duration(additionalDays) * workDay

		realDurationString = dayMatchRegex.ReplaceAllString(realDurationString, "")
	}

	parsedDuration := time.Duration(0)

	if realDurationString != "" {
		var err error
		if parsedDuration, err = time.ParseDuration(realDurationString); err != nil {
			return time.Duration(0), err
		}
	}

	parsedDuration += weeksDuration
	parsedDuration += daysDuration

	return parsedDuration, nil
}
