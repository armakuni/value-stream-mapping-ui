package mapper

// Labels a collection of Label
type Labels []Label

// Label a struct representation of a JointJS label
type Label struct {
	Attrs    map[string]map[string]string `json:"attrs,omitempty"`
	Position LabelPosition                `json:"position"`
}

// LabelPosition a struct representation of a JointJS labels position
type LabelPosition struct {
	Distance float64     `json:"distance,omitempty"`
	Offset   LabelOffset `json:"offset,omitempty"`
}

// LabelOffset a struct for JoinJS label offsets
type LabelOffset struct {
	X int `json:"x"`
	Y int `json:"y"`
}

// IsValid - checks the validity of fields on the Label struct
func (label Label) IsValid() bool {
	if label.Position.Distance >= 0 && label.Position.Distance <= 1 {
		return true
	}
	return false
}

// AreValid - checks the validity of Labels
func (labels Labels) AreValid() bool {
	for _, label := range labels {
		if !label.IsValid() {
			return false
		}
	}
	return true
}
