package mapper

// Graph a struct representation of a JointJS graph
type Graph struct {
	Cells Cells `json:"cells"`
}

// IsValid validate a Graph struct
func (graph Graph) IsValid() bool {
	return graph.Cells.AreValid()
}

// NewGraph - Render a new graph and cells from a value stream
func NewGraph(valueStream ValueStream) (Graph, error) {
	flowResources := valueStream.Resources.BuildFlowResources()

	// Begin rendering SVG
	const startX = 10
	const startY = 10
	const width = 180
	var height = 84 + 84

	if valueStream.Resources.HasMetrics() {
		height += 20 + 20 + 20
	}

	const margin = 82
	xProcessSize := width + margin
	yProcessSize := height + margin

	var (
		cells    Cells
		currentX int
		y        int
	)

	currentX = startX
	y = startY

	resourceY := y + 170
	if valueStream.Resources.HaveFlows() {
		resourceY += 110
	}

	currentY := resourceY

	var currentNodes []*Resource
	var nextNodes []*Resource

	var seenIds map[string]bool
	seenIds = make(map[string]bool)

	currentNodes = append(currentNodes, valueStream.Resources.Start())
	highestResourceY := 0

	for len(currentNodes) > 0 {
		resource := currentNodes[0]

		var outResources []Edge
		outResources, seenIds = resource.OutSorted(seenIds)

		for _, nextResource := range outResources {
			nextNodes = append(nextNodes, nextResource.Resource)
		}

		cell, err := NewProcessToCell(*resource, currentX, currentY, valueStream.RenderProcessKeys)
		if err != nil {
			return Graph{}, err
		}

		cells = append(cells, cell)
		cells = append(cells, NewLadderCells(valueStream.Resources.HasMetrics(), resource, currentX, currentY)...)

		if resource.Kaizen != "" {
			kaizenCell := NewKaizenCell(*resource, currentX-75, currentY-50)
			cells = append(cells, kaizenCell)
		}

		if len(currentNodes) > 1 {
			currentNodes = currentNodes[1:]

			currentY = currentY + yProcessSize
		} else {
			currentNodes = nextNodes
			nextNodes = []*Resource{}

			currentX = currentX + xProcessSize
			currentY = resourceY
		}

		if currentY > highestResourceY {
			highestResourceY = currentY
		}
	}

	cells.UpdateYPosition("uml.Ladder", highestResourceY+18)

	cells = append(cells, NewRelationCells(valueStream.Resources)...)

	center := ((currentX - (xProcessSize / 2)) / 2) - (startX / 2)

	cells = append(cells, NewStartCells(valueStream, center-((startX+94)/2), y, cells, width)...)
	cells = append(cells, NewFlowCells(flowResources, center, y+150, cells, width)...)

	// Ignore error as calculate NewSummaryCell cannot fail if calculate metrics did not
	summaryCell, _ := NewSummaryCell(valueStream, currentX-xProcessSize, highestResourceY+300)
	cells = append(cells, summaryCell)

	return Graph{Cells: cells}, nil
}

// merge this graph with another graph to compare them
func (graph Graph) merge(additional Graph, zeroPoint CellPosition) Graph {
	sourceCells := graph.Cells.prefixIDs("source-")
	comparisonCells := additional.Cells.prefixIDs("comparison-")
	comparisonCells = comparisonCells.offset(zeroPoint)

	newGraph := Graph{Cells: append(sourceCells, comparisonCells...)}

	return newGraph
}

// NewCompareGraph that compares two VSMs
func NewCompareGraph(sourceMap ValueStream, comparisonMap ValueStream) (Graph, error) {
	source, err := NewGraph(sourceMap)

	if err != nil {
		return Graph{}, err
	}

	comparison, err := NewGraph(comparisonMap)

	if err != nil {
		return Graph{}, err
	}

	merged := source.merge(comparison, CellPosition{Y: source.getLowestCellPosition() + 180, X: 0})

	diffInfo := NewDiffInformationCell(sourceMap, comparisonMap, 0, source.getLowestCellPosition()+140)
	merged.Cells = append(merged.Cells, diffInfo)

	return merged, nil
}

// getLowestCellPosition that this graph has
func (graph Graph) getLowestCellPosition() int {
	highestCell := 0

	for _, cell := range graph.Cells {
		if highestCell < cell.Position.Y {
			highestCell = cell.Position.Y
		}
	}

	return highestCell
}
