package mapper_test

import (
	"time"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("ValueStream#CalculateValueStream", func() {
	var valueStream mapper.ValueStream
	var err error
	var graphErr error
	var graphObj mapper.Graph

	JustBeforeEach(func() {
		err = valueStream.CalculateValueStream()
		graphObj, graphErr = mapper.NewGraph(valueStream)
	})

	Context("when calculating activity ratio fails", func() {
		Context("because lead time is invalid", func() {
			BeforeEach(func() {
				valueStream = mapper.ValueStream{
					Resources: mapper.Resources{
						{
							Process: "test",
							Metrics: &mapper.Metrics{
								LeadTime: "notADuration",
							},
						},
					},
				}
			})

			It("returns an error", func() {
				Ω(err).Should(MatchError("time: invalid duration notADuration"))
			})
		})

		Context("because process time is greater than lead time", func() {
			BeforeEach(func() {
				valueStream = mapper.ValueStream{
					Resources: mapper.Resources{
						{
							Process: "test",
							Metrics: &mapper.Metrics{
								LeadTime:    "1h",
								ProcessTime: "2h",
							},
							Flow: &mapper.Flow{
								Out: []string{"flow2"},
							},
						},
						{
							Process:         "test2",
							PreviousProcess: "test",
							Metrics: &mapper.Metrics{
								LeadTime:    "1h",
								ProcessTime: "2h",
							},
						},
					},
				}
			})

			It("returns an error", func() {
				Ω(err).ShouldNot(BeNil())
				Ω(err).Should(MatchError("lead time cannot be less than process time"))
			})
		})
	})

	Context("when everything works", func() {
		Context("and render process ids is enabled", func() {
			BeforeEach(func() {
				valueStream = mapper.ValueStream{
					RenderProcessKeys: true,
					Resources: mapper.Resources{
						{
							Process: "process3",
							Type:    "process",
							Title:   "test",
							Metrics: &mapper.Metrics{
								LeadTime:    "2m",
								ProcessTime: "2m",
							},
							PreviousProcess: "process2",
						},
						{
							Process: "process2",
							Title:   "test",
							Kaizen:  "An example action",
							Metrics: &mapper.Metrics{
								LeadTime:    "2m",
								ProcessTime: "2m",
							},
							Flow: &mapper.Flow{
								In: []string{"flow1", "flow3", "flow5"},
							},
							AutomatedFlow: &mapper.Flow{
								Out: []string{"flow2", "flow4"},
							},
							PreviousProcess: "process1",
						},
						{
							Process: "process1",
							Type:    "process",
							Title:   "test",
							Metrics: &mapper.Metrics{
								LeadTime:    "2m",
								ProcessTime: "2m",
							},
						},
					},
				}
			})

			It("Builds a graph of resources", func() {
				process1 := valueStream.Resources[2]

				Ω(process1.Process).Should(Equal("process1"))
				Ω(process1.In).Should(HaveLen(0))
				Ω(process1.Out).Should(HaveLen(1))

				Ω(process1.Out[0].Process).Should(Equal("process2"))
				Ω(process1.Out[0].In).Should(HaveLen(1))
				Ω(process1.Out[0].In[0].Process).Should(Equal("process1"))
				Ω(process1.Out[0].Out).Should(HaveLen(1))

				Ω(process1.Out[0].Out[0].Process).Should(Equal("process3"))
				Ω(process1.Out[0].Out[0].In).Should(HaveLen(1))
				Ω(process1.Out[0].Out[0].In[0].Process).Should(Equal("process2"))
				Ω(process1.Out[0].Out[0].Out).Should(HaveLen(0))
			})

			It("creates a graph object", func() {
				Ω(graphObj.Cells).Should(HaveLen(23))
			})
			It("does not create a graph error", func() {
				Ω(graphErr).Should(BeNil())
			})
		})

		Context("and render process ids is disabled", func() {
			BeforeEach(func() {
				valueStream = mapper.ValueStream{
					Resources: mapper.Resources{
						{
							Process: "process3",
							Type:    "process",
							Title:   "test",
							Metrics: &mapper.Metrics{
								LeadTime:    "2m",
								ProcessTime: "2m",
							},
							PreviousProcess: "process2",
						},
						{
							Process: "process2",
							Title:   "test",
							Kaizen:  "An example action",
							Metrics: &mapper.Metrics{
								LeadTime:    "2m",
								ProcessTime: "2m",
							},
							Flow: &mapper.Flow{
								In:  []string{"flow1"},
								Out: []string{"flow2"},
							},
							PreviousProcess: "process1",
						},
						{
							Process:       "process1",
							Type:          "process",
							Title:         "test",
							NumberOfStaff: 2,
							Metrics: &mapper.Metrics{
								LeadTime:    "2m",
								ProcessTime: "2m",
							},
						},
					},
				}
			})

			It("Builds a graph of resources", func() {
				process1 := valueStream.Resources[2]

				Ω(process1.Process).Should(Equal("process1"))
				Ω(process1.In).Should(HaveLen(0))
				Ω(process1.Out).Should(HaveLen(1))

				process2 := process1.Out[0]
				Ω(process2.Process).Should(Equal("process2"))
				Ω(process2.In).Should(HaveLen(1))
				Ω(process2.In[0].Process).Should(Equal("process1"))
				Ω(process2.Out).Should(HaveLen(1))

				process3 := process2.Out[0]
				Ω(process3.Process).Should(Equal("process3"))
				Ω(process3.In).Should(HaveLen(1))
				Ω(process3.In[0].Process).Should(Equal("process2"))
				Ω(process3.Out).Should(HaveLen(0))
			})

			It("creates a graph object", func() {
				Ω(graphObj.Cells).Should(HaveLen(17))
			})
			It("does not create a graph error", func() {
				Ω(graphErr).Should(BeNil())
			})
		})
	})
})

var _ = Describe("ValueStream#CalulateSummaryMetrics", func() {
	var (
		exampleLeadTimeDuration    = 96 * time.Hour
		exampleProcessTimeDuration = 24 * time.Hour
		exampleCARatio             = "0%"
		exampleActivityRatio       = "75%"
		valueStream                mapper.ValueStream
		workDay                    = "8h"
		workWeek                   = "5d"
	)

	AfterEach(func() {
		exampleLeadTimeDuration = 96 * time.Hour
		exampleProcessTimeDuration = 24 * time.Hour
		exampleCARatio = "0%"
		exampleActivityRatio = "75%"
	})

	JustBeforeEach(func() {
		valueStream = mapper.ValueStream{
			WorkWeekString: workWeek,
			WorkDayString:  workDay,
			Resources: mapper.Resources{
				{
					LongestPath: true,
					Metrics: &mapper.Metrics{
						LeadTimeDuration:    exampleLeadTimeDuration,
						ProcessTimeDuration: exampleProcessTimeDuration,
						CARatio:             exampleCARatio,
						ActivityRatio:       exampleActivityRatio,
					},
				},
				{
					LongestPath: true,

					Metrics: &mapper.Metrics{
						LeadTimeDuration:    10 * time.Minute,
						ProcessTimeDuration: 5 * time.Minute,
						CARatio:             "50%",
						ActivityRatio:       "50%",
					},
				},
				{
					LongestPath: true,

					Metrics: &mapper.Metrics{
						LeadTimeDuration:    20 * time.Minute,
						ProcessTimeDuration: 5 * time.Minute,
						CARatio:             "100%",
						ActivityRatio:       "25%",
					},
				},
			},
		}
	})

	Context("when at least one CARatio is invalid", func() {
		BeforeEach(func() {
			exampleCARatio = "notACAPercentage"
		})

		It("calculates the summaries of all of the metrics", func() {
			Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
			Ω(valueStream.CalculateSummaryMetrics()).Should(MatchError(`strconv.ParseFloat: parsing "notACAPercentage": invalid syntax`))
			Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
		})
	})

	Context("when total process time is greater than total lead time", func() {
		BeforeEach(func() {
			exampleProcessTimeDuration = 240 * time.Hour
			exampleLeadTimeDuration = 1 * time.Minute
		})

		It("returns an error", func() {
			Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
			Ω(valueStream.CalculateSummaryMetrics()).Should(MatchError("lead time cannot be less than process time"))
			Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
		})
	})

	Context("when all metrics are valid", func() {
		Context("and no job resources have a CARatio", func() {
			It("calculates the summaries of all of the metrics", func() {
				valueStream = mapper.ValueStream{
					WorkDayString:  workDay,
					WorkWeekString: workWeek,
					Resources: mapper.Resources{
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    96 * time.Hour,
								ProcessTimeDuration: 24 * time.Hour,
								ActivityRatio:       "75%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    10 * time.Minute,
								ProcessTimeDuration: 5 * time.Minute,
								ActivityRatio:       "50%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    20 * time.Minute,
								ProcessTimeDuration: 5 * time.Minute,
								ActivityRatio:       "25%",
							},
							LongestPath: true,
						},
					},
				}

				Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
				Ω(valueStream.CalculateSummaryMetrics()).Should(Succeed())
				Ω(valueStream.MetricsSummary.LeadTime).Should(Equal("2w2d30m"))
				Ω(valueStream.MetricsSummary.ProcessTime).Should(Equal("3d10m"))
				Ω(valueStream.MetricsSummary.CARatio).Should(Equal("0%"))
				Ω(valueStream.MetricsSummary.ActivityRatio).Should(Equal("25%"))
			})
		})

		Context("and no job resources have a process time", func() {

			It("calculates the summaries of all of the metrics", func() {
				valueStream = mapper.ValueStream{
					WorkDayString:  workDay,
					WorkWeekString: workWeek,
					Resources: mapper.Resources{
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration: 96 * time.Hour,
								ActivityRatio:    "75%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration: 10 * time.Minute,
								ActivityRatio:    "50%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration: 20 * time.Minute,
								ActivityRatio:    "25%",
							},
							LongestPath: true,
						},
					},
				}

				Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
				Ω(valueStream.CalculateSummaryMetrics()).Should(Succeed())
				Ω(valueStream.MetricsSummary.LeadTime).Should(Equal("2w2d30m"))
				Ω(valueStream.MetricsSummary.ProcessTime).Should(Equal("0s"))
				Ω(valueStream.MetricsSummary.CARatio).Should(Equal("0%"))
				Ω(valueStream.MetricsSummary.ActivityRatio).Should(Equal("0%"))
			})
		})

		Context("and no job resources have a lead time or process time", func() {
			It("calculates the summaries of all of the metrics", func() {
				var valueStream = mapper.ValueStream{
					WorkDayString:  workDay,
					WorkWeekString: workWeek,
					Resources: mapper.Resources{
						{
							Metrics: &mapper.Metrics{
								ActivityRatio: "75%",
							},
						},
						{
							Metrics: &mapper.Metrics{
								ActivityRatio: "50%",
							},
						},
						{
							Metrics: &mapper.Metrics{
								ActivityRatio: "25%",
							},
						},
					},
				}

				Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
				Ω(valueStream.CalculateSummaryMetrics()).Should(Succeed())
				Ω(valueStream.MetricsSummary.LeadTime).Should(Equal("0s"))
				Ω(valueStream.MetricsSummary.ProcessTime).Should(Equal("0s"))
				Ω(valueStream.MetricsSummary.CARatio).Should(Equal("0%"))
				Ω(valueStream.MetricsSummary.ActivityRatio).Should(Equal("0%"))
			})
		})

		Context("and some of the jobs provide some of the metrics", func() {
			It("calculates the summaries of all of the metrics", func() {
				var valueStream = mapper.ValueStream{
					WorkDayString:  workDay,
					WorkWeekString: workWeek,
					Resources: mapper.Resources{
						{
							Metrics: &mapper.Metrics{
								CARatio: "0%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    2 * time.Hour,
								ProcessTimeDuration: 1 * time.Hour,
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration: 4 * time.Hour,
								CARatio:          "100%",
							},
							LongestPath: true,
						},
						{},
					},
				}

				Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
				Ω(valueStream.CalculateSummaryMetrics()).Should(Succeed())
				Ω(valueStream.MetricsSummary.LeadTime).Should(Equal("6h"))
				Ω(valueStream.MetricsSummary.ProcessTime).Should(Equal("1h"))
				Ω(valueStream.MetricsSummary.CARatio).Should(Equal("50%"))
				Ω(valueStream.MetricsSummary.ActivityRatio).Should(Equal("16%"))
			})
		})

		Context("and all metrics are present", func() {
			It("calculates the summaries of all of the metrics", func() {
				var valueStream = mapper.ValueStream{
					WorkDayString:  workDay,
					WorkWeekString: workWeek,
					Resources: mapper.Resources{
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    96 * time.Hour,
								ProcessTimeDuration: 24 * time.Hour,
								CARatio:             "0%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    10 * time.Minute,
								ProcessTimeDuration: 5 * time.Minute,
								CARatio:             "50%",
							},
							LongestPath: true,
						},
						{
							Metrics: &mapper.Metrics{
								LeadTimeDuration:    20 * time.Minute,
								ProcessTimeDuration: 5 * time.Minute,
								CARatio:             "100%",
							},
							LongestPath: true,
						},
					},
				}

				Ω(valueStream.MetricsSummary).Should(Equal(mapper.MetricsSummary{}))
				Ω(valueStream.CalculateSummaryMetrics()).Should(Succeed())
				Ω(valueStream.MetricsSummary.LeadTime).Should(Equal("2w2d30m"))
				Ω(valueStream.MetricsSummary.ProcessTime).Should(Equal("3d10m"))
				Ω(valueStream.MetricsSummary.CARatio).Should(Equal("50%"))
				Ω(valueStream.MetricsSummary.ActivityRatio).Should(Equal("25%"))
			})
		})
	})
})

var _ = Describe("ValueSteam#Validate", func() {
	var (
		err       error
		resources = mapper.Resources{
			{
				Process: "process1",
				Type:    "process",
			},
		}
		workDay  = "8h"
		workWeek = "5d"
		version  string
	)

	JustBeforeEach(func() {
		valueStream := &mapper.ValueStream{
			Resources:      resources,
			WorkDayString:  workDay,
			WorkWeekString: workWeek,
			Version:        version,
		}
		err = valueStream.Validate()
	})

	AfterEach(func() {
		resources = mapper.Resources{
			{
				Process: "process1",
				Type:    "process",
			},
		}
		workDay = "8h"
		workWeek = "5d"
		version = ""
	})

	Context("when work day is invalid", func() {
		BeforeEach(func() {
			workDay = "notAValidDuration"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"work-duration-validation-error":"work day notAValidDuration does not follow format 10h3m4s"` +
				"}"))
		})
	})

	Context("when work day is more than work week", func() {
		BeforeEach(func() {
			workDay = "5h"
			workWeek = "4h"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"work-duration-validation-error":"work day must be less than work week"` +
				"}"))
		})
	})

	Context("when work week is invalid", func() {
		BeforeEach(func() {
			workDay = "5h"
			workWeek = "notAValidDuration"
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"work-duration-validation-error":"work week notAValidDuration does not follow format 3d10h3m4s"` +
				"}"))
		})
	})

	Context("work week can include days", func() {
		BeforeEach(func() {
			workDay = "4h"
			workWeek = "5d"
		})

		It("returns no error", func() {
			Ω(err).ShouldNot(HaveOccurred())
		})
	})

	Context("when a resources are invalid", func() {
		BeforeEach(func() {
			resources = mapper.Resources{}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"resource-validation-errors":{` +
				`"empty-resources-validation-error":"maps must have at least one resource"` +
				"}" +
				"}"))
		})
	})

	Context("when a version is invalid", func() {
		BeforeEach(func() {
			version = "not a semver"
		})
		It("returns an error", func() {
			Ω(err).Should(MatchError("{" +
				`"version-validation-error":"version must be semver compliant, was: not a semver"` +
				"}"))
		})
	})

	Context("when everything is valid", func() {
		BeforeEach(func() {
			workDay = ""
			version = "1.0.0"
		})

		It("returns nil", func() {
			Ω(err).Should(BeNil())
		})
	})
})

var _ = Describe("FindProcessCell", func() {
	var (
		cell = mapper.Cell{
			ID: "exampleID",
		}
		cells       = []mapper.Cell{cell}
		resource    mapper.Resource
		matchedCell mapper.Cell
	)

	JustBeforeEach(func() {
		matchedCell = mapper.FindProcessCell(resource, cells)
	})

	Context("when a cell cannot be found", func() {
		BeforeEach(func() {
			resource.Process = "notMatching"
		})

		It("returns an empty cell", func() {
			Ω(matchedCell).Should(Equal(mapper.Cell{}))
		})
	})

	Context("when a cell can be found", func() {
		BeforeEach(func() {
			resource.Process = "exampleID"
		})

		It("returns the matching cell", func() {
			Ω(matchedCell).Should(Equal(cell))
		})
	})
})
