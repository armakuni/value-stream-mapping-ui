package mapper

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Label#IsValid", func() {
	Context("when label position distance is greater than 1", func() {
		It("returns false", func() {
			Ω(Label{
				Position: LabelPosition{
					Distance: 20.01,
				},
			}.IsValid()).Should(BeFalse())
		})
	})

	Context("when label position distance is less than 0", func() {
		It("returns false", func() {
			Ω(Label{
				Position: LabelPosition{
					Distance: -0.5,
				},
			}.IsValid()).Should(BeFalse())
		})
	})

	Context("when label position distance is between 0 and 1", func() {
		It("returns true", func() {
			Ω(Label{
				Position: LabelPosition{
					Distance: 0.5,
				},
			}.IsValid()).Should(BeTrue())
		})
	})
})

var _ = Describe("Labels#AreValid", func() {
	Context("when all labels are invalid", func() {
		It("returns false", func() {
			Ω(Labels{
				{
					Position: LabelPosition{
						Distance: 20.01,
					},
				},
				{
					Position: LabelPosition{
						Distance: -0.2,
					},
				},
			}.AreValid()).Should(BeFalse())
		})
	})

	Context("when at least one label is invalid", func() {
		It("returns false", func() {
			Ω(Labels{
				{
					Position: LabelPosition{
						Distance: 20.01,
					},
				},
				{
					Position: LabelPosition{
						Distance: 0.5,
					},
				},
			}.AreValid()).Should(BeFalse())
		})
	})

	Context("when all labels are valid", func() {
		It("returns true", func() {
			Ω(Labels{
				{
					Position: LabelPosition{
						Distance: 0.2,
					},
				},
				{
					Position: LabelPosition{
						Distance: 0.5,
					},
				},
			}.AreValid()).Should(BeTrue())
		})
	})
})
