package mapper

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Cell#IsValid", func() {
	Context("when label position distance is invalid", func() {
		It("returns false", func() {
			Ω(Cell{
				Labels: []Label{
					{
						Position: LabelPosition{
							Distance: 20.01,
						},
					},
				},
			}.IsValid()).Should(BeFalse())
		})
	})

	Context("when cell size is invalid", func() {
		It("returns false", func() {
			Ω(Cell{
				Size: CellSize{
					Width:  -1,
					Height: -1,
				},
			}.IsValid()).Should(BeFalse())
		})
	})

	Context("when all fields are valid", func() {
		It("returns true", func() {
			Ω(Cell{
				Labels: []Label{
					{
						Position: LabelPosition{
							Distance: 0.5,
						},
					},
				},
			}.IsValid()).Should(BeTrue())
		})
	})
})

var _ = Describe("Cells#AreValid", func() {
	Context("when all cells are invalid", func() {
		It("returns false", func() {
			Ω(Cells{
				{
					Labels: []Label{
						{
							Position: LabelPosition{
								Distance: 20.45,
							},
						},
					},
				},
				{
					Labels: []Label{
						{
							Position: LabelPosition{
								Distance: 89.64,
							},
						},
					},
				},
			}.AreValid()).Should(BeFalse())
		})
	})

	Context("when at least one cell is invalid", func() {
		It("returns true", func() {
			Ω(Cells{
				{
					Labels: []Label{
						{
							Position: LabelPosition{
								Distance: 0.5,
							},
						},
					},
				},
				{
					Labels: []Label{
						{
							Position: LabelPosition{
								Distance: 20.13,
							},
						},
					},
				},
			}.AreValid()).Should(BeFalse())
		})
	})

	Context("when all cells are valid", func() {
		It("returns true", func() {
			Ω(Cells{
				{
					Labels: []Label{
						{
							Position: LabelPosition{
								Distance: 0.5,
							},
						},
					},
				},
				{
					Labels: []Label{
						{
							Position: LabelPosition{
								Distance: 0.2,
							},
						},
					},
				},
			}.AreValid()).Should(BeTrue())
		})
	})
})

var _ = Describe("CellSize#IsValid", func() {
	Context("when height is less than 0", func() {
		It("returns false", func() {
			Ω(CellSize{Width: -1}.IsValid()).Should(BeFalse())
		})
	})

	Context("when width is less than 0", func() {
		It("returns false", func() {
			Ω(CellSize{Height: -1}.IsValid()).Should(BeFalse())
		})
	})

	Context("when height and width are greater than 0", func() {
		It("returns true", func() {
			Ω(CellSize{}.IsValid()).Should(BeTrue())
		})
	})
})

var _ = Describe("NewProcessToCell", func() {
	Context("process without metrics without process keys", func() {
		var (
			cell Cell
			err  error
		)

		BeforeEach(func() {
			cell, err = NewProcessToCell(Resource{
				Type:           "process",
				Process:        "1234",
				Title:          "Title",
				Description:    "Description",
				LeadTimeWeight: 10,
				NumberOfStaff:  31,
				FlowBlockers:   "Blocked",
			}, 10, 20, false)
		})

		It("cell type is node", func() {
			Ω(cell.Type).Should(Equal("uml.Node"))
		})
		It("cell id is set to process", func() {
			Ω(cell.ID).Should(Equal("1234"))
		})
		It("cell description is set", func() {
			Ω(cell.Description).Should(Equal("Description"))
		})
		It("cell title is set", func() {
			Ω(cell.Title).Should(Equal("Title"))
		})
		It("cell co-ordinates are set", func() {
			Ω(cell.Position.X).Should(Equal(10))
			Ω(cell.Position.Y).Should(Equal(20))
		})
		It("cell lead time weight is set", func() {
			Ω(cell.LeadTimeWeight).Should(Equal(10.0))
		})
		It("number of staff is set", func() {
			Ω(cell.NumberOfStaff).Should(Equal(31))
		})
		It("flow blockers is set", func() {
			Ω(cell.FlowBlockers).Should(Equal("Blocked"))
		})
		It("should not set the process id", func() {
			Ω(cell.ProcessID).Should(BeEmpty())
		})
		It("error should not be set", func() {
			Ω(err).Should(BeNil())
		})
	})
	Context("process without metrics with process keys", func() {
		var (
			cell Cell
			err  error
		)

		BeforeEach(func() {
			cell, err = NewProcessToCell(Resource{
				Type:           "process",
				Process:        "1234",
				Title:          "Title",
				Description:    "Description",
				LeadTimeWeight: 10,
				NumberOfStaff:  31,
				FlowBlockers:   "Blocked",
			}, 10, 20, true)
		})

		It("cell type is node", func() {
			Ω(cell.Type).Should(Equal("uml.Node"))
		})
		It("cell id is set to process", func() {
			Ω(cell.ID).Should(Equal("1234"))
		})
		It("cell description is set", func() {
			Ω(cell.Description).Should(Equal("Description"))
		})
		It("cell title is set", func() {
			Ω(cell.Title).Should(Equal("Title"))
		})
		It("cell co-ordinates are set", func() {
			Ω(cell.Position.X).Should(Equal(10))
			Ω(cell.Position.Y).Should(Equal(20))
		})
		It("cell lead time weight is set", func() {
			Ω(cell.LeadTimeWeight).Should(Equal(10.0))
		})
		It("number of staff is set", func() {
			Ω(cell.NumberOfStaff).Should(Equal(31))
		})
		It("flow blockers is set", func() {
			Ω(cell.FlowBlockers).Should(Equal("Blocked"))
		})
		It("should set process id", func() {
			Ω(cell.ProcessID).Should(Equal("1234"))
		})
		It("error should not be set", func() {
			Ω(err).Should(BeNil())
		})
	})
	Context("process with incalculable metrics", func() {
		var (
			cell Cell
			err  error
		)

		BeforeEach(func() {
			duration10h, _ := time.ParseDuration("10h")
			duration20h, _ := time.ParseDuration("20h")

			cell, err = NewProcessToCell(Resource{
				Metrics: &Metrics{
					LeadTimeDuration:    duration10h,
					ProcessTimeDuration: duration20h,
				},
			}, 10, 20, true)
		})

		It("cell should be empty", func() {
			Ω(cell.ID).Should(BeEmpty())
		})
		It("error should be set", func() {
			Ω(err.Error()).Should(Equal("lead time cannot be less than process time"))
		})
	})
	Context("process with calculable metrics", func() {
		var (
			cell Cell
			err  error
		)

		BeforeEach(func() {
			duration10h, _ := time.ParseDuration("10h")
			duration20h, _ := time.ParseDuration("20h")

			cell, err = NewProcessToCell(Resource{
				Process: "1234",
				Metrics: &Metrics{
					LeadTimeDuration:    duration20h,
					ProcessTimeDuration: duration10h,
					LeadTime:            "20h",
					ProcessTime:         "10h",
					CARatio:             "90%",
					CARatioDesc:         "Mostly fine",
				},
			}, 10, 20, true)
		})

		It("cell should be populated like normal", func() {
			Ω(cell.ID).Should(Equal("1234"))
		})
		It("cell should have lead time", func() {
			Ω(cell.LeadTime).Should(Equal("20h"))
		})
		It("cell should have process time", func() {
			Ω(cell.ProcessTime).Should(Equal("10h"))
		})
		It("cell should have activity ratio", func() {
			Ω(cell.ActivityRatio).Should(Equal("50%"))
		})
		It("cell should have c&a ratio", func() {
			Ω(cell.CARatio).Should(Equal("90%"))
		})
		It("cell should have c&a description", func() {
			Ω(cell.CARatioDesc).Should(Equal("Mostly fine"))
		})
		It("error should not be set", func() {
			Ω(err).Should(BeNil())
		})
	})
})

var _ = Describe("NewLadderCell", func() {
	Context("no metrics", func() {
		var (
			cell Cell
		)

		BeforeEach(func() {
			cell = NewLadderCell(Resource{
				Process: "1234",
			}, 10, 20)
		})

		It("cell type is ladder", func() {
			Ω(cell.Type).Should(Equal("uml.Ladder"))
		})
		It("cell id is set to process", func() {
			Ω(cell.ID).Should(Equal("ladder-1234"))
		})
		It("cell co-ordinates are set", func() {
			Ω(cell.Position.X).Should(Equal(10))
			Ω(cell.Position.Y).Should(Equal(20))
		})
	})
	Context("metrics", func() {
		var (
			cell Cell
		)

		BeforeEach(func() {
			cell = NewLadderCell(Resource{
				Process: "1234",
				Metrics: &Metrics{
					LeadTime:    "20h",
					ProcessTime: "10h",
				},
			}, 10, 20)
		})

		It("cell type is ladder", func() {
			Ω(cell.Type).Should(Equal("uml.Ladder"))
		})
		It("cell id is set to process", func() {
			Ω(cell.ID).Should(Equal("ladder-1234"))
		})
		It("cell co-ordinates are set", func() {
			Ω(cell.Position.X).Should(Equal(10))
			Ω(cell.Position.Y).Should(Equal(20))
		})
		It("cell should have lead time", func() {
			Ω(cell.LeadTime).Should(Equal("20h"))
		})
		It("cell should have process time", func() {
			Ω(cell.ProcessTime).Should(Equal("10h"))
		})
	})
})

var _ = Describe("NewKaizenCell", func() {
	var (
		cell Cell
	)

	BeforeEach(func() {
		cell = NewKaizenCell(Resource{
			Process: "1234",
		}, 10, 20)
	})

	It("cell type is kaizen", func() {
		Ω(cell.Type).Should(Equal("uml.Kaizen"))
	})
	It("cell id is set to process", func() {
		Ω(cell.ID).Should(Equal("kaizen-1234"))
	})
	It("cell co-ordinates are set", func() {
		Ω(cell.Position.X).Should(Equal(10))
		Ω(cell.Position.Y).Should(Equal(20))
	})
})

var _ = Describe("NewSummaryCell", func() {
	var (
		cell Cell
		err  error
	)

	BeforeEach(func() {
		cell, err = NewSummaryCell(ValueStream{
			MetricsSummary: MetricsSummary{
				LeadTime:      "1h",
				ProcessTime:   "20h",
				ActivityRatio: "5%",
				CARatio:       "60%",
			},
		}, 10, 20)
	})

	It("cell type is summary", func() {
		Ω(cell.Type).Should(Equal("uml.Summary"))
	})
	It("cell has lead time", func() {
		Ω(cell.LeadTime).Should(Equal("1h"))
	})
	It("cell has process time", func() {
		Ω(cell.ProcessTime).Should(Equal("20h"))
	})
	It("cell has activity ratio", func() {
		Ω(cell.ActivityRatio).Should(Equal("5%"))
	})
	It("cell has c&a ratio", func() {
		Ω(cell.CARatio).Should(Equal("60%"))
	})
	It("cell co-ordinates are set", func() {
		Ω(cell.Position.X).Should(Equal(237))
		Ω(cell.Position.Y).Should(Equal(20))
	})
	It("error should not be set", func() {
		Ω(err).Should(BeNil())
	})
})
