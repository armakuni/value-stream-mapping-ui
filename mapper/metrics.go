package mapper

import (
	"fmt"
	"regexp"
	"time"
)

// Metrics - value stream mapping metrics
type Metrics struct {
	LeadTime            string        `json:"leadtime"`
	ProcessTime         string        `json:"processtime"`
	ActivityRatio       string        `json:"-"`
	CARatio             string        `json:"c&aratio"`
	CARatioDesc         string        `json:"c&aratio_desc,omitempty"`
	LeadTimeDuration    time.Duration `json:"-"`
	ProcessTimeDuration time.Duration `json:"-"`
}

type metricsValidatorErr struct {
	LeadTime      string `json:"lead-time-validation-error,omitempty"`
	ProcessTime   string `json:"process-time-validation-error,omitempty"`
	ActivityRatio string `json:"activity-ratio-validation-error,omitempty"`
	CARatio       string `json:"ca-ratio-validation-error,omitempty"`
}

func (m metricsValidatorErr) Error() string {
	return basicMarshal(m)
}

// Validate - validates that the properties of metrics are valid
func (metrics *Metrics) Validate() error {
	var validatorError metricsValidatorErr
	leadTimeDuration := metrics.validateLeadTime(&validatorError)
	metrics.validateProcessTime(&validatorError, leadTimeDuration)
	metrics.validateCARatio(&validatorError)
	if validatorError != (metricsValidatorErr{}) {
		return validatorError
	}
	return nil
}

// Calculate - calculates activity ration based on process time / lead time, sets it as a percentage value as an int
func (metrics *Metrics) Calculate() error {
	if metrics.LeadTimeDuration < metrics.ProcessTimeDuration {
		return fmt.Errorf("lead time cannot be less than process time")
	}
	var activityRatio int
	if metrics.LeadTimeDuration != 0 {
		activityRatio = int((metrics.ProcessTimeDuration.Seconds() / metrics.LeadTimeDuration.Seconds()) * 100)
	}
	metrics.ActivityRatio = fmt.Sprintf("%v%%", activityRatio)
	return nil
}

// CalculateDurations - sets lead time/ process time as durations
func (metrics *Metrics) CalculateDurations(workDuration WorkDuration) error {
	leadTimeDuration, processTimeDuration, err := metrics.calculateDurations(workDuration)

	if err != nil {
		return err
	}

	metrics.LeadTimeDuration = leadTimeDuration
	metrics.ProcessTimeDuration = processTimeDuration

	return nil
}

func (metrics *Metrics) calculateDurations(workDuration WorkDuration) (leadTime time.Duration, processTime time.Duration, err error) {
	leadTimeDuration, err := workDuration.StringToDuration(metrics.LeadTime)

	if err != nil {
		return time.Duration(0), time.Duration(0), err
	}

	processTimeDuration, err := workDuration.StringToDuration(metrics.ProcessTime)
	if err != nil {
		return time.Duration(0), time.Duration(0), err
	}

	return leadTimeDuration, processTimeDuration, nil
}

func (metrics *Metrics) validateLeadTime(validatorError *metricsValidatorErr) time.Duration {
	workDuration, initErr := NewWorkDuration(8*time.Hour, 5*8*time.Hour)

	if initErr != nil {
		validatorError.LeadTime = fmt.Sprintf("lead time \"%s\" does not match the format 1w2d3h1m2s", metrics.LeadTime)
	}

	leadTimeDuration, err := workDuration.StringToDuration(metrics.LeadTime)

	if err != nil {
		validatorError.LeadTime = fmt.Sprintf("lead time (%s) is not in the supported format is: 2d3h1m2s", metrics.LeadTime)
	}

	return leadTimeDuration
}

func (metrics *Metrics) validateProcessTime(validatorError *metricsValidatorErr, leadTimeDuration time.Duration) {
	workDuration, initErr := NewWorkDuration(8*time.Hour, 5*8*time.Hour)

	if initErr != nil {
		validatorError.ProcessTime = fmt.Sprintf("process time (%s) is not in the supported format is: 2d3h1m2s", metrics.ProcessTime)
	}

	processTimeDuration, err := workDuration.StringToDuration(metrics.ProcessTime)

	if err != nil {
		validatorError.ProcessTime = fmt.Sprintf("process time (%s) is not in the supported format is: 2d3h1m2s", metrics.ProcessTime)
	}

	if *validatorError == (metricsValidatorErr{}) {
		if processTimeDuration > leadTimeDuration {
			validatorError.ActivityRatio = fmt.Sprintf("lead time must be longer than process time, lead time was: %s, process time was: %s", metrics.LeadTime, metrics.ProcessTime)
		}
	}
}

func (metrics *Metrics) validateCARatio(validatorError *metricsValidatorErr) {
	if metrics.CARatio != "" {
		if !validPercentage(metrics.CARatio) {
			validatorError.CARatio = fmt.Sprintf("c&a ratio must be a valid percentage but was: %s", metrics.CARatio)
		}
	}
}

func validPercentage(percentage string) bool {
	percentageRegex := regexp.MustCompile("^(?:[1-9][0-9]?|100|0)%$")
	return percentageRegex.MatchString(percentage)
}
