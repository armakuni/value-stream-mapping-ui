# value-stream-mapping-ui

*VSM is currently deployed to: <https://vsm.armakuni.co.uk>*

Value Stream Mapping (VSM) UI is a tool built around the concepts
described in the [Value Stream Mapping Book][vsm-book] by Karen Martin
and Mike Osterling. If you are new to Value Stream Mapping as a
methodology then I highly recommend reading the book before going too
much further.

Its intention is to make drawing and visualising Value Stream Maps much
easier by having the content declared as YAML and viewable/ shareable in
your browser.

## Usage

### CLI

See [value-stream-mapping-cli][vsm-cli]

### API

#### Create/Update

`PUT` your yaml file (as json).

All maps are stores in a postgres database with a `name` tag. This field
will become your `title` unless you explicitly set one in your yaml.

If your YAML contains a `version` then you can `PUT` multiple maps with
the same name and different versions to enable the versioning feature so
you can switch through versions from a single view.

The below example requires the [yaml][yaml-cli] cli.

```sh
curl -X PUT \
     "https://vsm.armakuni.co.uk/v1/maps/<map_name>" \
     -d "$(yaml -j r <map_file>)" \
     -u "<username>:<password>"
```

#### Delete

Use a HTTP `DELETE` request in order to delete maps, as with creating
you delete maps by `name`. If you want to delete a particular version of
your map, specify its version in a query string as below.

##### Without version

```sh
curl -X DELETE \
     "https://vsm.armakuni.co.uk/v1/maps/<map_name>" \
     -u "<username>:<password>"
```

##### With version

```sh
curl -X DELETE \
     "https://vsm.armakuni.co.uk/v1/maps/<map_name>?version=<version>" \
     -u "<username>:<password>"
```

## Build, run and test

### Cloning this repo

To clone this repo:

```
if ! go get bitbucket.org/armakuni/value-stream-mapping-ui ; then
  mkdir -p "$GOPATH/src/bitbucket.org/armakuni/value-stream-mapping-ui"
  git clone git@bitbucket.org:armakuni/value-stream-mapping-ui.git \
            "$GOPATH/src/bitbucket.org/armakuni/value-stream-mapping-ui"
fi

cd "$GOPATH/src/bitbucket.org/armakuni/value-stream-mapping-ui"
```

### Dependencies

#### Development

* [brew][brew] (Used to install NPM)
* [Golang][golang]

#### Test

* [brew][brew] (Used to install ChromeDriver)
* [Chrome][chrome]
* [Docker][docker]
* That you have [the CLI tool checked out and built][vsm-cli]

### Build Tools

Everything in this project is served by a self-describing `Makefile` to
see all targets and see what they do run:

```sh
$ make
build-test                     install chromedriver
build                          build the value-stream-mapping-ui binary
docker-build-cf-deploy         build cf-deploy docker image
docker-build-qc                build go-quality-checks docker image
docker-build-vsm-test          build vsm-tests docker image
docker-build                   builds docker images
docker-push-cf-deploy          push cf-deploy docker image
docker-push-qc                 push go-quality-checks docker image
docker-push-vsm-test           push vsm-tests docker image
docker-push                    push images to dockerhub
fly-target                     setup a fly target to the team used for this project
git-crypt-export-key           export git-crypt key (this should be in 1password and never be required again)
git-crypt-unlock               unlock git-crypt using a vsm.key private key file
install-local                  install tools required for running locally
integration-test               run integration tests
npm                            install npm deps
prepare-local                  install tools and deps for running locally
run                            run app
set-pipeline                   set the vsm pipeline
test                           run all tests
unit-test                      run unit tests
```

### Compiling

If you want to build/run/test this code locally it is recommended that
you run:

```sh
make prepare-local
```

You can then build

```
make build
```

And run the tests

```sh
make test
```

### Running

To run the app run

```sh
make run
```

To add a sample map you can use [vsm cli tool][vsm-cli]:

```sh
vsm -u "$(yaml r private/local-secrets.yml vsm-local-1-username)" \
    -p "$(yaml r private/local-secrets.yml vsm-local-1-password)" \
    --uri http://localhost:8080 \
    set-map -n example -m maps/example1.yml
```

And view it in the UI

```sh
open "http://$(yaml r private/local-secrets.yml vsm-local-1-username):$(yaml r private/local-secrets.yml vsm-local-1-password)@localhost:8080/maps/example"
```

## CI

### Dependencies

* [Fly (Concourse CLI tool)][fly]
* [git-crypt][git-crypt]

### Setting the pipeline

You will need to target/ login to concourse before you can push updates
to the pipeline/hijack etc.

```sh
make fly-target
```

To push the pipeline:

```sh
make set-pipeline
```

### Docker images

The CI pipeline currently has a number of dependencies on Dockerfiles,
this can all be built and pushed using `make`

To build a Dockerfile you will need [docker][docker] installed.

We have tried to add an individual target for each Dockerfile and also a
single target for all.

Individuals:

```sh
make docker-build-cf-deploy
make docker-build-qc
make docker-build-vsm-test
make docker-push-cf-deploy
make docker-push-qc
make docker-push-vsm-test
```

All:

```sh
make docker-build
make docker-push
```

## Project Process

See [project/](project/) documents for example issues, and similar
project specific agreements, and issue templates.

* [Backup/Restore Process](project/backup-restore.md)
* [User On-boarding Process](project/onboarding-a-new-user.md)

## Secrets

### Git-Crypt

This project uses [git-crypt][git-crypt] to encrypt secrets that need to
be injected into Concourse, and are used for tests. If you need to
unlock git-crypt the key can be found in `1Password` and you can run the
following:

```sh
make git-crypt-unlock # key must be called vsm.key
```

[git-crypt]: https://www.agwa.name/projects/git-crypt/
[vsm-book]: https://www.ksmartin.com/books/value-stream-mapping/
[vsm-cli]: https://bitbucket.org/armakuni/value-stream-mapping-cli
[yaml-cli]: https://github.com/mikefarah/yaml
[brew]: https://brew.sh/
[golang]: https://golang.org/
[chrome]: https://www.google.com/chrome/browser/desktop/index.html
[fly]: https://github.com/concourse/fly#installing-from-the-concourse-ui-for-project-development
[docker]: http://docker.com/
