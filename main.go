package main

import (
	"database/sql"
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"os"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	vsmDB "bitbucket.org/armakuni/value-stream-mapping-ui/db"
	"bitbucket.org/armakuni/value-stream-mapping-ui/web"
	"github.com/mattes/migrate"

	// postgresql driver
	_ "github.com/lib/pq"
)

var templates *template.Template

var templateVariables struct{ Year int }

// Start - runs value-stream-mapping-ui web server
func Start(port string) {
	log.Println("version", os.Getenv("BUILD_VERSION"))

	connectionString, err := vsmDB.GetConnectionDetails()
	if err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	dbEncryptionKey := os.Getenv("DB_ENCRYPTION_KEY")

	if dbEncryptionKey == "" {
		log.Fatal("database needs a encryption key", dbEncryptionKey)
	}

	database := vsmDB.VsmDatabase{DB: db}

	var userCredentials authentication.UserCredentials

	if os.Getenv("USER_CREDENTIALS") != "" {
		err = json.Unmarshal([]byte(os.Getenv("USER_CREDENTIALS")), &userCredentials)

		if err != nil {
			log.Fatal(err)
		}
	} else {
		userCredentials = make(authentication.UserCredentials)
	}

	if os.Getenv("USERNAME") != "" && os.Getenv("PASSWORD") != "" {
		userCredentials[os.Getenv("USERNAME")] = os.Getenv("PASSWORD")
	}

	authenticator := authentication.NewAuthenticationMiddleware(dbEncryptionKey, userCredentials)

	migrateDB(database)

	apiController := web.NewAPIController(&database, authenticator)

	baseDir := os.Getenv("BASE_DIR")
	uiController := web.NewUIController(&database, authenticator, baseDir)

	server := web.NewServer(authenticator, apiController, uiController, baseDir)
	if err != nil {
		log.Fatal(err)
	}

	// App
	router := server.Start()

	http.Handle("/", router)

	log.Println("Listening on: http://127.0.0.1:" + port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Println(err.Error())
	}
}

func migrateDB(database vsmDB.VsmDatabase) {
	migrations, err := vsmDB.BuildMigrations(&database, migrationsPath())
	if err != nil {
		log.Fatal(err)
	}

	migrationErr := migrations.Up()

	if migrationErr != nil && migrate.ErrNoChange != migrationErr {
		log.Fatal(
			migrationErr,
			"\n You may need to use the commandline tool to repair the database https://github.com/mattes/migrate",
		)
	}
}

func main() {
	var port string
	if os.Getenv("PORT") == "" {
		port = "8080"
	} else {
		port = os.Getenv("PORT")
	}
	Start(port)
}

func migrationsPath() string {
	migrationPath := "migrations"

	if os.Getenv("BASE_DIR") != "" {
		return "file://" + os.Getenv("BASE_DIR") + migrationPath
	}
	return "file://./" + migrationPath
}
