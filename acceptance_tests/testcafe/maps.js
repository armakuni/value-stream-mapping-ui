import { Selector } from 'testcafe';
import config from '../config';

fixture`VSM`
  .page`${config.baseUrl}/maps/vsm`
  .httpAuth({
    username: `${config.username}`,
    password: `${config.password}`,
  });

test('View info', async t => {
  await t
    .expect(Selector('#burger-button').getStyleProperty('background-color')).eql('rgb(26, 37, 47)')
    .hover(Selector('#burger-button'))
    .expect(Selector('#burger-button').getStyleProperty('background-color')).eql('rgb(38, 55, 72)')
    .expect(Selector('#burger-content').getStyleProperty('display')).eql('none')

    .click(Selector('#burger-button'), { speed: 0.2 })

    .expect(Selector('#burger-content').getStyleProperty('display')).eql('block')
    .expect(Selector('#print-button', { visibilityCheck: true }).innerText).match(/^\sprint$/)
    .expect(Selector('#working-day', { visibilityCheck: true }).innerText).eql('Working Day: 7h')
    .expect(Selector('#working-week', { visibilityCheck: true }).textContent).eql('Working Week: 5d')
    .expect(Selector('#working-day', { visibilityCheck: true }).parent(0).getStyleProperty('background-color')).eql('rgb(26, 37, 47)')

    .hover(Selector('#working-day', { visibilityCheck: true }))

    .expect(Selector('#working-day', { visibilityCheck: true }).parent(0).getStyleProperty('background-color')).eql('rgb(29, 41, 53)');
});

test('Go home', async t => {
  await t
    .expect(Selector('#home-button').getStyleProperty('background-color')).eql('rgb(26, 37, 47)')
    .hover(Selector('#home-button'))
    .expect(Selector('#home-button').getStyleProperty('background-color')).eql('rgb(38, 55, 72)')

    .click(Selector('#home-button'), { speed: 0.2 })

    .expect(Selector('#index h1').innerText).eql('Value Stream Mapping UI')
    .expect(Selector('#titles a').count).gt(1);
});

test('Re-centre', async t => {
  await t
    .expect(Selector('#re-centre-button').getStyleProperty('background-color')).eql('rgb(26, 37, 47)')
    .hover(Selector('#re-centre-button'))
    .expect(Selector('#re-centre-button').getStyleProperty('background-color')).eql('rgb(38, 55, 72)');
});

test('Diff versions', async t => {
  await t
    .expect(Selector('#compare-version select').getStyleProperty('background-color')).eql('rgb(26, 37, 47)')
    .hover(Selector('#compare-version'))
    .expect(Selector('#compare-version select').getStyleProperty('background-color')).eql('rgb(38, 55, 72)')

    .click(Selector('#compare-version'), { speed: 0.2 })
    .click(Selector('#compare-version').find('option').withText('0.1.0-current'), { speed: 0.2 })

    .expect(Selector('.uml-diff-source-version').textContent).eql('0.1.0-future')
    .expect(Selector('.uml-diff-comparison-version').textContent).eql('0.1.0-current');
});

test('Change versions', async t => {
  await t
    .expect(Selector('#change-version select').getStyleProperty('background-color')).eql('rgb(26, 37, 47)')
    .hover(Selector('#change-version'))
    .expect(Selector('#change-version select').getStyleProperty('background-color')).eql('rgb(38, 55, 72)')

    .expect(Selector('.uml-summary-lead-time').textContent).match(/^total\slead\stime\s=\s2d1h$/)

    .click(Selector('#change-version'), { speed: 0.2 })
    .click(Selector('#change-version').find('option').withText('0.1.0-current'), { speed: 0.2 })

    .expect(Selector('.uml-summary-lead-time').textContent).match(/^total\slead\stime\s=\s2w2d1h5m$/)
});
