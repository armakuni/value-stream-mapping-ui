import { Selector } from 'testcafe';
import config from '../config';

fixture`VSM Menu`
  .page`${config.baseUrl}`
  .httpAuth({
    username: `${config.username}`,
    password: `${config.password}`,
  })


test('Load a map', async t => {
  await t
    .click(Selector('#titles a').withText('VSM: Path To Live', { timeout: 70000 }))
    .expect(Selector('#title h1').innerText).eql('VSM: Path To Live', { timeout: 70000 })
    .expect(Selector('.uml-start-text').textContent).eql('Mapper');
});
