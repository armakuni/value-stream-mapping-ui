package encryption

import (
	"crypto/rand"
	"crypto/sha1"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"

	"golang.org/x/crypto/nacl/secretbox"
)

// These are defined in golang.org/x/crypto/nacl/secretbox
const keySize = 32
const nonceSize = 24

// Credentials - Credentials to encrypt and decrypt with
type Credentials struct {
	Pad string
	Key string
}

// Encrypt message
func (e Credentials) Encrypt(clear string) (string, error) {
	if len(e.Pad) < keySize {
		return "", fmt.Errorf("pad must be at least %d characters pad given was %d", keySize, len(e.Pad))
	}

	key := []byte(e.Key)
	key = append(key, []byte(e.Pad)...)

	naclKey := new([keySize]byte)
	copy(naclKey[:], key[:keySize])

	nonce := new([nonceSize]byte)
	if _, err := io.ReadFull(rand.Reader, nonce[:]); err != nil {
		return "", err
	}

	encrypted := make([]byte, nonceSize)
	copy(encrypted, nonce[:])
	encrypted = secretbox.Seal(encrypted, []byte(clear), nonce, naclKey)

	return base64.StdEncoding.EncodeToString([]byte(encrypted)), nil
}

// Decrypt message encrypted by encrypt function
func (e Credentials) Decrypt(base64Encrypted string) (string, error) {
	if len(e.Pad) < keySize {
		return "", fmt.Errorf("pad must be at least %d characters pad given was %d", keySize, len(e.Pad))
	}

	var genericDecryptionFailure = fmt.Errorf("could not decrypt the message")

	if base64Encrypted == "" {
		return "", genericDecryptionFailure
	}

	encrypted, err := base64.StdEncoding.DecodeString(base64Encrypted)

	if err != nil {
		return "", err
	}

	key := []byte(e.Key)
	key = append(key, []byte(e.Pad)...)

	naclKey := new([keySize]byte)
	copy(naclKey[:], key[:keySize])
	nonce := new([nonceSize]byte)
	copy(nonce[:], encrypted[:nonceSize])

	message, ok := secretbox.Open(nil, encrypted[nonceSize:], nonce, naclKey)
	if ok {
		return string(message), nil
	}

	return "", genericDecryptionFailure
}

// Hash a string for comparison in the database.
func (e Credentials) Hash(s string) string {
	hash := sha512.New()
	hash.Write([]byte(s))
	hash.Write([]byte(e.Key))

	return hex.EncodeToString(hash.Sum(nil))
}

// NewCredentials - Generate new encryption credentials
func NewCredentials(username string, password string, dbEncryptionKey string, pad string) (*Credentials, error) {
	if len(pad) < keySize {
		return nil, fmt.Errorf("pad must be at least %d characters pad given was %d", keySize, len(pad))
	}

	// Note: This is simply generating the key, not the storage mechanism
	h := sha1.New()
	h.Write(
		[]byte(
			fmt.Sprintf(
				"%s:%s:%s",
				username,
				password,
				dbEncryptionKey,
			),
		),
	)

	return &Credentials{
		Pad: pad,
		Key: hex.EncodeToString(h.Sum(nil)),
	}, nil
}
