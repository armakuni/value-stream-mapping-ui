package encryption_test

import (
	"bitbucket.org/armakuni/value-stream-mapping-ui/encryption"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("encryption#Encrypt", func() {
	Context("pad is invalid", func() {
		var cypherText string
		var input string
		var encryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			input = "message to encrypt"

			credentials = encryption.Credentials{Pad: "too short", Key: "user secret"}
			cypherText, encryptionErr = credentials.Encrypt(input)
		})

		It("does not create an encrypted string", func() {
			Ω(cypherText).Should(Equal(""))
		})
		It("creates a pad error", func() {
			Ω(encryptionErr).Should(MatchError("pad must be at least 32 characters pad given was 9"))
		})
	})
})
var _ = Describe("encryption#Decrypt - encryption#Encryption integration", func() {
	Context("pad is valid, secret valid", func() {
		var cypherText string
		var clearText string
		var input string
		var encryptionErr error
		var decryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			input = "message to encrypt"
			credentials = encryption.Credentials{Pad: "]\\MIUsQm%g/JuxoKMt'5LMZpiKUwi(lz", Key: "user secret"}

			cypherText, encryptionErr = credentials.Encrypt(input)
			clearText, decryptionErr = credentials.Decrypt(cypherText)
		})

		It("creates a decrypt-able string", func() {
			Ω(clearText).Should(Equal("message to encrypt"))
		})
		It("there shouldn't be an error on encryption", func() {
			Ω(encryptionErr).Should(BeNil())
			Ω(decryptionErr).Should(BeNil())
		})
	})
})
var _ = Describe("encryption#Decrypt", func() {
	Context("decryption pad is too short", func() {
		var clearText string
		var input string
		var decryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			input = "message to encrypt"
			credentials = encryption.Credentials{Pad: "too short", Key: "user secret"}

			clearText, decryptionErr = credentials.Decrypt(input)
		})

		It("does not create an decrypted string", func() {
			Ω(clearText).Should(Equal(""))
		})
		It("creates a pad error message", func() {
			Ω(decryptionErr).Should(MatchError("pad must be at least 32 characters pad given was 9"))
		})
	})
	Context("decryption pad does not match", func() {
		var clearText string
		var input string
		var decryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			credentials = encryption.Credentials{Pad: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", Key: "user secret"}

			input = "gi7VLXHXp05tUHCXtLsiUmwXKYkMBSVMSaMz4N+mGVLGXGww5925ZTdiXLPYqYjXtI0SPsu+0X/cYQ=="
			input = "message to encrypt"

			clearText, decryptionErr = credentials.Decrypt(input)
		})

		It("does not create an decrypted string", func() {
			Ω(clearText).Should(Equal(""))
		})
		It("returns an error", func() {
			Ω(decryptionErr).Should(Not(BeNil()))
		})
	})
	Context("decryption pad does not match", func() {
		var clearText string
		var input string
		var decryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			credentials = encryption.Credentials{Pad: "FwE+}DYnjyMr5,f:h2)]w#$!!k|qD$`1", Key: "user secret"}
			input = "gi7VLXHXp05tUHCXtLsiUmwXKYkMBSVMSaMz4N+mGVLGXGww5925ZTdiXLPYqYjXtI0SPsu+0X/cYQ=="

			clearText, decryptionErr = credentials.Decrypt(input)
		})

		It("does not create an decrypted string", func() {
			Ω(clearText).Should(Equal(""))
		})
		It("gives a decryption error", func() {
			Ω(decryptionErr).Should(MatchError("could not decrypt the message"))
		})
	})
	Context("decryption works", func() {
		var clearText string
		var decryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			credentials = encryption.Credentials{Pad: "FwE+}DYnjyMr5,f:h2)]w#$!!k|qD$`1", Key: "user secret"}
			clearText, decryptionErr = credentials.Decrypt("QEktAe33GXN+MRG+te2L2THG6UlrlUyd1hz1MI5gLnC9o7/DH1sEwgLm7UWGW4+fIQ2+mA==")
		})

		It("decrypts the message", func() {
			Ω(clearText).Should(Equal("message here"))
		})
		It("there is no error", func() {
			Ω(decryptionErr).Should(BeNil())
		})
	})
	Context("decrypting a empty string", func() {
		var clearText string
		var decryptionErr error
		var credentials encryption.Credentials

		BeforeEach(func() {
			credentials = encryption.Credentials{Pad: "FwE+}DYnjyMr5,f:h2)]w#$!!k|qD$`1", Key: "user secret"}
			clearText, decryptionErr = credentials.Decrypt("")
		})

		It("returns an empty string", func() {
			Ω(clearText).Should(Equal(""))
		})
		It("returns a decryption error", func() {
			Ω(decryptionErr).Should(MatchError("could not decrypt the message"))
		})
	})
})

var _ = Describe("encryption#Hash", func() {
	Context("hashes a string", func() {
		var clearText string
		var credentials encryption.Credentials

		BeforeEach(func() {
			credentials = encryption.Credentials{Pad: "too short", Key: "user secret"}

			clearText = credentials.Hash("example")
		})

		It("does not create an decrypted string", func() {
			Ω(clearText).Should(Equal("6c4ef4041f11b2a9a1d8210a3f30fabd7d20c70c2dd0c3268f13185731a07e6ea5eece19d1fc852cbb96f9d1fb7cb137c97a3e68b7544cf2bb6fce476ddfedde"))
		})
	})
})
var _ = Describe("NewCredentials", func() {
	Context("long enough pad", func() {
		var credentials *encryption.Credentials

		BeforeEach(func() {
			credentials, _ = encryption.NewCredentials("user name", "user password", "server password", "FwE+}DYnjyMr5,f:h2)]w#$!!k|qD$`1")
		})

		It("sets the pad", func() {
			Ω(credentials.Pad).Should(Equal("FwE+}DYnjyMr5,f:h2)]w#$!!k|qD$`1"))
		})

		It("generates a key from the multiple data fragments", func() {
			Ω(credentials.Key).Should(Equal("c7db0b516345a0cd99ed18ba256b3b5cfab079be"))
		})
	})
	Context("too short pad", func() {
		var credentials *encryption.Credentials
		var credentialsErr error

		BeforeEach(func() {
			credentials, credentialsErr = encryption.NewCredentials("user name", "user password", "server password", "short")
		})

		It("credentials set", func() {
			Ω(credentials).Should(BeNil())
		})
		It("creates a pad error message", func() {
			Ω(credentialsErr).Should(MatchError("pad must be at least 32 characters pad given was 5"))
		})
	})
})
