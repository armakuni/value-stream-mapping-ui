help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the value-stream-mapping-ui binary
	@go build

build-test: ## install chromedriver
	@brew install chromedriver

browserify-install: ## install browserify
ifeq (, $(shell which browserify))
	@npm install browserify
endif

browserify: npm browserify-install ## run browserify
	@browserify js/umlsc.js -o assets/js/umlsc.js
	@browserify js/index.js -o assets/js/index.js

run: browserify ## run app
	@./scripts/run.sh

unit-test: ## run unit tests
	@./scripts/test.sh

integration-test: ## run integration tests
	@GOPRIVATE="bitbucket.org/armakuni/value-stream-mapping-cli,bitbucket.org/armakuni/value-stream-mapping-ui" ./scripts/integration-test.sh

quality-checks: ## run all lint and quality checks
	@./scripts/quality-checks.sh

security-test: ## run an active security test against the VSM app
	@./scripts/security-test.sh

test: quality-checks unit-test integration-test ## run all tests

set-test-maps: ## set a group of maps to show functionality locally
	@./scripts/set-test-maps.sh

npm: ## install npm deps
	@npm install

fly-target: ## setup a fly target to the team used for this project
	@fly -t vsm-dev login -n ak-software-github -c https://ci.armakuni.co.uk/

git-crypt-export-key: ## export git-crypt key (this should be in 1password and never be required again)
	@git-crypt export-key vsm.key

git-crypt-unlock: ## unlock git-crypt using a vsm.key private key file
	@git-crypt unlock vsm.key

set-pipeline: ## set the vsm pipeline
	@fly -t vsm-dev validate-pipeline -l private/secrets.yml -c ci/pipeline.yml
	@fly -t vsm-dev set-pipeline -l private/secrets.yml -c ci/pipeline.yml -p vsm
	@fly -t vsm-dev unpause-pipeline -p vsm
	@fly -t vsm-dev expose-pipeline -p vsm

update-aws: ## update aws using terraform
	@./scripts/update-aws.sh

create-aws-terraform-resources: ## create resources in aws required for terraform to use
	@./scripts/init-terraform.sh

retrieve-latest-backup: ## Output the latest database backup to stdout
	@./scripts/retrieve-latest-backup.sh

docker-build: docker-build-qc docker-build-vsm-test docker-build-cf-deploy docker-build-bd docker-build-openssl-cli docker-build-testcafe## builds docker images

docker-push: docker-push-qc docker-push-vsm-test docker-push-cf-deploy docker-push-bd docker-push-openssl-cli docker-push-testcafe ## push images to dockerhub

docker-build-qc: ## build go-quality-checks docker image
	@docker build -t armakuni/go-quality-checks ci/docker-images/go-quality-checks

docker-push-qc: ## push go-quality-checks docker image
	@docker push armakuni/go-quality-checks

docker-build-bd: ## build build-docs docker image
	@docker build -t armakuni/build-docs ci/docker-images/build-docs

docker-push-bd: ## build build-docs docker image
	@docker push armakuni/build-docs

docker-build-vsm-test: ## build vsm-tests docker image
	@docker build -t armakuni/vsm-tests ci/docker-images/vsm-tests

docker-build-openssl-cli: ## build openssl docker image
	@docker build -t armakuni/openssl-cli ci/docker-images/openssl-cli

docker-push-vsm-test: ## push vsm-tests docker image
	@docker push armakuni/vsm-tests

docker-build-cf-deploy: ## build cf-deploy docker image
	@docker build -t armakuni/cf-deploy ci/docker-images/cf-deploy

docker-push-cf-deploy: ## push cf-deploy docker image
	@docker push armakuni/cf-deploy

docker-push-openssl-cli: ## push openssl docker image
	@docker push armakuni/openssl-cli

docker-build-testcafe: ## build testcafe docker image
	@docker build -t armakuni/testcafe ci/docker-images/testcafe

docker-push-testcafe: ## push testcafe docker image
	@docker push armakuni/testcafe

install-local: ## install tools required for running locally
	@./scripts/install-local.sh

prepare-local: install-local npm ## install tools and deps for running locally
	@go mod tidy
