## Generic


resource "aws_route53_record" "vsm_cname" {
  zone_id = "${var.existing_zone_id}"
  name    = "vsm"
  type    = "CNAME"

  records = [
    "armakuni.co.uk-a56ab096.ssl.run.pivotal.io"
  ]

  ttl = "300"
}

resource "aws_route53_record" "docs_cname" {
  zone_id = "${var.existing_zone_id}"
  name    = "vsm-docs"
  type    = "CNAME"

  records = [
    "armakuni.co.uk-a56ab096.ssl.run.pivotal.io"
  ]

  ttl = "300"
}

variable "existing_zone_id" {
  type = "string"
  description = "The zone id that the vsm cname's will reside in."
}
