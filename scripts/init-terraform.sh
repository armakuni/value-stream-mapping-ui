#!/usr/bin/env bash

set -euo pipefail

echo "This needs your personal AWS credentials"
(cd ./terraform-initial && terraform init)
(cd ./terraform-initial && terraform apply)
