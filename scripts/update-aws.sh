#!/usr/bin/env bash

set -euo pipefail

export AWS_ACCESS_KEY_ID="$(cd ./terraform-initial && terraform output id)"
export AWS_SECRET_ACCESS_KEY="$(cd ./terraform-initial && terraform output secret)"

(cd ./terraform && terraform init )
(cd ./terraform && terraform apply \
  -var "access_key=${AWS_ACCESS_KEY_ID}" \
  -var "secret_key=${AWS_SECRET_ACCESS_KEY}" \
  -var "region=eu-west-1" \
  -refresh=true)
