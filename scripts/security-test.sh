#!/usr/bin/env bash

set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR}/functions.sh"

run_postgres
run_app \
  "$(yaml r ./test-secrets.yml vsm-local-1-username)" \
  "$(yaml r ./test-secrets.yml vsm-local-1-password)" \
  "$(yaml r ./test-secrets.yml vsm-local-2-username)" \
  "$(yaml r ./test-secrets.yml vsm-local-2-password)" \
  "$(yaml r ./test-secrets.yml vsm-local-db-encryption-key)"

set_map "vsm" "vsm-vsm-current.yml" "$(yaml r ./test-secrets.yml vsm-local-1-username)" "$(yaml r ./test-secrets.yml vsm-local-1-password)"
set_map "vsm" "vsm-vsm-future.yml" "$(yaml r ./test-secrets.yml vsm-local-1-username)" "$(yaml r ./test-secrets.yml vsm-local-1-password)"


docker run --rm owasp/zap2docker-weekly zap-full-scan.py -T 5 -a -j -t "http://$(yaml r ./test-secrets.yml vsm-local-1-username):$(yaml r ./test-secrets.yml vsm-local-1-password)@host.docker.internal:8080/"
if lsof -ti tcp:8080 &> /dev/null ; then
  kill $(lsof -ti tcp:8080) &> /dev/null
fi
