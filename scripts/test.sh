#!/usr/bin/env bash

set -euo pipefail

DB_ENCRYPTION_KEY="$(yaml r ./test-secrets.yml vsm-local-db-encryption-key)" \
USER_CREDENTIALS="{ \"$(yaml r ./test-secrets.yml vsm-local-1-username)\" : \"$(yaml r ./test-secrets.yml vsm-local-1-password)\" } " \
POSTGRES_URI='postgres://postgres:postgres@127.0.0.1:5432/postgres?sslmode=disable' \
ginkgo -p -r -cover -race -skipPackage integration -skipPackage integration
