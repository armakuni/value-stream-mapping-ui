#!/usr/bin/env bash
set -euo pipefail

TEMP_DIR="$(mktemp -d)"
s3cmd --access_key="$(yaml r private/secrets.yml aws-access-key-id)" \
      --secret_key="$(yaml r private/secrets.yml aws-secret-access-key)" \
      get \
      s3://vsm-db-backups/live-backup.sql.enc \
      "${TEMP_DIR}/live-backup.sql.enc" > /dev/null

PASSWORD="$(yaml r private/secrets.yml db-backup-password)" \
openssl enc \
        -d \
        -aes-256-cbc \
        -pass "env:PASSWORD" \
        -in "${TEMP_DIR}/live-backup.sql.enc" \
        -out "${TEMP_DIR}/live-backup.sql"
cat "${TEMP_DIR}/live-backup.sql"
rm -rf "${TEMP_DIR}"
