#!/usr/bin/env bash

set -euo pipefail

hash terraform 2>/dev/null
HAS_TERRAFORM=$?

if [ "${HAS_TERRAFORM}" -ne "0" ]; then
  brew install terraform || true
fi

hash chromedriver 2>/dev/null
HAS_CHROME_DRIVER=$?

if [ "${HAS_CHROME_DRIVER}" -ne "0" ]; then
  brew install chromedriver || true
fi

hash nc 2>/dev/null
HAS_NETCAT=$?

if [ "${HAS_NETCAT}" -ne "0" ]; then
  brew install netcat || true
fi

hash curl 2>/dev/null
HAS_CURL=$?

if [ "${HAS_CURL}" -ne "0" ]; then
  brew install curl || true
fi

hash s3cmd  2>/dev/null
HAS_S3CMD=$?

if [ "${HAS_S3CMD}" -ne "0" ]; then
  brew install s3cmd
fi

hash openssl  2>/dev/null
HAS_OPENSSL=$?

if [ "${HAS_OPENSSL}" -ne "0" ]; then
  brew install openssl
fi

hash npm 2>/dev/null
HAS_NPM=$?

if [ "${HAS_NPM}" -ne "0" ]; then
  brew install npm
fi

if [ "$(npm --version | awk -F. '{print $1}')" -lt 3 ]; then
  brew install npm || brew upgrade npm
fi

go get -u github.com/mikefarah/yaml
go get -u github.com/onsi/ginkgo/ginkgo
