#!/usr/bin/env bash

set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR}/functions.sh"

run_postgres

DB_ENCRYPTION_KEY="$(yaml r ./test-secrets.yml vsm-local-db-encryption-key)" \
USER_CREDENTIALS="{ $(json_escape "$(yaml r ./test-secrets.yml vsm-local-1-username)") : $(json_escape "$(yaml r ./test-secrets.yml vsm-local-1-password)") } " \
POSTGRES_URI='postgres://postgres:postgres@127.0.0.1:5432/postgres?sslmode=disable' \
ginkgo -r integration
