#!/usr/bin/env bash

set -euo pipefail

NON_VENDOR_GO_FILES=()
while read -d '' -r; do
  NON_VENDOR_GO_FILES+=("$REPLY")
done < <(find . -type f -name '*.go' -not -path "./vendor/*" -print0)

NON_VENDOR_GO_MODULES=()
while read -d '' -r; do
  NON_VENDOR_GO_MODULES+=("$REPLY")
done < <(go list ./... | grep --null -v /vendor/)

if gofmt -s -e -d "${NON_VENDOR_GO_FILES[@]}" ; then
  echo "gofmt: looks good"
fi

GOVET_SUCCESS="0"

if ! go vet . ; then
  GOVET_SUCCESS="1"
fi

if [ "${GOVET_SUCCESS}" -eq 0 ] ; then
  echo "go vet: looks good"
fi

GOCYCLO_SUCCESS="0"

for FILE in "${NON_VENDOR_GO_FILES[@]}" ; do
  if ! gocyclo -over 10 "${FILE}" ; then
    GOCYCLO_SUCCESS="1"
  fi
done

if [ "${GOCYCLO_SUCCESS}" -eq 0 ] ; then
  echo "gocyclo: looks good"
fi

GOLINT_SUCCESS="0"

for MODULE in "${NON_VENDOR_GO_FILES[@]}" ; do
  if ! golint "${MODULE}" ; then
    GOLINT_SUCCESS="1"
  fi
done

if [ "${GOLINT_SUCCESS}" -eq 0 ] ; then
  echo "golint: looks good"
fi

if misspell "${NON_VENDOR_GO_FILES[@]}" ; then
  echo "misspell: looks good"
fi

if ./node_modules/.bin/standard "assets/**/*.js" ; then
  echo "standard: looks good"
fi

if ./node_modules/.bin/eslint assets ; then
  echo "eslint: looks good"
fi
