#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function set_map() {
  path="${DIR}/../maps/${2}"

  if ! yaml r "${path}" > /dev/null ; then
    yaml r "${path}"
    return 1
  fi

  json=$(yaml -j r "${path}")

  if curl -X PUT "http://127.0.0.1:8080/v1/maps/${1}" -d "${json}" -u "${3}:${4}" ; then
    echo setting map "maps/${2}" to vsm "${1}" successful
  else
    echo setting map "maps/${2}" to vsm "${1}" failed
    return 2
  fi
}

json_escape () {
  printf '%s' "${1}" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

run_postgres () {
  if docker ps -a | grep "vsm-postgres-test" &> /dev/null ; then
    echo "Deleting old DB"
    docker stop "vsm-postgres-test"
  fi

  docker run --rm --name vsm-postgres-test -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 postgres
  wait_for_tcp localhost 5432
  sleep 2 # postgres opens it's port before it's ready to become connectible
}

run_app () {
  BUILD_VERSION="dev" \
  DB_ENCRYPTION_KEY="${5}" \
  USER_CREDENTIALS="{
    $( json_escape "${1}" ) : $( json_escape "${2}"),
    $( json_escape "${3}" ) : $( json_escape "${4}")
  }" \
  VCAP_APPLICATION="{}" \
  VCAP_SERVICES='{"psql":[{"credentials":{"uri":"postgres://postgres:postgres@127.0.0.1:5432/postgres?sslmode=disable"},"tags":["postgresql"]}]}' \
  go run main.go &

  wait_for_curl "http://${1}:${2}@localhost:8080/"
}

wait_for_tcp () {
  echo "waiting for \"tcp://${1}:${2}\"..."
  while ! nc -z "${1}" "${2}" &> /dev/null; do
    sleep 1
  done
}

wait_for_curl () {
  echo "waiting for \"${1}\"..."

  while ! curl -sq "${1}" -o /dev/null &> /dev/null; do
    sleep 1
  done
}
