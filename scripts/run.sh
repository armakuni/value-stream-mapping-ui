#!/usr/bin/env bash

set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR}/functions.sh"

run_postgres
run_app \
"$(yaml r ./test-secrets.yml vsm-local-1-username)" \
"$(yaml r ./test-secrets.yml vsm-local-1-password)" \
"$(yaml r ./test-secrets.yml vsm-local-2-username)" \
"$(yaml r ./test-secrets.yml vsm-local-2-password)" \
"$(yaml r ./test-secrets.yml vsm-local-db-encryption-key)"

while ps -p "$(lsof -ti tcp:8080)" > /dev/null; do
  sleep 1
done
