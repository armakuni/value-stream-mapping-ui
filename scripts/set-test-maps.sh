#!/usr/bin/env bash

set -euo pipefail

username="$(yaml r ./test-secrets.yml vsm-local-1-username)"
password="$(yaml r ./test-secrets.yml vsm-local-1-password)"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${DIR}/functions.sh"

set_map "ca_ratio_only" "ca_ratio_only_example.yml" "${username}" "${password}"
set_map "custom_wd" "custom_working_day_example.yml" "${username}" "${password}"
set_map "custom_wd" "custom_working_week_example.yml" "${username}" "${password}"
set_map "kaizen" "example_kaizen.yml" "${username}" "${password}"
set_map "example1" "example1.yml" "${username}" "${password}"
set_map "example1" "example1-v2.yml" "${username}" "${password}"
set_map "example2" "example2.yml" "${username}" "${password}"
set_map "branching" "branching-dead-end.yml" "${username}" "${password}"
set_map "branching" "branching-no-metrics.yml" "${username}" "${password}"
set_map "branching" "branching-triangle.yml" "${username}" "${password}"
set_map "branching" "branching-two-paths-triangle.yml" "${username}" "${password}"
set_map "no_ca" "no_ca_ratio_example.yml" "${username}" "${password}"
set_map "no_metrics" "no_metrics_example.yml" "${username}" "${password}"
set_map "no_process_time" "no_process_time_example.yml" "${username}" "${password}"
set_map "vsm" "vsm-vsm-current.yml" "${username}" "${password}"
set_map "vsm" "vsm-vsm-future.yml" "${username}" "${password}"
