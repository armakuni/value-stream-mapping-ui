module bitbucket.org/armakuni/value-stream-mapping-ui

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.0
	github.com/Masterminds/squirrel v1.5.0
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/RyanCarrier/dijkstra v1.0.0
	github.com/alexbrainman/sspi v0.0.0-20180613141037-e580b900e9f5 // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/cloudfoundry-community/go-cfenv v1.18.0
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/go-chi/chi v4.0.3+incompatible // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/jcmturner/gokrb5/v8 v8.3.0 // indirect
	github.com/lib/pq v1.9.0
	github.com/mattes/migrate v3.0.1+incompatible
	github.com/mattn/go-sqlite3 v2.0.2+incompatible // indirect
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sclevine/agouti v3.0.0+incompatible
	github.com/srikrsna/security-headers v2.1.0+incompatible
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20201211090839-8ad439b19e0f // indirect
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.5.0 // indirect
	gopkg.in/jcmturner/rpc.v1 v1.1.0 // indirect
)
