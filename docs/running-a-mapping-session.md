# Running a Mapping Session

Running a Value Stream Mapping session can be a great way to identify, and provide evidence for, where to focus your efforts in improving workflow in your delivery pipeline.

I am going to take you through what a Value Stream Map is, what it's key elements are, how to capture those elements in a meeting, and finally look deeper into a few of the more advanced things you can do with it, and how to fit it into your organisation on an on-going basis.

Value Stream Mapping is an exercise to document workflow. It operates on the level of teams or roles and doesn't require full detail of every process that occurs. It does, however record metrics about amount of time work is waiting and how good the quality of the work coming out of a process is. This means we have just enough information to identify where to dig deeper to begin improvements.

A key thing that makes this exercise different to process mapping is that it is a macro level exercise. This means you're not interested in documenting every single little decision in a system, which can be tedious to do, and tedious to read, but rather you are recording just an overview of the path for 80% of the work flowing through the system. This is to get a "big picture" suitable for making a strategy - and recording where you might investigate further later.

## Core concepts

A finished Value Stream Map might look something like this example.

![Example Stream](images/notes-vsm-example.jpg)

While it's easy to see roughly what's going on in this diagram, there are lots of unusual details here, so lets look at the basic components first.

The most basic components of this system are:

### The Processes

![Process](images/notes-resource-process.jpg)

The process is anything that has an output and represents some work that is performed in the stream. It should be a thing a team does. In order to keep your stream macro, and to avoid slipping into process mapping, you should have somewhere between 5 and 15 of these in your value stream map by the time you're finished.

### Push Arrows 

![Push Arrows](images/notes-push-arrows.jpg)

These represent the relationship between each of your processes. They can branch too, to represent branch in the stream, but you shouldn't have more than 1 branching point per value stream map.

If you branch remember to put a note specifying the amount of work that each route takes.

![Push Arrows Branching](images/notes-branching.jpg)

### The Customer

![Customer](images/notes-customer.jpg)

The customer is the start and the end of the value stream, they request the work, and the work is delivered to them.

Don't get too hung up on this being someone that pays you money. The customer is the requester and delivery point for your work. If you're mapping a part of an organisation that fits within a wider organisation this could be the wider organisation.

With these elements you're ready for the first "walk" down the value stream.

## Your first walk

The first walk down the value stream is where you work out what processes are involved in delivering to your customer.

Before you do this walk have a think about what the scope is for your mapping exercise. You'll want to choose a single product to look at (or product group if the processes involved are very similar).

Don't be afraid if this means you have 3 or 4 product teams you're not looking at, larger organisations can have 10 or more (although smaller ones may only have 1 or 2) streams. Supporting a product, Developing a product, and consulting for a client are all examples of products that add you might map separately, keep it tightly related to one product.

Once you have decided on the scope of your value stream, you need to collect the people who can contribute best. Ideally you should have people who can effect real change across the entire value stream, but also have in depth knowledge across the whole process. This might mean senior management, but it might also mean just team leaders.

One of these people will have to own the transformation project for this product, and will be responsible for keeping the value streams up to date.

The reason you need people who can effect real change is that, while you can do the value stream without them, it's harder to recreate the same "a-ha" moments that the team has regarding the workflow after the fact, and as such harder to get that emotional investment in the results from people who can authorise change.

It's not impossible though, if you're a more junior member of staff without the clout to get the very senior people involved. You can use this exercise as evidence to more senior people to gain sponsorship for changes.

Now the walk itself. 

It's called a walk because (if you're feeling spry) if can literally be that. A walk from your finished product, back to the start. Find your finished product, and find the last person to touch it, and ask them who worked on the product before them, until you get to the customer outside your scope who requested the work. Then take your notes and pile into a meeting room with a lot of wall space.

Alternatively as not everyone is as mobile and sometimes the stream of work is virtual as opposed to physical, you can just get everyone together in a meeting room.

Your aim here is to build a chain of processes that document how work is built within your organisation. 

1. Ask everyone to note down all of the processes involved, I like to give everyone a sharpie and a stack of post it notes
2. Have them stick them on the wall or whiteboard in the order that they occur (throwing away duplicates)
3. Group them up until you have between 5 and 15 processes, and no more than 1 branching point.

Be careful people record what the process actually is, and not what the process "should be". It can be very tempting to idealise the stream, but you will struggle when it comes to the second walk then. There is room for an idealised future stream later when we have the evidence to create it properly.

By the end of this you should have something that looks like this, but made of post-it notes.

![Value Stream Map without Metrics](images/notes-no-metrics.jpg)

Once everyone has agreed that this is a correct and reasonable representation of the work flowing through the stream then you can move on to your second walk.

## Your second walk

Your second walk is to add metrics to your existing process. It doesn't have to happen on the same day, but often it does. The aim is to start gathering information about the current performance of the value stream.

This takes the form of adding metrics and summaries for your collected processes.

![Metrics](images/notes-resource-metrics.jpg)

The metrics you collect will look like the above. Typically after going backwards for the first walk I like to go forwards for the next, having defined the stream. 

For each process you need to ask

1. What is the lead time. The lead time is the total amount of time from asking for, to delivery that a process takes, so including any time waiting in the queue
2. What is the process time. The process time is the amount of time it actually takes to complete the process. This must be equal to or less than the lead time.
3. What is the percent complete and accurate is (C&A%). This is the percentage of time that a process has been completed without problems.
   
By recording each of these things you can start to identify waste in the system. 

Time based waste is recorded by the percentage of process time to lead time. We call this the activity ratio.

Longest process here indicates your bottleneck, and is the fastest items can flow through your stream.

The percent complete and accurate indicates where you're having problems with defects and shows you where you can introduce better quality control processes or practices to improve throughput.

Using this information you can start to identify where you need to focus your transformation efforts.

An overview of how long well your value stream map is performing can be gained by calculating a summary

![Summary](images/notes-summary.jpg)

If your activity ratio and average c&a is good across the board you might want to look for improvements in another value stream.

## Taking it further

During your stream walks you might find other useful information that influences what you might do in the future, or that influences the lead and process time.

One commonly discovered thing is the number of staff involved in a process. This can be useful in knowing how easy of difficult it is going to be to achieve change in that team, and if changing the team size is going to affect the process. You can note the using the little overview of a person sitting in the bottom left.

![Worker](images/notes-resource-nos.jpg)

The other thing that can be discovered is that meetings happen once per period of time, or multiple items are processed at once. This is called batching, and you can indicate that a process batches by adding this triangle exclamation mark symbol. It's worth noting the batch size next to it too.

![Batching](images/notes-resource-fb.jpg)

You might also, as you walk the stream discover various IT systems that information is pushed into automatically or manually. The documentation and recording of these can show failures in communications between two processes, or unneeded communications.

For automated processes, like a CI server pushing to a docker registry, use a lightning bolt connection

![Automated Flow](images/notes-resource-automated-flow.jpg)

For manual processes, like emailing someone, or writing in a wiki, use a line connector

![Manual Flow](images/notes-resource-flow.jpg)

## Recording Desired State

Once you know how the current system is working it's a good idea to make a new value stream. This will be the desired state of the flow within the organisation.

I recommend keeping this separate from your Current State value stream, so you can compare the two as you improve the flow in the organisation.

![Example Future Stream](images/notes-vsm-future.jpg)

On future state value streams put little kaizen bursts next to your processes. This explains how you are hoping to reach this state. During the meeting as people have good ideas on how to improve the process you might put these next to current state value streams as well, rather than creating a totally separate stream.

![Kaizen Burst](images/notes-resource-kaizen.jpg)

Once you have recorded this information once, it's important it does not stagnate. These documents can be updated in line with a normal sprint cycle. Checking the Current State value stream map is up to date, and considering if the plan is currently on track to reach the future state.

## What next?

If you want to take this further checkout [Value Stream Mapping Book](https://www.ksmartin.com/books/value-stream-mapping/) by Karen Martin and Mike Osterling they go into lots of detail, and have some great ideas for making the process the core of your business strategy toolset.

While I recommend using pen, paper, and and sharpie in meetings, I recommend typing them up in the VSM tool. See [Getting Started with the CLI Tool](getting-started-with-the-cli-tool.md) for more details.  
