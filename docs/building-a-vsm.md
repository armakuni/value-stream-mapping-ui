# Building a Value Stream Map

Now that you know how to use the `vsm` cli we need can start building our value stream map yaml files.

*For information on how to setup the CLI tool, see [Getting Started with the CLI Tool](getting-started-with-the-cli-tool.md)*

Here is an example value-stream-map.

![vsm-example](images/vsm-example.jpg)

The yaml for the above would have been:

```yaml
customer: Armakuni
title: My First Example
version: "2.0.0"
render_process_keys: true
resources:
  - type: process
    title: |
      Sign off
      change
      control
    process: '0874234'
    description: |
      manual review
      and sign off of
      submitted change
    metrics:
      leadtime: 24h
      processtime: 1h
      c&aratio: 60%
      c&aratio_desc: 'the process often results in a rejection hence our 60% ratio'
    flow:
      in: ["exampleFlow", "exampleFlow2"]
  - type: process
    title: |
      automated
      security
      test
    process: '0874235'
    previous_process: '0874234'
    description: 'the '
    metrics:
      leadtime: 12h
      processtime: 12h
      c&aratio: 60%
      c&aratio_desc: 'the process often results in a rejection hence our 60% ratio'
    flow:
      out: ["exampleFlow"]
  - type: process
    title: |
      automated
      performance
      test
    process: '0874236'
    previous_process: '0874235'
    description: 'the '
    metrics:
      leadtime: 35m
      processtime: 20m
      c&aratio: 60%
      c&aratio_desc: 'the process often results in a rejection hence our 60% ratio'
  - type: process
    title: example
    process: '0874237'
    previous_process: '0874236'
    description: 'example'
    metrics:
      leadtime: 1h40m
      processtime: 30m
      c&aratio: 60%
      c&aratio_desc: 'the process often results in a rejection hence our 60% ratio'
    flow:
      in: ["exampleFlow2"]
      out: ["exampleFlow2"]
    automated_flow:
      out: ["exampleFlow3"]
```

When configuring a map you have the ability to configure some top level [metadata](#metadata) fields and the configure the bulk of the map using [resources](#resources).

**Note**: A duration string is a possibly signed sequence of decimal numbers, each with optional fraction and a unit suffix, such as "300ms", "-1.5h" or "2h45m". Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".

## Metadata

```yaml
render_process_keys: bool
```

*Optional*. Default `false`. If set to `true`, process keys will be rendered on all resources.

![process-keys](images/process-keys.jpg)

```yaml
customer: string
```

*Optional*. Default `Customer`. The name of the customer that the value stream applies to.

![customer](images/customer.jpg)

```yaml
title: string
```

*Optional*. Defaults to the `name` of the value-stream-map. The page title.

```yaml
version: string
```

*Optional*. Must be `semver` compliant. The version number for the iteration of the value stream.

```yaml
working_day: duration
```

*Optional*. Default `8h`. The duration of a working day, this will be used to convert any time entered or calculated to days in the UI. For example `16h` will draw as `2d` when using the default. You may use time units of smaller than days to define this.

```yaml
working_week: duration
```

*Optional*. Default `5d`. The duration of a working week, this will be used to convert any time entered or calculated to weeks in the UI. For example `10d` will draw as `2w` when using the default. You may use time units of smaller than weeks to define this.

## Resources

```yaml
type: string
```

*Required*. The type of your resource, at present the only supported value is `process`.

```yaml
process: string
```

*Required*. A unique identifier for a process, the visibility of this field is controlled by [render_process_keys](#metadata)

```yaml
previous_process: string
```

*Required*. The `process` ID of the process that should have happened just before the current one. This is used to build a graph of the defined resources.

```yaml
title: string
```

*Optional*. The title of the process block.

![resource-title](images/resource-title.jpg)

```yaml
description: string
```

*Optional*. The description for the process block.

![resource-description](images/resource-description.jpg)

```yaml
kaizen: string
```

*Optional*. An action that will be applied to process to alter the value stream.

![resource-kaizen](images/resource-kaizen.jpg)

```yaml
number_of_staff: int
```

*Optional*. The number of staff that a process requires.

![resource-nos](images/resource-nos.jpg)

```yaml
flow_blockers: string
```

*Optional*. A freeform text field to describe what might block a process from flowing.

![resource-fb](images/resource-fb.jpg)

### Metrics

```yaml
metrics:
  leadtime: duration
  processtime: duration
  c&aratio: percentage
  c&aratio_desc: string
```

*Optional*. Process Metrics

```yaml
leadtime: duration
```

*Optional*. The duration a process takes from start to finish.

```yaml
processtime: duration
```

*Optional*. A subset duration of `leadtime` that defines the time a process was actively being worked on.

```yaml
c&aratio: percentage
```

*Optional*. Complete and Accurate ratio. A percentage representation of the chance of a process being considered completed successfully.

### Flow

```yaml
flow:
  in: [string]
  out: [string]
```

*Optional*: A definition of manual information flows in and out of a process.

![resource-flow](images/resource-flow.jpg)

### Automated Flow

```yaml
automated_flow:
  in: [string]
  out: [string]
```

*Optional*: A definition of automated information flows in and out of a process.

![resource-automated-flow](images/resource-automated-flow.jpg)

### Branching

```yaml
previous_processes: 
  - process: string
    note: string
```

*Optional*. As an alternative to using `previous_process` you can define an array of `previous_processes`, to allow for branching. Only branches that refer to a process in the next column is allowed.

![resource-branching](images/branching.png)
