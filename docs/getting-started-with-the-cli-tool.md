# Getting Started with the CLI Tool

How to get started with the VSM Cli tool.

*For information on how to gather the information, see [Running a Mapping Session](running-a-mapping-session.md)*

## Install the VSM CLI

The first thing you will want to do before creating some Value Stream Maps is install the CLI. This should be as simple as downloading the binary relevant to your operating system by clicking on the icons in the bottom right of your VSM homepage.

![download-cli](images/download-cli.jpg)

Once you have downloaded the CLI make sure to make it executable `chmod +x` add it to your `PATH` for ease of use.

## Using the CLI

`vsm` has a number of commands, running `vsm --help` will display what these are and what the options are but here is a brief summary to get you going.

**Note**: All command other than `validate-map` and `help` require `username`, `password` and `uri` to be set as they connect to the vsm-ui server. The commands below will show you how to set them as part of the CLI commands but you can also use the environment variables of: `VSM_USERNAME`, `VSM_PASSWORD` amd `VSM_URI` so that you can just set them once and re-use them for all your commands.

### Validate a value stream map

```sh
vsm validate-map -m <path to value-stream-map yaml file>
```

### List current value stream maps

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" list-maps
```

### List versions for a value stream map

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" list-map-versions -n <map_name>
```

### Download an existing value stream map

The usage of this command varies depending on if a map is versioned or not.

#### Without versions

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" get-map -n <map_name>
```

#### With versions

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" get-map -n <map_name> -v <version>
```

### Set a value stream map

**Note**: It is recommaned that you store your value stream map yaml files in version control, e.g [git](https://git-scm.com/). While setting a map stores it in our database we currently hold no guarantees around data storage.

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" set-map -n <map_name> -m <path to value-stream-map yaml file>
```

### Delete a values stream map

The usage of this command varies depending on if a map is versioned or not.

#### Without versions

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" delete-map -n <map_name>
```

#### With versions

```sh
vsm -u <username> -p <password> --uri="https://vsm.example.com" delete-map -n <map_name> -v <version>
```

## What next?

Now you have the CLI tool setup, have a look at how to create value stream map yaml files for it in [Building a Value Stream Map](building-a-vsm.md). 
