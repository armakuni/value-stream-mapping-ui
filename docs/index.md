# Value Stream Mapping UI

Value-stream mapping is a lean-management methodology for analysing the current state and designing a future state for the series of events that take a product or service from the customer need through to value delivery and return on investment.

Its intention is to use metrics to visualise areas of inefficiency. For example: slow manual processes, invisible queues impacting delivery, slow feedback cycles or low success rate tasks.

## Video Tutorial

A good way to get started is the [video tutorial](https://www.youtube.com/watch?v=vv8IbYukTxw). 

## What next?

New to Value Stream Mapping? Checkout the [Running a Mapping Session](running-a-mapping-session.md) guide on how to run your own Value Stream Mapping session.

If you are already comfortable with the concept, I recommend jumping straight to [Getting Started with the CLI Tool](getting-started-with-the-cli-tool.md). This will show you how to use existing maps with this application.
