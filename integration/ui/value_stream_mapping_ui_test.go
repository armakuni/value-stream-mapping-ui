package ui

import (
	"fmt"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/sclevine/agouti/matchers"

	"github.com/sclevine/agouti"
)

var _ = Describe("ValueStreamMappingUi", func() {
	var page *agouti.Page
	var year int

	BeforeEach(func() {
		var err error
		page, err = agouti.NewPage(agoutiDriver.URL(),
			agouti.Desired(agouti.Capabilities{
				"chromeOptions": map[string][]string{
					"args": {
						// There is no GPU on our Ubuntu box!
						"disable-gpu",

						// Sandbox requires namespace permissions that we don't have on a container
						"no-sandbox",
						"headless",
					},
				},
			}),
		)
		Ω(err).ShouldNot(HaveOccurred())
		year = time.Now().Year()
	})

	AfterEach(func() {
		Ω(page.Destroy()).Should(Succeed())
	})

	It("should have a valid homepage", func() {
		Ω(page.Navigate(fmt.Sprintf("%s/", uri))).Should(Succeed())
		Ω(page).Should(HaveURL(fmt.Sprintf("%s/", uri)))
		Ω(page.FindByLink("My First Example")).Should(BeFound())
		Ω(page.FindByLink("example2")).Should(BeFound())
		Ω(page.FindByID("footer")).Should(HaveText(fmt.Sprintf("Copyright 2017-%d Armakuni Ltd. All Rights Reserved", year)))
	})

	It("should have a render an example value stream map", func() {
		Ω(page.Navigate(fmt.Sprintf("%s/maps/example1", uri))).Should(Succeed())
		Ω(page).Should(HaveURL(fmt.Sprintf("%s/maps/example1", uri)))
		Eventually(page.AllByClass("joint-cell"), 30, 1).Should(BeFound())
		Ω(page.FindByID("footer")).Should(HaveText(fmt.Sprintf("Copyright 2017-%d Armakuni Ltd. All Rights Reserved", year)))
		Ω(page.FindByID("title")).Should(HaveText("My First Example"))
		Ω(page.FindByID("print-button")).Should(BeFound())

		Ω(page.Navigate(fmt.Sprintf("%s/maps/example2", uri))).Should(Succeed())
		Ω(page).Should(HaveURL(fmt.Sprintf("%s/maps/example2", uri)))
		Eventually(page.AllByClass("joint-cell"), 5, 1).Should(BeFound())
		Ω(page.FindByID("footer")).Should(HaveText(fmt.Sprintf("Copyright 2017-%d Armakuni Ltd. All Rights Reserved", year)))
		Ω(page.FindByID("title")).Should(HaveText("example2"))
		Ω(page.FindByID("print-button")).Should(BeFound())
	})
})
