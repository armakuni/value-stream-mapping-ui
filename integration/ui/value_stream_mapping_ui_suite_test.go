package ui

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
	"time"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
	"github.com/onsi/gomega/gexec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/ghodss/yaml"
	"github.com/sclevine/agouti"
)

func TestValueStreamMappingUi(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "UI Suite")
}

var (
	agoutiDriver *agouti.WebDriver
	uri          string
)

var _ = BeforeSuite(func() {

	path, _ := filepath.Abs("../../")
	os.Setenv("BASE_DIR", path+"/")
	os.Setenv("VCAP_APPLICATION", "{}")
	os.Setenv("VCAP_SERVICES", fmt.Sprintf(`{
	"psql":[
		{
			"credentials":{
				"uri":"%s"
			},
			"tags":[
				"postgresql"
			]
		}
	]
}`, os.Getenv("POSTGRES_URI")))

	agoutiDriver = agouti.ChromeDriver()

	Ω(agoutiDriver.Start()).Should(Succeed())

	uiPath, err := gexec.Build("bitbucket.org/armakuni/value-stream-mapping-ui")
	Ω(err).ShouldNot(HaveOccurred())
	command := exec.Command(uiPath)
	_, err = gexec.Start(command, GinkgoWriter, GinkgoWriter)
	Ω(err).ShouldNot(HaveOccurred())

	fmt.Println("Waiting for server to start...")
	time.Sleep(1 * time.Second)

	var userCredentials authentication.UserCredentials
	var credentialsJSON = os.Getenv("USER_CREDENTIALS")
	json.Unmarshal([]byte(credentialsJSON), &userCredentials)

	for username, password := range userCredentials {
		uri = fmt.Sprintf("http://%s:%s@127.0.0.1:8080", username, url.PathEscape(password))

		loadMap("example1", uri, username, password)
		loadMap("example2", uri, username, password)
	}
})

var _ = AfterSuite(func() {
	os.Unsetenv("VCAP_APPLICATION")
	os.Unsetenv("VCAP_SERVICES")
	os.Unsetenv("VSM_USERNAME")
	os.Unsetenv("VSM_PASSWORD")
	os.Unsetenv("VSM_URI")
	os.Unsetenv("BASE_DIR")
	os.Unsetenv("PORT")
	Ω(agoutiDriver.Stop()).Should(Succeed())
	gexec.KillAndWait()
	gexec.CleanupBuildArtifacts()
})

func loadMap(mapName, uri, username, password string) {
	fileData, err := ioutil.ReadFile(fmt.Sprintf("../../maps/%s.yml", mapName))
	Ω(err).Should(BeNil())
	var valueStream mapper.ValueStream
	Ω(yaml.Unmarshal(fileData, &valueStream)).Should(Succeed())
	jsonBytes, err := json.Marshal(valueStream)
	Ω(err).Should(BeNil())
	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/v1/maps/%s", uri, mapName), bytes.NewReader(jsonBytes))
	Ω(err).Should(BeNil())
	req.Header.Set("X-FORWARDED-PROTO", "https")
	req.SetBasicAuth(username, password)
	client := http.Client{}
	resp, err := client.Do(req)
	Ω(err).Should(BeNil())
	Ω(resp.StatusCode).Should(Equal(http.StatusNoContent))
}
