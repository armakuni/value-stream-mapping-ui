package cli

import (
	"fmt"
	"os/exec"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gbytes"
	"github.com/onsi/gomega/gexec"
	"github.com/sclevine/agouti"
	. "github.com/sclevine/agouti/matchers"
)

var _ = Describe("the cli integrates with the server", func() {
	var (
		page *agouti.Page
	)

	BeforeEach(func() {
		var err error
		page, err = agouti.NewPage(agoutiDriver.URL(),
			agouti.Desired(agouti.Capabilities{
				"chromeOptions": map[string][]string{
					"args": {
						// There is no GPU on our Ubuntu box!
						"disable-gpu",

						// Sandbox requires namespace permissions that we don't have on a container
						"no-sandbox",
						"headless",
					},
				},
			}),
		)
		Ω(err).ShouldNot(HaveOccurred())
	})

	AfterEach(func() {
		Ω(page.Destroy()).Should(Succeed())
	})

	It("creates a map", func() {
		Ω(page.Navigate(fmt.Sprintf("%s/", uri))).Should(Succeed())
		Ω(page).Should(HaveURL(fmt.Sprintf("%s/", uri)))
		Ω(page.FindByLink("My First Example")).ShouldNot(BeFound())
		Ω(page.FindByLink("example2")).ShouldNot(BeFound())

		commandSet1 := exec.Command(cliPath, "set-map", "--name=example1", "--map=../../maps/example1.yml")
		sessionSet1, err := gexec.Start(commandSet1, GinkgoWriter, GinkgoWriter)
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(sessionSet1).Should(gexec.Exit(0))
		Eventually(sessionSet1.Out).Should(gbytes.Say("map example1 has been set"))

		commandSet2 := exec.Command(cliPath, "set-map", "--name=example2", "--map=../../maps/example2.yml")
		sessionSet2, err := gexec.Start(commandSet2, GinkgoWriter, GinkgoWriter)
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(sessionSet2).Should(gexec.Exit(0))
		Eventually(sessionSet2.Out).Should(gbytes.Say("map example2 has been set"))

		Ω(page.Navigate(fmt.Sprintf("%s/", uri))).Should(Succeed())
		Ω(page).Should(HaveURL(fmt.Sprintf("%s/", uri)))
		Ω(page.FindByLink("My First Example")).Should(BeFound())
		Ω(page.FindByLink("example2")).Should(BeFound())

		commandGet1 := exec.Command(cliPath, "get-map", "--name=example2")
		sessionGet1, err := gexec.Start(commandGet1, GinkgoWriter, GinkgoWriter)
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(sessionGet1).Should(gexec.Exit(0))
		Eventually(sessionGet1.Out.Contents()).Should(ContainSubstring("customer: Armakuni"))

		commandListVersions1 := exec.Command(cliPath, "list-map-versions", "--name=example1")
		sessionListVersions1, err := gexec.Start(commandListVersions1, GinkgoWriter, GinkgoWriter)
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(sessionListVersions1).Should(gexec.Exit(0))
		Eventually(sessionListVersions1.Out).Should(gbytes.Say("0.0.1"))

		commandDelete1 := exec.Command(cliPath, "delete-map", "--name=example1", "--version=0.0.1")
		sessionDelete1, err := gexec.Start(commandDelete1, GinkgoWriter, GinkgoWriter)
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(sessionDelete1).Should(gexec.Exit(0))
		Eventually(sessionDelete1.Out).Should(gbytes.Say("deleted map example1 with version 0.0.1"))

		commandDelete2 := exec.Command(cliPath, "delete-map", "--name=example2")
		sessionDelete2, err := gexec.Start(commandDelete2, GinkgoWriter, GinkgoWriter)
		Ω(err).ShouldNot(HaveOccurred())
		Eventually(sessionDelete2).Should(gexec.Exit(0))
		Eventually(sessionDelete2.Out).Should(gbytes.Say("deleted map example2"))

		Ω(page.Navigate(fmt.Sprintf("%s/", uri))).Should(Succeed())
		Ω(page).Should(HaveURL(fmt.Sprintf("%s/", uri)))
		Ω(page.FindByLink("My First Example")).ShouldNot(BeFound())
		Ω(page.FindByLink("example2")).ShouldNot(BeFound())
	})
})
