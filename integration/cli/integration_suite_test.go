package cli

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"bitbucket.org/armakuni/value-stream-mapping-ui/authentication"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gexec"

	"github.com/sclevine/agouti"
)

func TestIntegration(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "CLI Integration Suite")
}

var (
	agoutiDriver *agouti.WebDriver
	uri          string
	cliPath      string
)

var _ = BeforeSuite(func() {
	var port = "9000"
	var userCredentials authentication.UserCredentials
	var credentialsJSON = os.Getenv("USER_CREDENTIALS")
	json.Unmarshal([]byte(credentialsJSON), &userCredentials)

	for username, password := range userCredentials {
		if username == "" {
			uri = "http://127.0.0.1:" + port
		} else {
			uri = fmt.Sprintf("http://%s:%s@127.0.0.1:%s", username, url.PathEscape(password), port)
		}

		os.Setenv("VSM_USERNAME", username)
		os.Setenv("VSM_PASSWORD", password)
	}

	os.Setenv("VCAP_APPLICATION", "{}")
	os.Setenv("VCAP_SERVICES", fmt.Sprintf(`{
	"psql":[
		{
			"credentials":{
				"uri":"%s"
			},
			"tags":[
				"postgresql"
			]
		}
	]
}`, os.Getenv("POSTGRES_URI")))

	os.Setenv("VSM_URI", "http://127.0.0.1:"+port)

	path, _ := filepath.Abs("../../")
	os.Setenv("BASE_DIR", path+"/")
	os.Setenv("PORT", port)

	agoutiDriver = agouti.ChromeDriver()

	Ω(agoutiDriver.Start()).Should(Succeed())

	var err error
	cliPath, err = gexec.Build("bitbucket.org/armakuni/value-stream-mapping-cli/vsm")
	Ω(err).ShouldNot(HaveOccurred())

	uiPath, err := gexec.Build("bitbucket.org/armakuni/value-stream-mapping-ui")
	Ω(err).ShouldNot(HaveOccurred())
	command := exec.Command(uiPath)
	_, err = gexec.Start(command, GinkgoWriter, GinkgoWriter)
	Ω(err).ShouldNot(HaveOccurred())
})

var _ = AfterSuite(func() {
	os.Unsetenv("VCAP_APPLICATION")
	os.Unsetenv("VCAP_SERVICES")
	os.Unsetenv("VSM_USERNAME")
	os.Unsetenv("VSM_PASSWORD")
	os.Unsetenv("VSM_URI")
	os.Unsetenv("BASE_DIR")
	os.Unsetenv("PORT")
	Ω(agoutiDriver.Stop()).Should(Succeed())
	gexec.KillAndWait()
	gexec.CleanupBuildArtifacts()
})
