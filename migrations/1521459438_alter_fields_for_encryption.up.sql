ALTER TABLE maps ADD COLUMN name_index text NULL;
ALTER TABLE maps ALTER COLUMN name TYPE text USING name::text;
ALTER TABLE maps ADD COLUMN version_index text NULL;
ALTER TABLE maps ALTER COLUMN version TYPE text USING version::text;
