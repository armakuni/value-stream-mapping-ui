CREATE TABLE maps
		(
			id serial primary key,
			name varchar(255) not null,
			version varchar(20),
			vsm_json text
		);
