/** Note: This will fail if any of the data has already been encrypted **/
ALTER TABLE maps ALTER COLUMN vsm_json TYPE json USING vsm_json::json;
