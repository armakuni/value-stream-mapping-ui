ALTER TABLE maps DROP name_index;
ALTER TABLE maps ALTER COLUMN name TYPE varchar(255) USING name::varchar(255);
ALTER TABLE maps DROP version_index;
ALTER TABLE maps ALTER COLUMN version TYPE varchar(20) USING version::varchar(20);
