# On-boarding a new User

This will talk you through how to add a new user to the service. This is
currently a manual process.

## Steps

1. Add user to site
2. Set an example VSM on their account
3. Send a welcome email to their account

## Add user to site

Open [private/secrets.yml](../private/secrets.yml) and look for the `vsm-ak-user-credentials` key.

This is a key value pair of username to password.

It'll look something like this, and is a json object.

```yaml
vsm-ak-user-credentials: |
  {
    "bthompson" : "ex@mple",
    "ewalt": "ex@mple3"
  }
```

Add the new users account to the bottom of this list with the username matching the scheme first letter of first name, and surname. E.g. "Billie Thompson" becomes "bthompson". Generate them a random password, using pwgen or similar tool.



Commit this and in the commit message remember to add

```
Requested by: An Ak-er an.aker@armakuni.com
```

Then set the pipeline.

```shell
make set-pipeline
```

## Set an example VSM on their account

Once the pipeline has finished set the example VSM Map into their account.

```
cd $(mktemp -d)
vsm e > example.yml
vsm --uri https://vsm.armakuni.co.uk/ \
    --username nusername \
    --password choos3Hoxu \
    set-map \
    --name example \
    --map example.yml
```

## Send a welcome email to their account

Next send them an email using this template

> Hi *{Name}*!
>
> Created you an account on our awesome little VSM tool.
>
> Your username is **nusername**
>
> and your password is **choos3Hoxu**
>
> I've stuck a demo stream in there so you can see it in action right off, you can see that here: https://nusername:choos3Hoxu@vsm.armakuni.co.uk/maps/example.
>
> There's full documentation at https://vsm-docs.armakuni.co.uk/. In there is how to set the maps and a little guide on how run VSM sessions, how to set and update maps, and generally how to get started with the service.
>
> I'd love to hear what you think. Drop me a message if you have any problems, have any comments or thoughts
>
> Thanks,
>
> *{Your Name}*
