# Backup & Restore

This will give you information on how frequently backups happen, and
how to restore data from them.

## SLA

* We backup every 6 hours
* Backups are kept for 90 days total. 
 - 30 days in quick access storage
 - 30 to 60 days in infrequent access storage
 - 60 to 90 days in long term storage
 - More than 90 days the backups are removed
* Backups are encrypted using `aes-256-cbc`

## Backup process

Backups are triggered by concourse and are written to an S3 Bucket. 

You can see details of that s3 bucket by running

```bash
cd terraform-initial
terraform state show aws_s3_bucket.vsm-db-backups
```

The backup lifecycle is handled by S3.

Backups are tested each deploy by running the migrations against them.

## Restore

Restore is a manual process.

To simply get the latest version of the database backed up run

```bash
make retrieve-latest-backup                  
```

If you want to retrieve a specific version you can do that from the s3
ui, and choose from the versions there.

If you manually download the backup you can run

```bash
PASSWORD="$(yaml r private/secrets.yml db-backup-password)" \
openssl enc \
        -d \
        -aes-256-cbc \
        -pass "env:PASSWORD" \
        -in "example.sql.enc" \
        -out "example.sql"
```

The next step you want is to either to restore part or all of the 
backup.

You can retrieve where you want to restore to, the postgres server, from
the environment variable of you application.

You can do this by running

```bash
cf login -u "username@example.com" -o armakuni -s value-stream-mapping
cf e value-stream-map
```

The URI will be in the system provided "VCAP_SERVICES". You can use this
to connect to the database.

```bash
psql "postgres://username:password@example.com:5432/example"
```

This will give you a prompt on the database.

If you wish to restore the backup to this database, run

```bash
psql "postgres://username:password@example.com:5432/example" -f "example.sql"
```

Alternatively you may wish to restore to a local version and perform 
some selective updates.
