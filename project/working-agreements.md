# Working Agreements

## Definition of Ready

Example of a ready story

```
As a mapper 
I can like to detail flow blockers to reflect the observations made with the team
So that mappers can identify blockers to flow

# Acceptance Criteria

* Available on vsm.armakuni.co.uk
* Does not share the datastores with other instances of the tool
* Is continuously deployed when the tool is updated
```

### Story Meets I.N.V.E.S.T. Criteria

  * "I" ndependent (of all others)
  * "N" egotiable (not a specific contract for features)
  * "V" aluable (or vertical)
  * "E" stimable (to a good approximation)
  * "S" mall (so as to fit within an iteration)
  * "T" estable (in principle, even if there isn't a test for it yet)

### Business Value is Recorded

The format should use

```
As a {user}
I can {goal}
So that {reason}
```

The users should be documented in the [backlog.md](backlog.md)

## Definition of Done

Example of a done story

```
As a mapper 
I can like to detail flow blockers to reflect the observations made with the team
So that mappers can identify blockers to flow

# Acceptance Criteria

* Available on vsm.armakuni.co.uk
* Does not share the datastores with other instances of the tool
* Is continuously deployed when the tool is updated

# To Accept

* Visit the [example](https://vsm.armakuni.co.uk/maps/example) and you should be able to see the blockers
```

### To Accept

On completion of the story the developer should add a to accept section
on how to test the implemented feature.
