# Backlog

## Stories & Tasks

See [pivotal tracker](https://www.pivotaltracker.com/n/projects/2116133)

## Actors

This document records the actors for business value statements

That is the user in this style of statement:

```
As a {user} I want to {do something valuable}"
```

### mapper

A Mapper is a user who wishes to display a value stream map. They are
the basic "user" for this system.

They have no special privileges, beyond the http basic auth to see the 
application.
