# Risks

Risks, and mitigation's, that would prevent successful delivery

## To Be Decided

### Risk

What might happen

### Mitigation

How we might avoid that.
